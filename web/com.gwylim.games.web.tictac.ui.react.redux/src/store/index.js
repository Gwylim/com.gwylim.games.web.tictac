import {applyMiddleware, compose, createStore} from "redux";
import rootReducer from "../reducers/index";
import promise from 'redux-promise-middleware'
import thunk from "redux-thunk";
import {websocketMiddleware} from "../middleware/websocket";

const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    rootReducer,
    storeEnhancers(applyMiddleware(
        websocketMiddleware(),
        promise,
        thunk
    ))
);

export default store;

// Getters:
export const getSignIn = state => state.signin || {};
export const signInUsernameInUseError = state => getSignIn(state).username_in_use || false;
export const signInFailure = state => getSignIn(state).failure || false;
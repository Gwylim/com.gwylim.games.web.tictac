import React, {Component} from "react";
import {connect} from "react-redux";
import {loadLobby, toggleOpponent} from "../actions/lobbyActions";


export class LobbyWidget extends Component {

    componentDidMount() {
        // Load the lobby when the component is mounted:
        //this.props.loadLobby();
    }

    handleClick(opponenetRef) {
        this.props.toggleOpponent(opponenetRef);
    }

    render() {
        return (
            <ul className="list-group list-group-flush">
                {this.props.lobby
                    .filter(ref => ref !== this.props.player.playerRef) // Don't display our-self.
                    .map(ref => this.props.players[ref])
                    .filter(player => player) // Filter out things that have not loaded yet.
                    .map(player => {
                        const ref =  player.playerRef;
                        const className =  "list-group-item " + (this.props.opponents.includes(ref) ? "alert-primary" : "");

                        return (<li className={className}
                            key={ref}
                            onClick={()=> this.handleClick(ref)}>

                                {player.name}
                        </li>)
                    })}
            </ul>
        );
    }
}

function mapStateToProps(state) {
    return {
        player: state.player,
        lobby: state.lobby,
        opponents: state.opponents,
        players: state.players
    };
}

function mapDispatchToProps(dispatch) {
    return {
        toggleOpponent: opponentRef => dispatch(toggleOpponent(opponentRef)),
        loadLobby
    };
}


export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(LobbyWidget);
import React, {Component} from "react";
import {connect} from "react-redux";
import BoardWidget from "./BoardWidget";
import {leaveGame} from "../actions/gameActions";

export class GameWidget extends Component {

    render() {
        const {
            player,
            game,
            players
        } = this.props;

        const playerRef = player.playerRef;

        const {
            winner,
            toGo,
            opponent,
            board,
        } = game;

        const gameOver = !toGo;

        const youWin = gameOver && (winner === playerRef);
        const youLost = gameOver && winner && (winner !== playerRef);
        const draw = gameOver && !winner;

        const yourTurn = !gameOver && (toGo === playerRef);
        const theirTurn = !gameOver && (toGo !== playerRef);

        const opponentName = players[opponent].name;

        return (
            <div>
                <h1>Game</h1>
                Opponent: {opponentName}

                { board && <BoardWidget/> }

                { yourTurn && <h1>Your turn</h1> }
                { theirTurn && <h1>Their turn</h1> }

                { youWin && <h1>You win</h1> }
                { youLost && <h1>You lost</h1> }
                { draw && <h1>draw</h1> }

                { gameOver &&
                    <div onClick={this.props.leaveGame}>
                        Return to lobby
                    </div>
                }

            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        player: state.player,
        game: state.game,
        players: state.players
    };
}

function mapDispatchToProps(dispatch) {
    return {
        leaveGame: () => dispatch(leaveGame()),
    };
}


export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(GameWidget);
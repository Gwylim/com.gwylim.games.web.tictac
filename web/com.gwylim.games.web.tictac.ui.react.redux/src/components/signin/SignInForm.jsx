import React, {Component} from "react";
import {connect} from "react-redux";
import {signIn} from "../../actions/signInActions";
import { signInFailure } from '../../store';

class ConnectedSignInForm extends Component {
    constructor() {
        super();
        this.state = {
            username: ""
        };
    }

    handleChange(event) {
        this.setState({ [event.target.id]: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        const { username, password } = this.state;
        this.props.signIn(username, password);
    }

    render() {
        const { username } = this.state;
        const {
            player,
            failure
        } = this.props;

        if (player) {
            return (
                <h2>{player.name}</h2>
            );
        }
        else {
            return (
                <div>
                    <h2>Log In</h2>
                    <form onSubmit={this.handleSubmit.bind(this)}>
                        <div className="form-group">
                            <label htmlFor="Player name:">Username</label>
                            <input
                                type="text"
                                className="form-control"
                                id="username"
                                value={username}
                                onChange={this.handleChange.bind(this)}
                            />
                        </div>

                        <div className="form-group">
                            <label>Password</label>
                            <input
                                type="password"
                                className="form-control"
                                id="password"
                                onChange={this.handleChange.bind(this)}
                            />
                        </div>

                        <button type="submit" className="btn btn-success btn-lg">
                            Log in
                        </button>
                    </form>

                    { failure &&
                        <div className="error">Username or password incorrect.</div>
                    }
                </div>
            );
        }
    }
}


function mapDispatchToProps(dispatch) {
    return {
        signIn: (username, password) => dispatch(signIn(username, password))
    };
}

function mapStateToProps(state) {
    return {
        player: state.player,
        failure: signInFailure(state)
    };
}

const SignInForm = connect(mapStateToProps, mapDispatchToProps)(ConnectedSignInForm);

export default SignInForm;
import React, {Component, Fragment} from "react";
import SignInForm from "./SignInForm";
import SignUpForm from "./SignUpForm";

const SIGNIN = "signin";
const SIGNUP = "signup";

class SignInPage extends Component {
    constructor() {
        super();
        this.state = {
            mode: SIGNIN,
        };
    }

    render() {
        const {
            mode
        } = this.state;

        return (
            <div>
                { mode === SIGNIN && this.renderSignIn() }
                { mode === SIGNUP && this.renderSignUp() }
            </div>
        );
    }


    toggleMode() {
        const mode = this.state.mode === SIGNUP ? SIGNIN : SIGNUP;

        this.setState({
            ...this.state,
            mode
        })
    }

    renderSignUp() {
        return <Fragment>
            <SignUpForm/>
            <button className="btn btn-lg"
                    onClick={this.toggleMode.bind(this)}>
                Log in
            </button>
        </Fragment>;
    }


    renderSignIn() {
        return <Fragment>
            <SignInForm/>
            <button className="btn btn-lg"
                    onClick={this.toggleMode.bind(this)}>
                Sign up
            </button>
        </Fragment>;
    }
}

export default SignInPage;
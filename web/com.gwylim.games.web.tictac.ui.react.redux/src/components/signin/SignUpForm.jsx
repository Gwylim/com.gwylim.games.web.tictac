import React, {Component} from "react";
import {connect} from "react-redux";
import {signInUsernameInUseError} from "../../store";
import {signUp} from "../../actions/signInActions";

class SignUpFormComponent extends Component {
    constructor() {
        super();
        this.state = {
            username: "",
            passwordMatchError: false
        };
    }

    handleChange(event) {
        this.setState({ [event.target.id]: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        const {
            username,
            password,
            passwordRepeat
        } = this.state;

        if (password === passwordRepeat) {
            this.props.signUp(username, password);
        }
        else {
            console.log("Password match error:" + password + " - " + passwordRepeat);
            this.setState({passwordMatchError: true})
        }
    }

    render() {
        const { username, passwordMatchError } = this.state;
        const {
            player,
            usernameInUseError
        } = this.props;

        if (player) {
            return (
                <h2>{player.name}</h2>
            );
        }
        else {
            return (
                <div>
                    <h2>Sign Up</h2>
                    <form onSubmit={this.handleSubmit.bind(this)}>
                        <div className="form-group">
                            <label>Username</label>
                            <input
                                type="text"
                                className="form-control"
                                id="username"
                                value={username}
                                onChange={this.handleChange.bind(this)}
                            />
                            { usernameInUseError &&
                                <div className="error">Username already in use</div>
                            }
                        </div>

                        <div className="form-group">
                            <label>Password</label>
                            <input
                                type="password"
                                className="form-control"
                                id="password"
                                onChange={this.handleChange.bind(this)}
                            />
                            { passwordMatchError &&
                                <div className="error">Passwords do not match</div>
                            }
                        </div>

                        <div className="form-group">
                            <label>Password</label>
                            <input
                                type="password"
                                className="form-control"
                                id="passwordRepeat"
                                onChange={this.handleChange.bind(this)}
                            />
                        </div>

                        <button type="submit" className="btn btn-success btn-lg">
                            Log in
                        </button>
                    </form>
                </div>
            );
        }
    }
}


function mapDispatchToProps(dispatch) {
    return {
        signUp: (username, password) => dispatch(signUp(username, password))
    };
}

function mapStateToProps(state) {
    return {
        player: state.player,
        usernameInUseError: signInUsernameInUseError(state)
    };
}

const SignUpForm = connect(mapStateToProps, mapDispatchToProps)(SignUpFormComponent);

export default SignUpForm;
import React, {Component} from "react";
import {connect} from "react-redux";
import classNames from 'classnames';
import "./style/BoardWidget.css"
import {makeMove} from "../actions/gameActions";


export class BoardWidget extends Component {

    render() {
        return (
            <div>
                <div className="board">
                    {this.createSquares()}
                </div>
            </div>
        );
    }

    createSquares() {
        const {
            playerRef,
            game
        } = this.props;

        const {
            board,
            winMap,
            winner
        } = game;

        let table = [];

        // Outer loop to create parent
        for (let y = 0; y < 3; y++) {
            const children = [];
            //Inner loop to create children
            for (let x = 0; x < 3; x++) {
                const ours = board[x][y] === playerRef;
                const theirs = board[x][y] && board[x][y] !== playerRef;

                const classes = classNames({
                    icon: true,
                    win: winMap[x][y],
                    X: ours,
                    O: theirs
                });

                children.push(
                    <div
                        className="square"
                        onClick={() => !ours && !theirs && !winner && this.props.makeMove(x, y)}
                    >

                        <div className={classes} />
                    </div>
                );
            }
            //Create the parent and add the children
            table.push(<div className="boardRow">{children}</div>)
        }
        return table
    }
}

function mapStateToProps(state) {
    return {
        playerRef: state.player.playerRef,
        game: state.game,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        makeMove: (x, y) => dispatch(makeMove(x, y)),
    };
}


export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(BoardWidget);
import React, {Component} from "react";
import LobbyWidget from "./LobbyWidget";
import GameWidget from "./GameWidget";
import {connect} from "react-redux";

import SignInPage from "./signin/SignInPage";
import axios from "axios";
import { signIn } from "../actions/signInActions";

import "./style/App.css";

export class App extends Component {

    constructor(props) {
        super(props);

        // Set the default content type for POST requests.
        // This will be used for sending strings, objects are sent as json.
        axios.defaults.headers.post['Content-Type'] = "text/plain";
    }


    render() {
        return (
            <div className="container">{this.getContent()}</div>
        );
    }

    getContent() {
        if (!this.props.player) {
            return (
                <SignInPage/>
            );
        }
        else if (this.props.game && this.props.game.opponent) {
            return (
                <GameWidget/>
            );
        }
        else {
            return (
                <LobbyWidget/>
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        player: state.player,
        game: state.game,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        signIn: playerName => dispatch(signIn(playerName)),
    };
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
import axios from "axios";
import {authorisation_header, forbiddenWords} from "../constants";

export const signin_action_types = {
    LOG_IN_SUCCESS: "LOG_IN_SUCCESS",
    LOG_IN_FAILURE: "LOG_IN_FAILURE",
    LOG_IN_BAD_WORD_FOUND: "LOG_IN_BAD_WORD_FOUND",
    LOG_IN_FAILURE_USERNAME_IN_USE: "LOG_IN_FAILURE_USERNAME_IN_USE"
};

export const signIn = (username, password) => dispatch => {
    if (!username || !password) {
        // Don't allow blank player name:
        return;
    }

    return axios.post("/api/player/signIn", {username, password} )
        .then(
            response => dispatch(signInSuccess(response.data)),
            error => dispatch(signInFailure(error))
        );
};

export const signUp = (username, password) => dispatch => {
    if (!username || !password) {
        // Don't allow blank player name:
        return;
    }

    const foundWords = forbiddenWords.filter(word => username.includes(word));

    if (foundWords.length > 0) {
        // If bad word found: block the signin action and emit a LOG_IN_BAD_WORD_FOUND.
        dispatch(signInBadWordFound(foundWords));
        return;
    }

    return axios.post("/api/player/signUp", {username, password} )
        .then(
            response => dispatch(signInSuccess(response.data)),
            error => dispatch(signInFailure(error))
        );
};


export const signInSuccess = player => dispatch => {
    const { token, playerRef } = player;

    // Set the auth token for further requests:
    axios.defaults.headers.common[authorisation_header] = token;


    window.onbeforeunload = () => {
        disconnect(playerRef);
    };

    dispatch({type: signin_action_types.LOG_IN_SUCCESS, player});
};

export const signInFailure = data => {
    if (data.response.status === 409) {
        return {type: signin_action_types.LOG_IN_FAILURE_USERNAME_IN_USE};
    }
    return {type: signin_action_types.LOG_IN_FAILURE, data};
};
export const signInBadWordFound = words => ({type: signin_action_types.LOG_IN_BAD_WORD_FOUND, words});

export const disconnect = (playerRef) => {
    axios.post("/api/player/disconnect", playerRef)
         .resolve();
};
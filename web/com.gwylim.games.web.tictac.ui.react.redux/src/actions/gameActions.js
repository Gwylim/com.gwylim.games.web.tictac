import axios from "axios";
import {enterLobby} from "./lobbyActions";

export const game_action_types = {
    GAME_STARTED: "GAME_STARTED",
    GAME_ENDED: "GAME_ENDED",

    MOVE_MADE: "MOVE_MADE",

    MAKE_MOVE: "MAKE_MOVE",
    MAKE_MOVE_SUCCESS: "MAKE_MOVE_SUCCESS",
    MAKE_MOVE_FAILURE: "MAKE_MOVE_FAILURE",

    LEAVE_GAME: "LEAVE_GAME",

    LOAD_GAME_SUCCESS: "LOAD_GAME_SUCCESS",
    LOAD_GAME_FAILURE: "LOAD_GAME_FAILURE",

};

export const gameStarted = (opponent, gameRef) => (dispatch) => {
    dispatch(loadGame(gameRef));
    dispatch({type: game_action_types.GAME_STARTED, opponent, gameRef});
};
export const gameEnded = () => ({type: game_action_types.GAME_ENDED });

export const moveMade = () => (dispatch, getState) => {
    const { game } = getState();

    dispatch(loadGame(game.gameRef));
    dispatch({type: game_action_types.MOVE_MADE})
};

export const makeMove = (x, y) => (dispatch, getState) => {
    const {
        player,
        game
    } = getState();

    const location = {x, y};

    axios.post(`/api/game/${game.gameRef}/${player.playerRef}/move`, location)
        .then(
            response => dispatch(makeMoveSuccess(response.data)),
            error => dispatch(makeMoveFailure(error))
        );

    dispatch({type: game_action_types.MAKE_MOVE, location });
};
export const makeMoveSuccess = game => ({type: game_action_types.MAKE_MOVE_SUCCESS, game});
export const makeMoveFailure = () => ({type: game_action_types.MAKE_MOVE_FAILURE });

export const leaveGame = () => (dispatch, getState) => {
    const {
        player,
        game
    } = getState();

    axios.post(`/api/game/${game.gameRef}/leave`, player.playerRef)
        .then(
            () => dispatch(leaveGameSuccess()),
            error => dispatch(leaveGameFailure(error))
        );

    dispatch(enterLobby());

    dispatch({type: game_action_types.LEAVE_GAME});
};

export const leaveGameSuccess = () => ({type: game_action_types.MAKE_MOVE_SUCCESS});
export const leaveGameFailure = (error) => ({type: game_action_types.MAKE_MOVE_FAILURE, error});

export const loadGame = gameRef => (dispatch) => {

    axios.get(`/api/game/${gameRef}`)
        .then(
            response => dispatch(loadGameSuccess(response.data)),
            error => dispatch(loadGameFailure(error))
        );

    // if (action.type === action_types.GAME_STARTED || action.type === action_types.MOVE_MADE) {
};

export const loadGameSuccess = game => ({type: game_action_types.LOAD_GAME_SUCCESS, game});
export const loadGameFailure = data => ({type: game_action_types.LOAD_GAME_FAILURE, data});
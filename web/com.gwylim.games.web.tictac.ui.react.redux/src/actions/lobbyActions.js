import axios from "axios";

export const lobby_action_types = {
    ENTER_LOBBY_SUCCESS: "ENTER_LOBBY_SUCCESS",
    ENTER_LOBBY_FAILURE: "ENTER_LOBBY_FAILURE",

    TOGGLE_OPPONENT_SUCCESS: "TOGGLE_OPPONENT_SUCCESS",
    TOGGLE_OPPONENT_FAILURE: "TOGGLE_OPPONENT_FAILURE",

    LOAD_LOBBY: "LOAD_LOBBY",
    LOAD_LOBBY_SUCCESS: "LOAD_LOBBY_SUCCESS",
    LOAD_LOBBY_FAILURE: "LOAD_LOBBY_FAILURE",

    LOAD_OPPONENTS_SUCCESS: "LOAD_OPPONENTS_SUCCESS",
    LOAD_OPPONENTS_FAILURE: "LOAD_OPPONENTS_FAILURE",

    PLAYER_LOAD_SUCCESS: "PLAYER_LOAD_SUCCESS",
    PLAYER_LOAD_FAILURE: "PLAYER_LOAD_FAILURE",

    OTHER_PLAYER_ENTERED_LOBBY: "OTHER_PLAYER_ENTERED_LOBBY",
    OTHER_PLAYER_LEFT_LOBBY: "OTHER_PLAYER_LEFT_LOBBY",
};

export const enterLobby = playerArg => (dispatch, getState) => {
    const { player } = getState();

    const { playerRef } = (player || playerArg);

    axios.post("/api/lobby/", playerRef)
        .then(
            () => dispatch(enterLobbySuccess()),
            error => dispatch(enterLobbyFailure(error))
        );
};
export const enterLobbySuccess = () => (dispatch) => {

    dispatch(loadLobby());

    dispatch({type: lobby_action_types.ENTER_LOBBY_SUCCESS});
};
export const enterLobbyFailure = data => ({type: lobby_action_types.ENTER_LOBBY_FAILURE, data});


export const otherEnteredLobby = playerRef => dispatch => {

    dispatch(loadLobbyPlayers(playerRef));

    dispatch({type: lobby_action_types.OTHER_PLAYER_ENTERED_LOBBY, playerRef})
};
export const otherLeftLobby = playerRef => dispatch => setTimeout(() => dispatch({type: lobby_action_types.OTHER_PLAYER_LEFT_LOBBY, playerRef}));

export const toggleOpponent = opponentRef => (dispatch, getState) => {
    const { player } = getState();

    axios.post(`/api/lobby/${player.playerRef}/opponents/toggle`, opponentRef)
        .then(
            () => dispatch(toggleOpponentSuccess(opponentRef)),
            error => dispatch(toggleOpponentFailure(error, opponentRef))
        );
};

export const toggleOpponentSuccess = opponentRef => dispatch => {
    dispatch(loadOpponents());
    dispatch({type: lobby_action_types.TOGGLE_OPPONENT_SUCCESS, opponentRef});
};
export const toggleOpponentFailure = (data, opponentRef) => dispatch => dispatch({type: lobby_action_types.TOGGLE_OPPONENT_FAILURE, data, opponentRef});

export const loadOpponents = () => (dispatch, getState) => {
    const { player } = getState();

    axios.get(`/api/lobby/${player.playerRef}/opponents`)
        .then(
            lobby => dispatch(loadOpponentSuccess(lobby.data)),
            error => dispatch(loadOpponentFailure(error))
        );
};

export const loadOpponentSuccess = opponents => ({type: lobby_action_types.LOAD_OPPONENTS_SUCCESS, opponents});
export const loadOpponentFailure = (data) => ({type: lobby_action_types.LOAD_OPPONENTS_FAILURE, data});

export const loadLobby = () => dispatch => {

    dispatch(() => axios.get("/api/lobby/")
        .then(
            lobby => dispatch(loadLobbySuccess(lobby.data)),
            error => dispatch(loadLobbyFailure(error))
        )
    );

    dispatch({type: lobby_action_types.LOAD_LOBBY});
};
export const loadLobbySuccess = lobby => dispatch => {
    // If we want to load the players that will be in the lobby we will have to
    // merge getState().lobby into the data from the action OR fire a different event.

    // Load the new players from the lobby update:
    dispatch(loadLobbyPlayers(lobby));

    dispatch({type: lobby_action_types.LOAD_LOBBY_SUCCESS, lobby})
};
export const loadLobbyFailure = data => ({type: lobby_action_types.LOAD_LOBBY_FAILURE, data});



export const loadLobbyPlayers = playersToLoad => (dispatch, getState) => {
    // NB: Lobby in state will not yet have been updated.
    const { players } = getState();

    const playersToLoadArray = Array.isArray(playersToLoad) ? playersToLoad : [playersToLoad];

    // If we want to load the players that will be in the lobby we will have to
    // merge getState().lobby into the data from the action OR fire a different event.

    // Load the new players from the lobby update:
    playersToLoadArray.filter(player => !players[player]) // Don't reload players that are already loaded
        .forEach(p => {
                dispatch(() => axios.get(`/api/player/${p}`)
                    .then(
                        response => dispatch(playerLoaded(response.data)),
                        error => dispatch(playerLoadError(error))
                    ))
            }
        );
};

export const playerLoaded = player => ({type: lobby_action_types.PLAYER_LOAD_SUCCESS, player});
export const playerLoadError = error => ({type: lobby_action_types.PLAYER_LOAD_FAILURE, error});
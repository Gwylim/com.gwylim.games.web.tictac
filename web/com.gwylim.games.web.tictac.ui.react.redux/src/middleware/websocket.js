import {
    game_action_types,
    gameEnded, gameStarted, moveMade,
} from "../actions/gameActions";
import {Stomp} from "@stomp/stompjs/esm5/compatibility/stomp";
import {authorisation_header} from "../constants";
import {enterLobby, lobby_action_types, otherEnteredLobby, otherLeftLobby} from "../actions/lobbyActions";
import {signin_action_types} from "../actions/signInActions";


export function websocketMiddleware() {
    const url = "ws://localhost:8888/updates";

    console.log("Create STOMP Client");
    const client = Stomp.client(url);
    const subscriptions = {};

    return function websocketMiddleware({dispatch, getState}) {
        return function (next) {
            return async function (action) {
                const player = getState().player || action.player;

                // None of this is applicable if there is no player logged in:
                if (!player) {
                    return next(action);
                }

                const headers = { [authorisation_header]: player.token };

                if (action.type === signin_action_types.LOG_IN_SUCCESS) {
                    client.connect(headers, () => {
                        // Once the web socket is connected enter the lobby:
                        dispatch(enterLobby(player));
                    });
                    return next(action);
                }

                if (action.type === lobby_action_types.ENTER_LOBBY_SUCCESS) {
                    if (subscriptions.lobbyEntered) {
                        console.log("ENTER_LOBBY_SUCCESS has triggered a subscribe that is already present...");
                        return next(action);
                    }

                    subscriptions.lobbyEntered = client.subscribe("/topic/lobby/players/entered", message => {
                        if (message.command === "MESSAGE") {
                            const body = JSON.parse(message.body);
                            dispatch(otherEnteredLobby(body.playerRef))
                        }
                    }, headers);

                    subscriptions.lobbyLeft = client.subscribe("/topic/lobby/players/left", message => {
                        if (message.command === "MESSAGE") {
                            const body = JSON.parse(message.body);
                            dispatch(otherLeftLobby(body.playerRef))
                        }
                    }, headers);

                    subscriptions.gameStarted = client.subscribe(`/topic/player/${player.playerRef}/games/start`, message => {
                        if (message.command === "MESSAGE") {
                            const body = JSON.parse(message.body);
                            const {
                                players,
                                game
                            } = body;
                            const opponent = players.filter(p => player.playerRef !== p);

                            dispatch(gameStarted(opponent[0], game));
                        }
                    }, headers);
                }


                if (action.type === game_action_types.GAME_STARTED) {
                    if (subscriptions.moveMade) {
                        console.log("GAME_STARTED has triggered a subscribe that is already present...");
                        return next(action);
                    }

                    if (subscriptions.lobbyEntered) {
                        subscriptions.lobbyEntered.unsubscribe();
                        subscriptions.lobbyEntered = null;
                    }
                    if (subscriptions.lobbyLeft) {
                        subscriptions.lobbyLeft.unsubscribe();
                        subscriptions.lobbyLeft = null;
                    }
                    if (subscriptions.gameStarted) {
                        subscriptions.gameStarted.unsubscribe();
                        subscriptions.gameStarted = null;
                    }

                    const {
                        gameRef
                    } = action;

                    subscriptions.moveMade = client.subscribe(`/topic/game/${gameRef}/end`, message => {
                        if (message.command === "MESSAGE") {
                            dispatch(gameEnded());
                        }
                    }, headers);

                    subscriptions.gameEnded = client.subscribe(`/topic/game/${gameRef}/moves`, message => {
                        if (message.command === "MESSAGE") {
                            const body = JSON.parse(message.body);
                            dispatch(moveMade(body.mover));
                        }
                    }, headers);
                }


                if (action.type === game_action_types.LEAVE_GAME) {
                    if (!subscriptions.moveMade) {
                        console.log("LEAVE_GAME has triggered a unsubscribe that is not subscribed...");
                        return next(action);
                    }

                    if (subscriptions.moveMade) {
                        subscriptions.moveMade.unsubscribe();
                        subscriptions.moveMade = null;
                    }
                    if (subscriptions.gameEnded) {
                        subscriptions.gameEnded.unsubscribe();
                        subscriptions.gameEnded = null;
                    }
                }

                return next(action);
            }
        }
    }
}
import {game_action_types} from "../actions/gameActions";
import {signin_action_types} from "../actions/signInActions";
import {lobby_action_types} from "../actions/lobbyActions";

const initialState = {
    lobby: [],
    opponents: [],
    players: {},
};

function rootReducer(state = initialState, action) {

    switch (action.type) {
        case signin_action_types.LOG_IN_SUCCESS: {
            const { player } = action;

            return Object.assign({}, state, { player });
        }

        case signin_action_types.LOG_IN_FAILURE_USERNAME_IN_USE: {
            return Object.assign({}, state, { signin: { username_in_use: true }});
        }

        case signin_action_types.LOG_IN_FAILURE: {
            return Object.assign({}, state, { signin: { failure: true} });
        }

        case lobby_action_types.LOAD_LOBBY_SUCCESS: {
            return Object.assign({}, state, {
                lobby: action.lobby
            });
        }

        case lobby_action_types.OTHER_PLAYER_ENTERED_LOBBY: {
            const newLobby = new Set(state.lobby);

            newLobby.add(action.playerRef);

            return Object.assign({}, state, {
                lobby: [...newLobby]
            });
        }

        case lobby_action_types.OTHER_PLAYER_LEFT_LOBBY: {
            const newLobby = new Set(state.lobby);
            newLobby.delete(action.playerRef);

            return Object.assign({}, state, {
                lobby: [...newLobby]
            });
        }

        case lobby_action_types.PLAYER_LOAD_SUCCESS: {
            const players =  {...state.players};
            const { player } = action;

            players[player.playerRef] = player;

            return Object.assign({}, state, {
                players
            });
        }

        case lobby_action_types.LOAD_OPPONENTS_SUCCESS: {
            const { opponents } = action;

            return Object.assign({}, state, { opponents });
        }

        case game_action_types.GAME_STARTED: {
            const {
                gameRef,
                opponent
            } = action;

            return Object.assign({}, state, { game: { gameRef, opponent } });
        }

        case game_action_types.MAKE_MOVE_SUCCESS: // Making a move returns the new game state:
        case game_action_types.LOAD_GAME_SUCCESS: {
            return Object.assign({}, state, { game: { ...state.game, ...action.game } });
        }

        case game_action_types.LEAVE_GAME: {
            return Object.assign({}, state, { game: null });
        }

        default:
            return state;
    }
}

export default rootReducer;
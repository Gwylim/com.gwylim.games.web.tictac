package com.gwylim.games.web.tictac.service.game.model;

import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static com.gwylim.games.web.tictac.service.game.model.GameTestUtil.createDrawGame;
import static com.gwylim.games.web.tictac.service.game.model.GameTestUtil.createNewGame;
import static com.gwylim.games.web.tictac.service.game.model.GameTestUtil.createWonGame;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GameToGoTest {

    @Test
    public void whenWon() {
        // ARRANGE:
        final Game game = createWonGame();

        // ACT:
        final Optional<PlayerRef> toGo = game.toGo();

        // ASSERT:
        assertTrue(toGo.isEmpty());
    }

    @Test
    public void beforeWin() {
        // ARRANGE:
        final Game game = createNewGame();

        // ACT:
        final Optional<PlayerRef> toGo = game.toGo();

        // ASSERT:
        assertTrue(toGo.isPresent());
    }

    @Test
    public void whenDraw() {
        // ARRANGE:
        final Game game = createDrawGame();

        // ACT:
        final Optional<PlayerRef> toGo = game.toGo();

        // ASSERT:
        assertTrue(toGo.isEmpty());
    }
}
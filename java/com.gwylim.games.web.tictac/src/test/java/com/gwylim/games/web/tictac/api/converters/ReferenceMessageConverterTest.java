package com.gwylim.games.web.tictac.api.converters;

import com.google.common.base.Charsets;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReferenceMessageConverterTest {

    public static final ReferenceMessageConverter CONVERTER = new ReferenceMessageConverter();

    @Mock
    private HttpInputMessage inputMessage;

    @Mock
    private HttpOutputMessage outputMessage;


    @Mock
    private OutputStream outputStream;

    @Test
    public void canReadReferenceSubtype() {
        Assertions.assertTrue(CONVERTER.canRead(PlayerRef.class, MediaType.APPLICATION_JSON));
    }


    @Test
    public void readReferenceSubtype() throws IOException {
        final PlayerRef playerRef = PlayerRef.generateNew();
        when(inputMessage.getBody()).thenReturn(new ByteArrayInputStream(playerRef.getValue().getBytes(StandardCharsets.UTF_8)));

        assertEquals(playerRef, CONVERTER.read(PlayerRef.class, inputMessage));
    }

    @Test
    public void failsToCreateIncorrectlyPrefixedReference() throws IOException {
        when(inputMessage.getBody()).thenReturn(new ByteArrayInputStream("BadValue".getBytes(StandardCharsets.UTF_8)));

        assertThrows(HttpMessageNotReadableException.class, () ->
            CONVERTER.read(PlayerRef.class, inputMessage)
        );

    }

    @Test
    public void writeReferenceSubtype() throws IOException {
        final PlayerRef playerRef = PlayerRef.generateNew();

        when(outputMessage.getBody()).thenReturn(outputStream);

        CONVERTER.write(playerRef, MediaType.APPLICATION_JSON, outputMessage);

        verify(outputMessage, times(1)).getBody();
        verify(outputStream, times(1)).write(playerRef.getValue().getBytes(Charsets.UTF_8));
    }
}

package com.gwylim.games.web.tictac.service.game.model;

import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static com.gwylim.games.web.tictac.service.game.model.GameTestUtil.createDrawGame;
import static com.gwylim.games.web.tictac.service.game.model.GameTestUtil.createNewGame;
import static com.gwylim.games.web.tictac.service.game.model.GameTestUtil.createWonGame;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GameWinnerTest {

    @Test
    public void getWinnerWhenWon() {
        // ARRANGE
        final Game game = createWonGame();

        // ACT:
        final Optional<PlayerRef> winner = game.winner();

        // ASSERT:
        assertTrue(winner.isPresent());
    }

    @Test
    public void getWinnerBeforeWin() {
        // ARRANGE:
        final Game game = createNewGame();

        // ACT:
        final Optional<PlayerRef> winner = game.winner();

        // ASSERT:
        assertTrue(winner.isEmpty());
    }

    @Test
    public void getWinnerWhenDraw() {
        // ARRANGE:
        final Game game = createDrawGame();

        // ACT:
        final Optional<PlayerRef> winner = game.winner();

        // ASSERT:
        assertTrue(winner.isEmpty());
    }
}
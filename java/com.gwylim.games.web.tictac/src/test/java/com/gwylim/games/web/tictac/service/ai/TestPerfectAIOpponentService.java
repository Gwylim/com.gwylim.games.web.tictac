package com.gwylim.games.web.tictac.service.ai;

import java.util.Optional;

import com.gwylim.games.web.tictac.api.models.PlayerType;
import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.service.game.model.Game;
import com.gwylim.games.web.tictac.service.game.model.Players;
import com.gwylim.games.web.tictac.service.player.PlayerDetailsService;
import org.junit.jupiter.api.Test;

import static com.gwylim.games.web.tictac.service.ai.PerfectAIOpponentService.findForkingMoveFor;
import static com.gwylim.games.web.tictac.service.ai.PerfectAIOpponentService.findWinningMoveFor;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestPerfectAIOpponentService {
    public static final PlayerDetailsService PLAYER_SERVICE = new PlayerDetailsService();

    public static final PlayerRef AI = PLAYER_SERVICE.createPlayerDetails("A", PlayerType.AI, PlayerRef.generateNew()).getPlayerRef();
    public static final PlayerRef HU = PLAYER_SERVICE.createPlayerDetails("B", PlayerType.HUMAN, PlayerRef.generateNew()).getPlayerRef();
    public static final Players PLAYERS = new Players(AI, HU);

    @Test
    void testWinningMoveFinder() {
        final Game game = new Game(PLAYERS, AI, GameRef.generateNew())
            .makeMove(0, 0, AI)
            .makeMove(0, 1, HU)
            .makeMove(1, 0, AI)
            .makeMove(1, 1, HU);

        //  - - -
        //  H H -
        //  A A X << Expected

        final Optional<AIOpponentServiceBase.Move> winningMove = findWinningMoveFor(game, AI);
        assertTrue(winningMove.isPresent());
        assertEquals(2, winningMove.get().getX());
        assertEquals(0, winningMove.get().getY());
    }

    @Test
    void testForkingMoveFinder() {
        final Game game = new Game(PLAYERS, AI, GameRef.generateNew())
            .makeMove(0, 0, AI)
            .makeMove(0, 1, HU)
            .makeMove(2, 2, AI)
            .makeMove(1, 2, HU);

        //  - H A
        //  H - -
        //  A - X << Expected

        final Optional<AIOpponentServiceBase.Move> winningMove = findForkingMoveFor(game, AI);
        assertTrue(winningMove.isPresent());
        assertEquals(2, winningMove.get().getX());
        assertEquals(0, winningMove.get().getY());
    }
}

package com.gwylim.games.web.tictac.service.game.model;

import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;

final class GameTestUtil {

    private GameTestUtil() {
        // Util class
    }

    public static Game createDrawGame() {
        final Game game = createNewGame();

        final Players players = game.getPlayers();
        final PlayerRef a = game.toGo().orElseThrow();
        final PlayerRef b = players.getOther(a);

        // a b a
        // b a a
        // b a b

        return game.makeMove(0, 0, a)
                   .makeMove(1, 0, b)
                   .makeMove(2, 0, a)
                   .makeMove(0, 1, b)
                   .makeMove(1, 1, a)
                   .makeMove(0, 2, b)
                   .makeMove(1, 2, a)
                   .makeMove(2, 2, b)
                   .makeMove(2, 1, a);
    }

    public static Game createWonGame() {
        final Game game = createNewGame();

        final Players players = game.getPlayers();
        final PlayerRef a = game.toGo().orElseThrow();
        final PlayerRef b = players.getOther(a);

        // a a a
        // b . .
        // b . .

        return game.makeMove(0, 0, a)
                   .makeMove(0, 1, b)
                   .makeMove(1, 0, a)
                   .makeMove(0, 2, b)
                   .makeMove(2, 0, a);
    }

    public static Game createNewGame() {
        final Players players = new Players(PlayerRef.generateNew(), PlayerRef.generateNew());
        return new Game(players, GameRef.generateNew());
    }
}
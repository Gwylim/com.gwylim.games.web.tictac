package com.gwylim.games.web.tictac.api.converters;

import com.google.common.base.Charsets;
import com.gwylim.common.models.Meaningful;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MeaningfulMessageConverterTest {

    public static final MeaningfulMessageConverter CONVERTER = new MeaningfulMessageConverter();

    @Mock
    private HttpInputMessage inputMessage;

    @Mock
    private HttpOutputMessage outputMessage;


    @Mock
    private OutputStream outputStream;

    static class Name extends Meaningful<String> {
        public Name(final String name) {
            super(name);
        }
    }

    @Test
    public void canReadReferenceSubtype() {
        Assertions.assertTrue(CONVERTER.canRead(Name.class, MediaType.APPLICATION_JSON));
    }

    @Test
    public void readReferenceSubtype() throws IOException {
        final Name name = new Name("Some Name");
        when(inputMessage.getBody()).thenReturn(new ByteArrayInputStream(name.getValue().getBytes(StandardCharsets.UTF_8)));

        assertEquals(name, CONVERTER.read(Name.class, inputMessage));
    }

    @Test
    public void writeReferenceSubtype() throws IOException {
        final Name name = new Name("Some Name");

        when(outputMessage.getBody()).thenReturn(outputStream);

        CONVERTER.write(name, MediaType.APPLICATION_JSON, outputMessage);

        verify(outputMessage, times(1)).getBody();
        verify(outputStream, times(1)).write(name.getValue().getBytes(Charsets.UTF_8));
    }
}
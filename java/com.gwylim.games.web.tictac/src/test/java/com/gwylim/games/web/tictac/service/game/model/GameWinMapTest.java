package com.gwylim.games.web.tictac.service.game.model;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static com.gwylim.games.web.tictac.service.game.model.GameTestUtil.createNewGame;
import static com.gwylim.games.web.tictac.service.game.model.GameTestUtil.createWonGame;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class GameWinMapTest {

    @Test
    public void winRowsBeforeWin() {
        // ARRANGE:
        final Game game = createNewGame();

        // ACT:
        final Boolean[][] winMap = game.getWinMap();

        // ASSERT:
        final Boolean[][] expected = {
                {false, false, false},
                {false, false, false},
                {false, false, false},
        };

        assertArrayEquals(expected, winMap);
    }

    @Test
    public void winMapWhenWon() {
        // ARRANGE:
        final Game game = createWonGame();

        // ACT:
        final Boolean[][] winMap = game.getWinMap();

        // ASSERT:
        final Boolean[][] expected = {
                {true, false, false},
                {true, false, false},
                {true, false, false},
        };

        assertArrayEquals(expected, winMap,
            "Expected " + Arrays.deepToString(expected)
                  + " but was: " + Arrays.deepToString(winMap)
        );
    }

    @Test
    public void winMapWhenDraw() {
        // ARRANGE:
        final Game game = createNewGame();

        // ACT:
        final Boolean[][] winMap = game.getWinMap();

        // ASSERT:
        final Boolean[][] expected = {
                {false, false, false},
                {false, false, false},
                {false, false, false},
        };

        assertArrayEquals(expected, winMap);
    }
}
package com.gwylim.games.web.tictac.service;

import java.util.List;
import java.util.Set;

import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.events.GameStartedEvent;
import com.gwylim.games.web.tictac.events.LobbyEvents.OpponentSelectedEvent;
import com.gwylim.games.web.tictac.events.LobbyEvents.PlayerEnteredLobbyEvent;
import com.gwylim.games.web.tictac.events.LobbyEvents.PlayerLeftLobbyEvent;
import com.gwylim.games.web.tictac.service.game.GameDAO;
import com.gwylim.games.web.tictac.service.game.GameService;
import com.gwylim.system.events.api.EventBroadcaster;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

class LobbyServiceTest {

    @Test
    void opponentMatch_emits_event() {
        // ARRANGE
        final EventBroadcaster gameServiceEventBroadcaster = Mockito.mock(EventBroadcaster.class);
        final GameService gameService = new GameService(Mockito.mock(GameDAO.class), gameServiceEventBroadcaster);

        final EventBroadcaster lobbyServiceEventBroadcaster = Mockito.mock(EventBroadcaster.class);
        final LobbyService lobbyService = new LobbyService(lobbyServiceEventBroadcaster, gameService);

        final PlayerRef player1 = PlayerRef.generateNew();
        final PlayerRef player2 = PlayerRef.generateNew();

        lobbyService.enterPlayer(player1);
        lobbyService.enterPlayer(player2);

        // ACT
        lobbyService.addOpponentSelection(player1, player2);
        lobbyService.addOpponentSelection(player2, player1);

        // ASSERT

        // Lobby events:
        final ArgumentCaptor<Object> lobbyEventCaptor = ArgumentCaptor.forClass(GameStartedEvent.class);
        Mockito.verify(lobbyServiceEventBroadcaster, Mockito.times(6)).globalBroadcast(lobbyEventCaptor.capture());

        final List<Object> allValues = lobbyEventCaptor.getAllValues();
        if (allValues.get(0) instanceof PlayerEnteredLobbyEvent) {
            Assertions.assertEquals(((PlayerEnteredLobbyEvent) allValues.get(0)).getPlayerRef(), player1);
        }
        if (allValues.get(1) instanceof PlayerEnteredLobbyEvent) {
            Assertions.assertEquals(((PlayerEnteredLobbyEvent) allValues.get(1)).getPlayerRef(), player2);
        }
        if (allValues.get(2) instanceof OpponentSelectedEvent) {
            final OpponentSelectedEvent opponentSelectedEvent = (OpponentSelectedEvent) allValues.get(2);
            Assertions.assertEquals(opponentSelectedEvent.getPlayerRef(), player1);
            Assertions.assertEquals(opponentSelectedEvent.getOpponent(), player2);
        }
        if (allValues.get(3) instanceof OpponentSelectedEvent) {
            final OpponentSelectedEvent opponentSelectedEvent = (OpponentSelectedEvent) allValues.get(3);
            Assertions.assertEquals(opponentSelectedEvent.getPlayerRef(), player2);
            Assertions.assertEquals(opponentSelectedEvent.getOpponent(), player1);
        }
        if (allValues.get(4) instanceof PlayerLeftLobbyEvent) {
            Assertions.assertEquals(((PlayerLeftLobbyEvent) allValues.get(4)).getPlayerRef(), player2);
        }
        if (allValues.get(5) instanceof PlayerLeftLobbyEvent) {
            Assertions.assertEquals(((PlayerLeftLobbyEvent) allValues.get(5)).getPlayerRef(), player1);
        }

        // Game started event:
        final ArgumentCaptor<GameStartedEvent> gameEventCaptor = ArgumentCaptor.forClass(GameStartedEvent.class);
        Mockito.verify(gameServiceEventBroadcaster, Mockito.times(1)).globalBroadcast(gameEventCaptor.capture());

        final GameStartedEvent value = gameEventCaptor.getValue();
        final Set<PlayerRef> players = value.getPlayers();
        Assertions.assertEquals(players.size(), 2);
        Assertions.assertTrue(players.contains(player1));
        Assertions.assertTrue(players.contains(player2));
    }

}
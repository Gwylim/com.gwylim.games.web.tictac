package com.gwylim.games.web.tictac.service.game.model;

import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.service.player.model.PlayerDetails;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class Players {
    private final PlayerRef a;
    private final PlayerRef b;

    public Players(final PlayerRef a, final PlayerRef b) {
        if (a == null || b == null) {
            throw new IllegalArgumentException("Neither player can be null when creating Players object.");
        }

        this.a = a;
        this.b = b;
    }

    public PlayerRef getA() {
        return a;
    }

    public PlayerRef getB() {
        return b;
    }

    public PlayerRef getRandom() {
        return ThreadLocalRandom.current().nextBoolean() ? a : b;
    }

    public PlayerRef getOther(final PlayerRef playerRef) {
        if (a.equals(playerRef)) {
            return b;
        }
        else if (b.equals(playerRef)) {
            return a;
        }
        else {
            throw new IllegalArgumentException("Tried to get other player for '" + playerRef + "' not in players: " + toString());
        }
    }

    public boolean contains(final PlayerDetails playerDetails) {
        return contains(playerDetails.getPlayerRef());
    }

    public boolean contains(final PlayerRef playerRef) {
        return a.equals(playerRef) || b.equals(playerRef);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("a", a)
                .append("b", b)
                .toString();
    }

    public Stream<PlayerRef> stream() {
        return Stream.of(a, b);
    }

    public void forEach(final Consumer<PlayerRef> consumer) {
        stream().forEach(consumer);
    }
}

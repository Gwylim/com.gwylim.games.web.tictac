package com.gwylim.games.web.tictac.events.LobbyEvents;

import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.events.PlayerEvent;
import com.gwylim.system.events.api.annotations.Subject;

public class OpponentSelectionEvent extends PlayerEvent {
    private final PlayerRef opponent;

    public OpponentSelectionEvent(final PlayerRef playerRef, final PlayerRef opponent) {
        super(playerRef);
        this.opponent = opponent;
    }

    @Subject
    public PlayerRef getOpponent() {
        return opponent;
    }
}

package com.gwylim.games.web.tictac.service.game.model;

import com.gwylim.games.web.tictac.api.models.references.GameRef;

public class GameException extends RuntimeException {
    private final GameRef gameRef;

    public GameException(final GameRef gameRef, final String message) {
        super(message);
        this.gameRef = gameRef;
    }

    public GameRef getGameRef() {
        return gameRef;
    }
}

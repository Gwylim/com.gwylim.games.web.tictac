package com.gwylim.games.web.tictac.service.game;

import com.gwylim.common.exceptions.NotFoundException;
import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.events.GameEndedEvent;
import com.gwylim.games.web.tictac.events.GameMoveMadeEvent;
import com.gwylim.games.web.tictac.events.GameStartedEvent;
import com.gwylim.games.web.tictac.events.PlayerDisconnectedEvent;
import com.gwylim.games.web.tictac.service.game.model.Game;
import com.gwylim.games.web.tictac.service.game.model.GameException;
import com.gwylim.games.web.tictac.service.game.model.Players;
import com.gwylim.system.events.api.EventBroadcaster;
import com.gwylim.system.events.api.annotations.EventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
@Scope("singleton")
public class GameService {
    private static final Logger LOG = LoggerFactory.getLogger(GameService.class);

    private final GameDAO gameDAO;
    private final EventBroadcaster eventBroadcaster;

    @Autowired
    public GameService(final GameDAO gameDAO,
                       final EventBroadcaster eventBroadcaster) {
        this.gameDAO = gameDAO;

        this.eventBroadcaster = eventBroadcaster;
    }

    public GameRef newGame(final PlayerRef a, final PlayerRef b) {
        if (a.equals(b)) {
            throw new IllegalArgumentException("A player cannot play itself: " + a);
        }

        LOG.info("New game created between: " + a + " and " + b);
        final GameRef gameRef = GameRef.generateNew();
        final Game game = new Game(new Players(a, b), gameRef);
        gameDAO.add(game);

        eventBroadcaster.globalBroadcast(new GameStartedEvent(game));

        return gameRef;
    }

    @EventListener
    public void handlePlayerDisconnected(final PlayerDisconnectedEvent event) {
        final Set<Game> games = gameDAO.getGameForPlayer(event.getPlayerRef());

        games.forEach(game -> gameTerminated(game.getGameRef()));
    }

    /**
     * Indicate game has terminated, this is idempotent.
     * If the game was present a GameEndedEvent will be broadcast.
     *
     * @param gameRef The game that just terminated.
     */
    public synchronized void gameTerminated(final GameRef gameRef) {
        LOG.info("Game terminated: " + gameRef);

        gameDAO.get(gameRef).ifPresent(game -> {
            final Game updatedGame = gameDAO.updateGame(game.terminate());

            eventBroadcaster.globalBroadcast(new GameEndedEvent(updatedGame));
        });
    }

    public void makeMove(final int x, final int y, final PlayerRef playerRef, final GameRef gameRef) throws GameException {
        final Game updatedGame = gameDAO.updateGame(getGame(gameRef).makeMove(x, y, playerRef));

        eventBroadcaster.globalBroadcast(new GameMoveMadeEvent(updatedGame, playerRef));
        if (updatedGame.toGo().isEmpty()) {
            gameTerminated(gameRef);
        }
    }

    public Game getGame(final GameRef gameRef) {
        return gameDAO.get(gameRef)
                      .orElseThrow(() -> new NotFoundException(gameRef));
    }
}

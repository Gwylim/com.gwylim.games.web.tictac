package com.gwylim.games.web.tictac.service.game.model;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.gwylim.games.web.tictac.api.models.references.PlayerRef;

public final class Line {
    private final Set<Square> squares;

    Line(final Stream<Square> squares) {
        this.squares = squares.collect(Collectors.toSet());
    }

    public Stream<Square> getSquares() {
        return squares.stream();
    }

    Optional<PlayerRef> getWinningPlayer() {
        return squares.stream().map(Square::getPlayer).reduce((a, b) -> a.equals(b) ? a : Optional.empty()).filter(Optional::isPresent) // Just in-case the row had zero squares /s...
                      .map(Optional::get);
    }

    @Override
    public String toString() {
        return "L" + squares;
    }
}

package com.gwylim.games.web.tictac.service.player.model;

import com.gwylim.games.web.tictac.api.models.PlayerType;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;

public class PlayerDetails {
    private final PlayerRef playerRef;

    private final String name;
    private final PlayerType type;
    private final String icon;


    public PlayerDetails(final String name,
                  final PlayerType type,
                  final String icon,
                  final PlayerRef playerRef) {

        this.name = name;
        this.type = type;
        this.icon = icon;
        this.playerRef = playerRef;
    }

    public String getName() {
        return name;
    }

    public PlayerType getType() {
        return type;
    }

    public PlayerRef getPlayerRef() {
        return playerRef;
    }

    public String getIcon() {
        return icon;
    }

    @Override
    public String toString() {
        return getName();
    }

}

package com.gwylim.games.web.tictac.service.ai;

import com.google.common.collect.ImmutableList;
import com.gwylim.common.ThreadUtil;
import com.gwylim.games.web.tictac.api.models.PlayerType;
import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.events.GameEndedEvent;
import com.gwylim.games.web.tictac.events.GameMoveMadeEvent;
import com.gwylim.games.web.tictac.events.GameStartedEvent;
import com.gwylim.games.web.tictac.events.LobbyEvents.LobbyPlayersChangedEvent;
import com.gwylim.games.web.tictac.events.LobbyEvents.OpponentSelectedEvent;
import com.gwylim.games.web.tictac.service.LobbyService;
import com.gwylim.games.web.tictac.service.game.GameService;
import com.gwylim.games.web.tictac.service.game.model.Game;
import com.gwylim.games.web.tictac.service.game.model.Square;
import com.gwylim.games.web.tictac.service.player.PlayerDetailsService;
import com.gwylim.system.events.api.EventSystem;
import com.gwylim.system.events.api.Listener;
import com.gwylim.system.events.api.annotations.EventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Service that provides AI opponents that will automatically play against
 * players who select them in the lobby.
 */
public abstract class AIOpponentServiceBase {
    private static final Logger LOG = LoggerFactory.getLogger(AIOpponentServiceBase.class);

    public static final int MINIMUM_DELAY_MS = 100;
    public static final int MAXIMUM_DELAY_MS = 1500;

    private final GameService gameService;
    private final LobbyService lobbyService;
    private final PlayerDetailsService playerDetailsService;
    private final EventSystem globalEventSystem;

    private final AtomicInteger count = new AtomicInteger();
    private final Map<PlayerRef, List<Listener<?>>> playingPlayers = new LinkedHashMap<>();
    private final Map<PlayerRef, Listener<GameStartedEvent>> lobbyPlayers = new LinkedHashMap<>();

    public AIOpponentServiceBase(final GameService gameService,
                                 final LobbyService lobbyService,
                                 final PlayerDetailsService playerDetailsService,
                                 final EventSystem globalEventSystem) {
        this.gameService = gameService;
        this.lobbyService = lobbyService;
        this.playerDetailsService = playerDetailsService;
        this.globalEventSystem = globalEventSystem;
    }

    /**
     * Check that there is always an AI player in the Lobby at all times.
     */
    @EventListener
    public final synchronized void lobbyPlayersChanged(final LobbyPlayersChangedEvent event) {
        addPlayerToLobbyIfNeeded();
    }

    /**
     * Add a player to the lobby of this type.
     * Called @PostConstruct so that the lobby is pre-populated.
     */
    @PostConstruct
    public void addPlayerToLobbyIfNeeded() {
        if (lobbyPlayers.isEmpty()) {
            LOG.info("Creating new AI player.");
            // TODO: Should not need to generateRef for AI player?
            // TODO: This should be a player without auth, when we have session management a single PlayerDetails / PlayerRef Will be reused.
            final PlayerRef newPlayerRef = playerDetailsService.createPlayerDetails(name() + " " + count.getAndIncrement(), PlayerType.AI, PlayerRef.generateNew()).getPlayerRef();
            this.lobbyPlayers.put(
                    newPlayerRef,
                    globalEventSystem.register(Listener.of(GameStartedEvent.class, gse -> startGame(gse.getGameRef(), newPlayerRef), newPlayerRef))
            );
            // Enter the player in the lobby after putting them in the map so this method is idempotent.
            lobbyService.enterPlayer(newPlayerRef);
        }
    }

    /**
     * When someone selects one of the AI players, select them back.
     * Use selection so that we don't have to enumerate all players in the lobby and select them all.
     */
    @EventListener
    public final void opponentSelected(final OpponentSelectedEvent event) {
        if (lobbyPlayers.containsKey(event.getOpponent())) {
            LOG.info("AI slecting opponenet back: " + event.getOpponent());
            lobbyService.addOpponentSelection(event.getOpponent(), event.getPlayerRef());
        }
    }


    private void startGame(final GameRef gameRef, final PlayerRef playerRef) {
        LOG.info("Game started for " + playerRef);
        globalEventSystem.unregister(this.lobbyPlayers.remove(playerRef));

        playingPlayers.put(playerRef, ImmutableList.of(
            globalEventSystem.register(Listener.of(GameMoveMadeEvent.class, gmme -> makeMove(gmme.getGameRef(), playerRef), gameRef)),
            globalEventSystem.register(Listener.of(GameEndedEvent.class, gee -> gameEnded(playerRef), gameRef))
        ));

        // Try to make a move:
        makeMove(gameRef, playerRef);
    }

    /**
     * When a game ends clean up the listeners for the playerRef.
     */
    private void gameEnded(final PlayerRef playerRef) {
        LOG.info("Game ended for " + playerRef);

        Optional.ofNullable(playingPlayers.remove(playerRef))
                .ifPresent(listeners ->
                        listeners.forEach(globalEventSystem::unregister)
                );
        // PlayerRef is discarded.
    }

    /**
     * Make a move if it is our turn.
     */
    private void makeMove(final GameRef gameRef, final PlayerRef playerRef) {
        final Game game = gameService.getGame(gameRef);
        if (game.toGo().map(playerRef::equals).orElse(false)) {
            ThreadUtil.run(shortDelay(), () -> {
                        final Move move = chooseMove(game, playerRef);
                        final int x = move.x;
                        final int y = move.y;
                        gameService.makeMove(x, y, playerRef, gameRef);
                    }
            );
        }
    }

    /**
     * Get the name of this AI type.
     * This will be suffixed with the instance number.
     */
    abstract String name();

    /**
     * Choose a location to play.
     * @param game      The game we are playing.
     * @param playerRef Our playerRef.
     * @return          The coordinate of the position we wish to play.
     */
    abstract Move chooseMove(Game game, PlayerRef playerRef);

    public static final class Move {
        private final int x;
        private final int y;

        Move(final int x, final int y) {
            this.x = x;
            this.y = y;
        }

        Move(final Square square) {
            this.x = square.getX();
            this.y = square.getY();
        }

        public int getX() { return x; }
        public int getY() { return y; }
    }

    private Duration shortDelay() {
        return Duration.of(ThreadLocalRandom.current().nextInt(MINIMUM_DELAY_MS, MAXIMUM_DELAY_MS), ChronoUnit.MILLIS);
    }
}

package com.gwylim.games.web.tictac.events;

import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.system.events.api.annotations.Subject;

public abstract class PlayerEvent {

    private final PlayerRef playerRef;

    public PlayerEvent(final PlayerRef playerRef) {
        this.playerRef = playerRef;
    }

    @Subject
    public PlayerRef getPlayerRef() {
        return playerRef;
    }
}

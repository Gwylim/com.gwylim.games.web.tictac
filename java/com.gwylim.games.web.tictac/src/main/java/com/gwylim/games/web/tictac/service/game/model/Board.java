package com.gwylim.games.web.tictac.service.game.model;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import com.gwylim.games.web.tictac.api.models.references.PlayerRef;

public class Board {

    @Nonnull
    private final PlayerRef[][] board;

    private final List<Line> lines;

    /**
     * Create an empty board.
     */
    public Board() {
        this(new PlayerRef[][] {
            {null, null, null},
            {null, null, null},
            {null, null, null}
        });
    }

    private Board(@Nonnull final PlayerRef[][] boardState) {
        board = boardState;

        lines = generateLines();
    }

    public Board makeMove(final PlayerRef player, final int x, final int y) {
        final PlayerRef[][] newBoard = board.clone();
        newBoard[x][y] = player;
        return new Board(newBoard);
    }

    public PlayerRef[][] getBoard() {
        return board.clone();
    }

    /**
     * Get the dimensions of the board.
     */
    public int getXDim() {
        return board.length;
    }

    public int getYDim() {
        return board[0].length;
    }

    public Optional<PlayerRef> get(final int x, final int y) {
        return Optional.ofNullable(board[x][y]);
    }

    /**
     * Lines exposed to help with AI.
     */
    public List<Line> getLines() {
        return new ArrayList<>(lines);
    }


    @SuppressWarnings("SuspiciousNameCombination")
    private List<Line> generateLines() {
        return Stream.concat(
            IntStream.range(0, getXDim()).boxed().flatMap(x ->
                Stream.of(new Line(IntStream.range(0, getYDim()).mapToObj(y -> new Square(get(x, y).orElse(null), x, y))),
                    new Line(IntStream.range(0, getYDim()).mapToObj(y -> new Square(get(y, x).orElse(null), y, x))))
            ),
            Stream.of(
                new Line(IntStream.range(0, getYDim()).mapToObj(y -> new Square(get(y, y).orElse(null), y, y))),
                new Line(IntStream.range(0, getYDim()).mapToObj(y -> new Square(get(y, 2 - y).orElse(null), y, 2 - y)))
            )
        ).collect(Collectors.toList());
    }
}

package com.gwylim.games.web.tictac.service;

import com.google.common.collect.ImmutableSet;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.events.LobbyEvents.OpponentSelectedEvent;
import com.gwylim.games.web.tictac.events.LobbyEvents.OpponentUnselectedEvent;
import com.gwylim.games.web.tictac.events.LobbyEvents.PlayerEnteredLobbyEvent;
import com.gwylim.games.web.tictac.events.LobbyEvents.PlayerLeftLobbyEvent;
import com.gwylim.games.web.tictac.events.PlayerDisconnectedEvent;
import com.gwylim.games.web.tictac.service.game.GameService;
import com.gwylim.system.events.api.EventBroadcaster;
import com.gwylim.system.events.api.annotations.EventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;

@Component
@Scope("singleton")
public class LobbyService {
    private static final Logger LOG = LoggerFactory.getLogger(LobbyService.class);

    private final EventBroadcaster eventBroadcaster;
    private final GameService gameService;

    private final Map<PlayerRef, Set<PlayerRef>> currentLobbyPlayers = new ConcurrentHashMap<>();

    @Autowired
    public LobbyService(final EventBroadcaster eventBroadcaster,
                        final GameService gameService) {
        this.eventBroadcaster = eventBroadcaster;
        this.gameService = gameService;
    }

    public void enterPlayer(final PlayerRef playerRef) {
        LOG.info("Adding player: " + playerRef + " to lobby.");
        currentLobbyPlayers.put(playerRef, Collections.newSetFromMap(new WeakHashMap<>()));

        eventBroadcaster.globalBroadcast(new PlayerEnteredLobbyEvent(playerRef));
    }

    public void exitPlayer(final PlayerRef playerRef) {
        LOG.info("PlayerDisconnectedEvent: Removing player: " + playerRef + " from lobby.");
        playersLeftLobby(Collections.singleton(playerRef));
    }

    private void playersLeftLobby(final Set<PlayerRef> playerRefs) {
        playerRefs.forEach(p -> {
            currentLobbyPlayers.remove(p);
            eventBroadcaster.globalBroadcast(new PlayerLeftLobbyEvent(p));
        });
    }

    public Set<PlayerRef> getPlayers() {
        return new LinkedHashSet<>(currentLobbyPlayers.keySet());
    }
    public Set<PlayerRef> getOpponentsForPlayer(final PlayerRef playerRef) {
        return new LinkedHashSet<>(currentLobbyPlayers.getOrDefault(playerRef, Collections.emptySet()));
    }

    @EventListener
    public void handlePlayerDisconnected(final PlayerDisconnectedEvent event) {
        exitPlayer(event.getPlayerRef());
    }

    /**
     * For internal use, get the opponents for a player if they are in the lobby.
     * Return a new empty set if not.
     */
    private Set<PlayerRef> safeOpponents(final PlayerRef playerRef) {
        return currentLobbyPlayers.getOrDefault(playerRef, new LinkedHashSet<>());
    }

    public void addOpponentSelection(final PlayerRef playerRef, final PlayerRef newOpponent) {
        if (playerRef.equals(newOpponent)) {
            throw new IllegalArgumentException("A player cannot play itself: " + playerRef);
        }

        final Set<PlayerRef> opponents = safeOpponents(playerRef);
        if (opponents.add(newOpponent)) {
            LOG.info(playerRef + " selected opponent " + newOpponent + " new opponents: " + opponents);
            eventBroadcaster.globalBroadcast(new OpponentSelectedEvent(playerRef, newOpponent));
        }

        if (safeOpponents(newOpponent).contains(playerRef)) {
            startGame(playerRef, newOpponent);
        }
    }

    public void removeOpponentSelection(final PlayerRef playerRef, final PlayerRef opponent) {
        final Set<PlayerRef> opponents = safeOpponents(playerRef);
        if (opponents.remove(opponent)) {
            LOG.info(playerRef + " unselected opponent " + opponent + " new opponents: " + opponents);
            eventBroadcaster.globalBroadcast(new OpponentUnselectedEvent(playerRef, opponent));
        }
    }


    private void startGame(final PlayerRef playerRef, final PlayerRef opponent) {
        LOG.info("Match found:" + playerRef + " vs " + opponent);

        gameService.newGame(playerRef, opponent);
        playersLeftLobby(ImmutableSet.of(playerRef, opponent));
    }
}

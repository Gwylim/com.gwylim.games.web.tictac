package com.gwylim.games.web.tictac.service.player.auth.schema;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(name = "player_auth", indexes = {
    @Index(name = "player_auth_player_ref_unique", columnList = "player_ref", unique = true),
    @Index(name = "player_auth_username_unique", columnList = "username", unique = true)
})
@SuppressWarnings({"unused", "JpaDataSourceORMInspection"}) // This is used to generate schema.
public class PlayerAuthTable {

    @Id
    @Column(name = "player_ref")
    private String playerRef;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;
}
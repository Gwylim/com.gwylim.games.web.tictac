package com.gwylim.games.web.tictac.events.LobbyEvents;

import com.gwylim.games.web.tictac.api.models.references.PlayerRef;

public class OpponentSelectedEvent extends OpponentSelectionEvent {
    public OpponentSelectedEvent(final PlayerRef playerRef, final PlayerRef opponent) {
        super(playerRef, opponent);
    }
}

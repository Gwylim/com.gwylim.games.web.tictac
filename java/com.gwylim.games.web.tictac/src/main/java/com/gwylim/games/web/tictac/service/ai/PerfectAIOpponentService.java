package com.gwylim.games.web.tictac.service.ai;

import com.google.common.collect.Sets;
import com.gwylim.common.ArrayUtil;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.service.LobbyService;
import com.gwylim.games.web.tictac.service.game.GameService;
import com.gwylim.games.web.tictac.service.game.model.Board;
import com.gwylim.games.web.tictac.service.game.model.Game;
import com.gwylim.games.web.tictac.service.game.model.Line;
import com.gwylim.games.web.tictac.service.game.model.Square;
import com.gwylim.games.web.tictac.service.player.PlayerDetailsService;
import com.gwylim.system.events.api.EventSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.gwylim.common.StreamUtil.randomly;

/**
 * Perfect AI opponent using algorithm.
 * Described here: https://en.wikipedia.org/wiki/Tic-tac-toe#Strategy
 */
@Component
@Scope("singleton")
public class PerfectAIOpponentService extends AIOpponentServiceBase {
    private static final Logger LOG = LoggerFactory.getLogger(PerfectAIOpponentService.class);

    @Autowired
    public PerfectAIOpponentService(final GameService gameService,
                                    final LobbyService lobbyService,
                                    final PlayerDetailsService playerDetailsService,
                                    final EventSystem eventSystem) {
        super(gameService, lobbyService, playerDetailsService, eventSystem);
    }

    @Override
    String name() {
        return "Perfect AI";
    }

    Move chooseMove(final Game game, final PlayerRef playerRef) {
        return findMove(game, playerRef);
    }

    private static Move findMove(final Game game, final PlayerRef playerRef) {
        final PlayerRef opponent = game.getPlayers().getOther(playerRef);

        // Win: If the playerRef has two in a row, they can place a third to get three in a row.
        final Optional<Move> winningMove = findWinningMoveFor(game, playerRef);
        if (winningMove.isPresent()) {
            return winningMove.get();
        }

        // Block: If the opponent has two in a row, the playerRef must play the third themselves to block the opponent.
        final Optional<Move> blockingMove = findWinningMoveFor(game, opponent);
        if (blockingMove.isPresent()) {
            return blockingMove.get();
        }

        // Fork: Create an opportunity where the playerRef has two threats to win (two non-blocked lines of 2).
        final Optional<Move> ourForkingMove = findForkingMoveFor(game, playerRef);
        if (ourForkingMove.isPresent()) {
            return ourForkingMove.get();
        }

        // Blocking an opponent's fork: If there is only one possible fork for the opponent, the playerRef should block it.
        // TODO: Otherwise, the playerRef should block any forks in any way that simultaneously allows them to create two in a row.
        // TODO: Otherwise, the playerRef should create a two in a row to force the opponent into defending, as long as it doesn't result in them creating a fork.
        final Optional<Move> theirForkingMove = findForkingMoveFor(game, opponent);
        if (theirForkingMove.isPresent()) {
            return theirForkingMove.get();
        }

        final Board board = game.getBoard();

        // Center: A playerRef marks the center.
        if (board.get(1, 1).isEmpty()) {
            return new Move(1, 1);
        }

        // Opposite corner: If the opponent is in the corner, the playerRef plays the opposite corner.
        if (board.get(0, 0).map(opponent::equals).orElse(false) && board.get(2, 2).isEmpty()) {
            return new Move(2, 2);
        }
        if (board.get(2, 2).map(opponent::equals).orElse(false) && board.get(0, 0).isEmpty()) {
            return new Move(0, 0);
        }
        if (board.get(2, 0).map(opponent::equals).orElse(false) && board.get(0, 2).isEmpty()) {
            return new Move(0, 2);
        }
        if (board.get(0, 2).map(opponent::equals).orElse(false) && board.get(2, 0).isEmpty()) {
            return new Move(2, 0);
        }

        // Empty corner: The playerRef plays in a corner square.
        final Optional<Move> randomEmptyCorner = Stream.of(new Move(2, 2), new Move(0, 0), new Move(0, 2), new Move(2, 0))
                .filter(m -> board.getBoard()[m.getX()][m.getY()] == null)
                .min(randomly());
        if (randomEmptyCorner.isPresent()) {
            return randomEmptyCorner.get();
        }

        // Empty side: The playerRef plays in a middle square on any of the 4 sides.
        final Optional<Move> randomEmptyEdge = Stream.of(new Move(0, 1), new Move(1, 2), new Move(2, 1), new Move(1, 0))
                .filter(m -> board.getBoard()[m.getX()][m.getY()] == null)
                .min(randomly());
        if (randomEmptyEdge.isPresent()) {
            return randomEmptyEdge.get();
        }
        else {
            LOG.warn("Could not find move using perfect strategy, choosing randomly move");
            return ArrayUtil.stream(board.getBoard())
                    .filter(v -> v.getValue() == null)
                    .sorted(randomly())
                    .map(v -> new Move(v.getX(), v.getY()))
                    .findFirst()
                    .orElseThrow(() -> new RuntimeException("No move to make."));
        }
    }

    public static Optional<Move> findWinningMoveFor(final Game game, final PlayerRef playerRef) {
        return game.getBoard().getLines().stream()
                .map(l -> findWinningMoveFor(l, playerRef))
                .filter(Optional::isPresent)
                .findFirst()
                .map(Optional::get);
    }

    /**
     * Find move that will complete a for the given playerRef.
     */
    public static Optional<Move> findWinningMoveFor(final Line line, final PlayerRef playerRef) {
        final long ourSquares = line.getSquares()
                .filter(s -> isPlayersSquare(s, playerRef))
                .count();

        if (ourSquares == 2) {
            return line.getSquares()
                .filter(s -> s.getPlayer().isEmpty())
                .findAny()
                .map(Move::new);
        }
        else {
            return Optional.empty();
        }
    }

    // Blocking an opponent's fork: If there is only one possible fork for the opponent, the playerRef should block it.
    // TODO: Otherwise, the playerRef should block any forks in any way that simultaneously allows them to create two in a row.
    // TODO: Otherwise, the playerRef should create a two in a row to force the opponent into defending, as long as it doesn't result in them creating a fork.
    public static Optional<Move> findForkingMoveFor(final Game game, final PlayerRef playerRef) {
        // Find all lines which are empty except for one of our pieces
        final Set<Line> singlePieceLines = game.getBoard().getLines().stream()
                .filter(l -> l.getSquares().filter(s -> isPlayersSquare(s, playerRef)).count() == 1)
                .filter(l -> l.getSquares().filter(Square::isEmpty).count() == 2)
                .collect(Collectors.toSet());

        // Find an empty intersection between any of those lines
        final Set<Square> forkCreationSpots = Sets.cartesianProduct(singlePieceLines, singlePieceLines).stream()
                                                  .flatMap(PerfectAIOpponentService::intersection)
                                                  .filter(Square::isEmpty)
                                                  .collect(Collectors.toSet());

        if (forkCreationSpots.size() == 0) {
            return Optional.empty();

        }

        // If there is only one possible fork for the opponent, the playerRef should block it:
        if (forkCreationSpots.size() == 1) {
            return forkCreationSpots.stream().findFirst().map(Move::new);
        }

        // PlayerRef should block any forks in any way that simultaneously allows them to create two in a row.
        // PlayerRef should create a two in a row to force the opponent into defending, as long as it doesn't result in them creating a fork.

        // Find all locations the opposition could go that would provide two-in a row
        final Set<Square> twoInARowSquares = game.getBoard().getLines().stream()
                                                 .filter(l -> l.getSquares().filter(s -> isPlayersSquare(s, game.getPlayers().getOther(playerRef))).count() == 1)
                                                 .filter(l -> l.getSquares().filter(Square::isEmpty).count() == 2)
                                                 .flatMap(l -> l.getSquares().filter(Square::isEmpty))
                                                 .collect(Collectors.toSet());

        // Prefer any that are in the set of blocking moves
        final Optional<Square> blockingTwoInARow = Sets.intersection(twoInARowSquares, forkCreationSpots).stream()
                                                       .min(randomly());
        if (blockingTwoInARow.isPresent()) {
            return blockingTwoInARow.map(Move::new);
        }
        else {
            // If none are, choose at random
            return twoInARowSquares.stream()
                                   .sorted(randomly())
                                   .map(Move::new).findFirst();
        }
    }

    private static Stream<Square> intersection(final List<Line> lines) {
        return lines.stream()
                .map(l -> l.getSquares().collect(Collectors.toSet()))
                .reduce(Sets::intersection)
                .filter(p -> p.size() == 1)
                .orElse(Collections.emptySet())
                .stream();
    }

    private static Boolean isPlayersSquare(final Square s, final PlayerRef playerRef) {
        return s.getPlayer().map(playerRef::equals).orElse(false);
    }

}

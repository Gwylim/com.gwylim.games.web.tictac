package com.gwylim.games.web.tictac.events.LobbyEvents;

import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.events.PlayerEvent;

public abstract class LobbyPlayersChangedEvent extends PlayerEvent {
    public LobbyPlayersChangedEvent(final PlayerRef playerRef) {
        super(playerRef);
    }
}

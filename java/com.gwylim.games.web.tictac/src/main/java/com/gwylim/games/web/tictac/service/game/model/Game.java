package com.gwylim.games.web.tictac.service.game.model;

import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;

import javax.annotation.CheckReturnValue;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Game {
    private final GameRef gameRef;
    private final Players players;

    @Nullable
    private final PlayerRef toGo;
    @Nullable
    private final PlayerRef winner;

    @Nonnull
    private final Board board;

    private final Boolean[][] winMap = new Boolean[][] {
            {false, false, false},
            {false, false, false},
            {false, false, false}
    };

    public Game(final Players players, final GameRef gameRef) {
        this(players, players.getRandom(), gameRef);
    }

    public Game(final Players players, @Nullable final PlayerRef toGo, final GameRef gameRef) {
        this(players, new Board(), toGo, gameRef);
    }

    public Game(final Players players, final Board board, final PlayerRef toGo, final GameRef gameRef) {
        this.players = players;
        this.gameRef = gameRef;
        this.board = board;

        this.winner = checkForWin().orElse(null);

        if (winner().isPresent()) {
            this.toGo = null;
        }
        else {
            this.toGo = checkForDraw() ? null : toGo;
        }
    }

    public Board getBoard() {
        return board;
    }

    /**
     * Get the dimensions of the board.
     */
    public int getXDim() {
        return board.getXDim();
    }

    public int getYDim() {
        return board.getYDim();
    }

    public Players getPlayers() {
        return players;
    }

    public GameRef getGameRef() {
        return gameRef;
    }

    public Optional<PlayerRef> toGo() {
        return Optional.ofNullable(toGo);
    }

    public Optional<PlayerRef> winner() {
        return Optional.ofNullable(winner);
    }

    /**
     * Get a map of the board indicating which squares contributed to the win.
     */
    public Boolean[][] getWinMap() {
        return winMap.clone();
    }

    @CheckReturnValue
    public Game terminate() {
        return new Game(players, board, null, gameRef);
    }

    @CheckReturnValue
    public Game makeMove(final int x, final int y, final PlayerRef playerRef) throws GameException {
        if (!playerRef.equals(toGo)) {
            throw new GameException(gameRef, "PlayerRef " + playerRef + " attempted to make a move when not their turn.");
        }

        if (board.get(x, y).isEmpty()) {
            final Board newBoard = this.board.makeMove(playerRef, x, y);
            return new Game(players, newBoard, players.getOther(toGo), gameRef);
        }
        else {
            throw new GameException(gameRef, "Player " + playerRef + " attempted to make a move into non-empty square: " + x + "," + y);
        }
    }

    private boolean checkForDraw() {
        // Seek an empty square:
        return IntStream.range(0, getXDim())
                .boxed()
                .flatMap(x -> IntStream.range(0, getYDim())
                                       .mapToObj(y -> board.get(x, y).isPresent()))
                .reduce(true, (a, b) -> a && b);
    }


    private Optional<PlayerRef> checkForWin() {
        // Check all lines for being all one player. i.e wins the game:
        final List<Line> winningLines = board.getLines().stream()
                                             .filter(l -> l.getWinningPlayer().isPresent())
                                             .collect(Collectors.toList());

        // Update win map:
        winningLines.forEach(r ->
            r.getSquares().forEach(s -> winMap[s.getX()][s.getY()] = true)
        );

        // Update winner:
        return winningLines.stream()
                .findFirst()
                .flatMap(Line::getWinningPlayer);
    }

    @Override
    public String toString() {
        return "Game between " + players;
    }

    /*  Classes to support win detection:  */

}

package com.gwylim.games.web.tictac.events.LobbyEvents;

import com.gwylim.games.web.tictac.api.models.references.PlayerRef;

public class OpponentUnselectedEvent extends OpponentSelectionEvent {
    public OpponentUnselectedEvent(final PlayerRef playerRef, final PlayerRef opponent) {
        super(playerRef, opponent);
    }
}

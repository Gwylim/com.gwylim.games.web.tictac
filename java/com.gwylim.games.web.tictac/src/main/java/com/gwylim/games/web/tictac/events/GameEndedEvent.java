package com.gwylim.games.web.tictac.events;

import com.gwylim.games.web.tictac.service.game.model.Game;

public class GameEndedEvent extends GameEvent {
    public GameEndedEvent(final Game game) {
        super(game);
    }
}

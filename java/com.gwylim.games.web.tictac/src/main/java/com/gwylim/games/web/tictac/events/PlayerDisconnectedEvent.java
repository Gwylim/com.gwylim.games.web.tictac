package com.gwylim.games.web.tictac.events;

import com.gwylim.games.web.tictac.api.models.references.PlayerRef;

public class PlayerDisconnectedEvent extends PlayerEvent {
    public PlayerDisconnectedEvent(final PlayerRef playerRef) {
        super(playerRef);
    }
}

package com.gwylim.games.web.tictac.service.game;

import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.service.game.model.Game;
import com.gwylim.games.web.tictac.service.game.model.Players;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.WeakHashMap;

@Component
@Scope("singleton")
public class GameDAO {
    private final Map<PlayerRef, GameRef> gamesByPlayer = new LinkedHashMap<>();

    /* If there are no references then the game cannot be retrieved. */
    private final Map<GameRef, Game> games = new WeakHashMap<>();

    public synchronized void add(final Game game) {
        game.getPlayers().forEach(player -> gamesByPlayer.put(player, game.getGameRef()));
        games.put(game.getGameRef(), game);
    }

    public Optional<Game> get(final GameRef gameRef) {
        return Optional.ofNullable(games.get(gameRef));
    }

    public synchronized Set<Game> getGameForPlayer(final PlayerRef playerRef) {
        return Optional.ofNullable(gamesByPlayer.get(playerRef))
                       .map(games::get)
                       .map(Set::of)
                       .orElse(Collections.emptySet());
    }

    /**
     * If something has a reference it should not be removed.
     */
    @Deprecated
    public synchronized void removeGame(final GameRef gameRef) {
        get(gameRef).stream()
            .map(Game::getPlayers)
            .flatMap(Players::stream)
            .forEach(gamesByPlayer::remove);

        games.remove(gameRef);
    }

    public Game updateGame(final Game game) {
        add(game);
        return game;
    }
}


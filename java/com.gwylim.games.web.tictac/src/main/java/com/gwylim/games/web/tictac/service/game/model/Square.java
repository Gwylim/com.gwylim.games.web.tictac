package com.gwylim.games.web.tictac.service.game.model;

import javax.annotation.Nullable;
import java.util.Optional;

import com.google.common.base.Objects;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;

public final class Square {
    @Nullable
    private final PlayerRef playerRef;
    private final int x;
    private final int y;

    Square(@Nullable final PlayerRef playerRef, final int x, final int y) {
        this.playerRef = playerRef;
        this.x = x;
        this.y = y;
    }

    public Optional<PlayerRef> getPlayer() {
        return Optional.ofNullable(playerRef);
    }

    public boolean isEmpty() {
        return getPlayer().isEmpty();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        final String playerString = getPlayer().map(Object::hashCode).map(String::valueOf).map(s -> s.substring(0, 1)).orElse("-");
        return "(" + x + "," + y + ":" + playerString + ")";
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Square square = (Square) o;
        return x == square.x && y == square.y;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(x, y);
    }
}

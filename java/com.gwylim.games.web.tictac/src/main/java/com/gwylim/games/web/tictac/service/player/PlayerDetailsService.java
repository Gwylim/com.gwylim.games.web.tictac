package com.gwylim.games.web.tictac.service.player;

import com.gwylim.common.exceptions.NotFoundException;
import com.gwylim.games.web.tictac.api.models.PlayerType;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.service.player.model.PlayerDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.WeakHashMap;
import java.util.concurrent.ThreadLocalRandom;

@Component
public class PlayerDetailsService {
    private static final Logger LOG = LoggerFactory.getLogger(PlayerDetailsService.class);

    private static final Map<PlayerRef, PlayerDetails> MAP = new WeakHashMap<>();

    public PlayerDetails getPlayer(final PlayerRef playerRef) {
        return Optional.ofNullable(MAP.get(playerRef))
                       .orElseThrow(() -> new NotFoundException(playerRef));
    }

    public PlayerDetails createPlayerDetails(final String name, final PlayerType type, final PlayerRef playerRef) {
        final PlayerDetails player = new PlayerDetails(name, type, getIcon(type), playerRef);
        MAP.put(player.getPlayerRef(), player);
        return player;
    }


    private String getIcon(final PlayerType type) {
        switch (type) {
            case AI:
                return "COG";
            case HUMAN:
                return ThreadLocalRandom.current().nextBoolean() ? "MALE" : "FEMALE";
            default:
                LOG.error("Unknown player type:" + type);
                return "COG";
        }
    }
}

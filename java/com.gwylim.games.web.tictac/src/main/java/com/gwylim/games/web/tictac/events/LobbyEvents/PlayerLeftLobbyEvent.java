package com.gwylim.games.web.tictac.events.LobbyEvents;

import com.gwylim.games.web.tictac.api.models.references.PlayerRef;

public class PlayerLeftLobbyEvent extends LobbyPlayersChangedEvent {
    public PlayerLeftLobbyEvent(final PlayerRef playerRef) {
        super(playerRef);
    }
}

package com.gwylim.games.web.tictac.events.LobbyEvents;

import com.gwylim.games.web.tictac.api.models.references.PlayerRef;

public class PlayerEnteredLobbyEvent extends LobbyPlayersChangedEvent {
    public PlayerEnteredLobbyEvent(final PlayerRef playerRef) {
        super(playerRef);
    }
}

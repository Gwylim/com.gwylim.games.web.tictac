package com.gwylim.games.web.tictac.events;

import com.gwylim.games.web.tictac.service.game.model.Game;

public class GameStartedEvent extends GameEvent {
    public GameStartedEvent(final Game game) {
        super(game);
    }
}

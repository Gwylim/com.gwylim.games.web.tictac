package com.gwylim.games.web.tictac.events;

import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.service.game.model.Game;

public class GameMoveMadeEvent extends GameEvent {
    private final PlayerRef mover;

    public GameMoveMadeEvent(final Game game,
                             final PlayerRef mover) {
        super(game);
        this.mover = mover;
    }

    public PlayerRef getMover() {
        return mover;
    }
}

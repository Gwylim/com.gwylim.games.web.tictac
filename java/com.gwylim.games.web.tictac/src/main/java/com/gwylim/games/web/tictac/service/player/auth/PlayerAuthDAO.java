package com.gwylim.games.web.tictac.service.player.auth;

import com.clevergang.jdbc.FluentNamedParameterJdbcTemplate;
import com.google.common.base.Joiner;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.api.models.meaningful.Password;
import com.gwylim.games.web.tictac.api.models.meaningful.Username;
import com.gwylim.games.web.tictac.service.player.auth.exceptions.UsernameExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Component
public class PlayerAuthDAO {
    private static final Logger LOG = LoggerFactory.getLogger(PlayerAuthDAO.class);

    private final FluentNamedParameterJdbcTemplate jdbc;
    private final PasswordEncoder passwordEncoder;

    @Bean
    public static PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public PlayerAuthDAO(final FluentNamedParameterJdbcTemplate jdbc,
        final PasswordEncoder passwordEncoder) {

        this.jdbc = jdbc;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Create a new account given the username and password.
     * @return The {@link PlayerRef} for the new account.
     */
    public PlayerRef createAccount(final Username username, final Password password) throws UsernameExistsException {
        try {
            final PlayerRef playerRef = PlayerRef.generateNew();

            jdbc.update(Joiner.on(" ").join(
                "INSERT INTO player_auth (player_ref, username, password)",
                "VALUES (:player_ref, :username, :password)"))
                .bind("player_ref", playerRef.getValue())
                .bind("username", username.getValue())
                .bind("password", passwordEncoder.encode(password.getValue()))
                .execute();

            return playerRef;
        }
        catch (final DuplicateKeyException e) {
            // We appear to have to check the message for which unique constraint was violated.
            final String message = e.getMessage();
            if (message != null && message.toLowerCase().contains("player_auth_username_unique")) {
                throw new UsernameExistsException();
            }
            else {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Get the {@link PlayerRef} For the given username if it is present and the provided password matches.
     */
    public Optional<PlayerRef> passwordMatches(final Username username, final Password password) {
        final Optional<AccountDbo> account = jdbc.query("SELECT * FROM player_auth WHERE username = :username")
                                                 .bind("username", username.getValue())
                                                 .fetch(this::mapper)
                                                 .stream()
                                                 .findFirst();

        return account.filter(a -> passwordEncoder.matches(password.getValue(), a.password.getValue()))
                      .map(AccountDbo::getPlayerRef);
    }

    private AccountDbo mapper(final ResultSet resultSet, final int i) {
        try {
            return new AccountDbo(
                new PlayerRef(resultSet.getString("player_ref")),
                new Username(resultSet.getString("username")),
                new Password(resultSet.getString("password"))
            );
        }
        catch (final SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static final class AccountDbo {
        private final PlayerRef playerRef;
        private final Username username;
        private final Password password;

        private AccountDbo(final PlayerRef playerRef, final Username username, final Password password) {
            this.playerRef = playerRef;
            this.username = username;
            this.password = password;
        }

        public PlayerRef getPlayerRef() {
            return playerRef;
        }

        public Username getUsername() {
            return username;
        }

        public Password getPassword() {
            return password;
        }
    }
}


package com.gwylim.games.web.tictac.events;

import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.service.game.model.Game;
import com.gwylim.system.events.api.annotations.Subject;

import java.util.Set;
import java.util.stream.Collectors;

public class GameEvent {
    private final GameRef gameRef;
    private final Set<PlayerRef> playerRefs;

    public GameEvent(final Game game) {
        this.gameRef = game.getGameRef();
        this.playerRefs = game.getPlayers().stream().collect(Collectors.toSet());
    }

    @Subject
    public GameRef getGameRef() {
        return gameRef;
    }

    @Subject
    public Set<PlayerRef> getPlayers() {
        return playerRefs;
    }
}

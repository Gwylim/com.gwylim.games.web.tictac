package com.gwylim.games.web.tictac.service.player.auth;

import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.api.models.meaningful.Password;
import com.gwylim.games.web.tictac.api.models.meaningful.Username;
import com.gwylim.games.web.tictac.service.player.auth.exceptions.UsernameExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PlayerAuthService {

    private final PlayerAuthDAO playerAuthDAO;

    @Autowired
    public PlayerAuthService(final PlayerAuthDAO playerAuthDAO) {
        this.playerAuthDAO = playerAuthDAO;
    }


    public PlayerRef createPlayerAuth(final Username username, final Password password) throws UsernameExistsException {
        return playerAuthDAO.createAccount(username, password);
    }

    public Optional<PlayerRef> authenticatePlayer(final Username username, final Password password) {
        return playerAuthDAO.passwordMatches(username, password);
    }

}

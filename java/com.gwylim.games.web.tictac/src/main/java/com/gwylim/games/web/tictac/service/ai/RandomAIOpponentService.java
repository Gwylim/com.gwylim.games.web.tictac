package com.gwylim.games.web.tictac.service.ai;

import com.gwylim.common.ArrayUtil;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.service.LobbyService;
import com.gwylim.games.web.tictac.service.game.GameService;
import com.gwylim.games.web.tictac.service.game.model.Game;
import com.gwylim.games.web.tictac.service.player.PlayerDetailsService;
import com.gwylim.system.events.api.EventSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static com.gwylim.common.StreamUtil.randomly;

/**
 * Service that provides AI opponents that will automatically play against
 * players who select them in the lobby.
 */
@Component
@Scope("singleton")
public class RandomAIOpponentService extends AIOpponentServiceBase {

    @Autowired
    public RandomAIOpponentService(final GameService gameService,
                                   final LobbyService lobbyService,
                                   final PlayerDetailsService playerDetailsService,
                                   final EventSystem eventSystem) {
        super(gameService, lobbyService, playerDetailsService, eventSystem);
    }

    Move chooseMove(final Game game, final PlayerRef playerRef) {
        return ArrayUtil.stream(game.getBoard().getBoard())
                .filter(v -> v.getValue() == null)
                .sorted(randomly())
                .map(v -> new Move(v.getX(), v.getY()))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("No move to make."));
    }

    @Override
    String name() {
        return "Random AI";
    }
}

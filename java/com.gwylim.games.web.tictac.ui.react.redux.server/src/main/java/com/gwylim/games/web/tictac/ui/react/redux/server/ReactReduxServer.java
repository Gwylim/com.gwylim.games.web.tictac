package com.gwylim.games.web.tictac.ui.react.redux.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
@ComponentScan(basePackages = {"com.gwylim"})
public class ReactReduxServer {
    private static final Logger LOG = LoggerFactory.getLogger(ReactReduxServer.class);

    public static void main(final String... args) {
        SpringApplication.run(ReactReduxServer.class, args);
    }

    @Bean
    public CommandLineRunner logStartup() {
        return (args) -> LOG.info("React Redux Server + API started.");
    }
}

package com.gwylim.games.web.tictac.api.models.push.player;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.system.push.messaging.common.SubscriptionPath;

@SubscriptionPath("/topic/lobby/players/left")
public class PlayerLeftLobbyPushModel extends PlayerPushModelBase {
    @JsonCreator
    public PlayerLeftLobbyPushModel(@JsonProperty("playerRef") final PlayerRef playerRef) {
        super(playerRef);
    }
}

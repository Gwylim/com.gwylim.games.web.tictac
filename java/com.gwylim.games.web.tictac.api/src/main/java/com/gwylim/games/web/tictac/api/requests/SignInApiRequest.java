package com.gwylim.games.web.tictac.api.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gwylim.games.web.tictac.api.models.meaningful.Password;
import com.gwylim.games.web.tictac.api.models.meaningful.Username;

public class SignInApiRequest {
    private final Username username;
    private final Password password;

    @JsonCreator
    public SignInApiRequest(@JsonProperty("username") final Username username,
                            @JsonProperty("password") final Password password) {

        this.username = username;
        this.password = password;
    }

    public Username getUsername() {
        return username;
    }

    public Password getPassword() {
        return password;
    }
}
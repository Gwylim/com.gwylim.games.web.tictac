package com.gwylim.games.web.tictac.api.models.meaningful;

import com.gwylim.common.models.Meaningful;

public class Password extends Meaningful<String> {
    public Password(final String identifier) {
        super(identifier);
    }
}

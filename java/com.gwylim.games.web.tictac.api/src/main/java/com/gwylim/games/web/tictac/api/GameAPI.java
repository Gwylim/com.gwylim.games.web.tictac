package com.gwylim.games.web.tictac.api;


import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.api.models.rest.GameApiModel;
import com.gwylim.games.web.tictac.api.requests.MakeMoveApiRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * API for games.
 * See: https://cloud.spring.io/spring-cloud-netflix/multi/multi_spring-cloud-feign.html
 */
@FeignClient(name = "GameAPI", url = "${com.gwylim.games.web.tictac.api.url}")
@RequestMapping(GameAPI.PATH)
public interface GameAPI {
    String PATH = "/api/game";

    @GetMapping("/{game}")
    GameApiModel getGame(@PathVariable("game") GameRef gameRef);

    @PostMapping("/{game}/{player}/move")
    GameApiModel makeMove(@PathVariable("game") GameRef gameRef,
                          @PathVariable("player") PlayerRef playerRef,
                          @RequestBody MakeMoveApiRequest move);

    @PostMapping("/{game}/leave")
    void leave(@PathVariable("game") GameRef gameRef, @RequestBody PlayerRef playerRef);
}

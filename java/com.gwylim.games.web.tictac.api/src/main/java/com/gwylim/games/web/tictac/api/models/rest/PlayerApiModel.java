package com.gwylim.games.web.tictac.api.models.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gwylim.games.web.tictac.api.models.PlayerType;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;

public class PlayerApiModel {

    private final PlayerRef playerRef;
    private final PlayerType type;
    private final String name;
    private final String icon;

    @JsonCreator
    public PlayerApiModel(@JsonProperty("playerRef") final PlayerRef playerRef,
                          @JsonProperty("type") final PlayerType type,
                          @JsonProperty("name") final String name,
                          @JsonProperty("icon") final String icon) {
        this.playerRef = playerRef;
        this.type = type;
        this.name = name;
        this.icon = icon;
    }

    public PlayerRef getPlayerRef() {
        return playerRef;
    }

    public PlayerType getType() {
        return type;
    }

    public String getIcon() {
        return icon;
    }

    public String getName() {
        return name;
    }
}

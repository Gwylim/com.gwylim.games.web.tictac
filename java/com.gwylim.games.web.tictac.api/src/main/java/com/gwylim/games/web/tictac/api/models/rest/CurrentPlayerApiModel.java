package com.gwylim.games.web.tictac.api.models.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gwylim.games.web.tictac.api.models.PlayerType;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRestToken;

public class CurrentPlayerApiModel extends PlayerApiModel {

    private final PlayerRestToken token;

    public CurrentPlayerApiModel(final PlayerApiModel playerApiModel,
                                 final PlayerRestToken token) {
        
        this(playerApiModel.getPlayerRef(),
             playerApiModel.getType(),
             playerApiModel.getName(),
             playerApiModel.getIcon(),
             token);
    }

    @JsonCreator
    public CurrentPlayerApiModel(@JsonProperty("playerRef") final PlayerRef playerRef,
                                 @JsonProperty("type") final PlayerType type,
                                 @JsonProperty("name") final String name,
                                 @JsonProperty("icon") final String icon,
                                 @JsonProperty("token") final PlayerRestToken token) {

        super(playerRef, type, name, icon);
        this.token = token;
    }

    public PlayerRestToken getToken() {
        return token;
    }
}

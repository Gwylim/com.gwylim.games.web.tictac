package com.gwylim.games.web.tictac.api;


import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Set;

/**
 * API for games.
 * See: https://cloud.spring.io/spring-cloud-netflix/multi/multi_spring-cloud-feign.html
 */
@FeignClient(name = "LobbyAPI", url = "${com.gwylim.games.web.tictac.api.url}")
@RequestMapping(path = LobbyAPI.PATH)
public interface LobbyAPI {
    String PATH = "/api/lobby";


    @PostMapping(path = "/", consumes = MediaType.TEXT_PLAIN_VALUE)
    void enterLobby(@RequestBody PlayerRef playerRef);


    @DeleteMapping("/{player}")
    void exitLobby(@PathVariable("player") PlayerRef playerRef);


    @GetMapping("/")
    Set<PlayerRef> getPlayersInLobby();


    @GetMapping("/{player}/opponents")
    Set<PlayerRef> getOpponents(@PathVariable("player") PlayerRef playerRef);


    @PostMapping(path = "/{player}/opponents", consumes = MediaType.TEXT_PLAIN_VALUE)
    void addOpponent(@PathVariable("player") PlayerRef playerRef, @RequestBody PlayerRef opponentRef);


    @DeleteMapping("/{player}/opponents/{opponent}")
    void removeOpponent(@PathVariable("player") PlayerRef playerRef,
                        @PathVariable("opponent") PlayerRef opponentRef);


    @PostMapping(path = "/{player}/opponents/toggle", consumes = MediaType.TEXT_PLAIN_VALUE)
    void toggleOpponent(@PathVariable("player") PlayerRef playerRef, @RequestBody PlayerRef opponentRef);
}

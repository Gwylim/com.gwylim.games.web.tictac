package com.gwylim.games.web.tictac.api.models.push.game;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;

import java.util.Set;


public abstract class GamePushModelBase {

    private final GameRef game;
    private final Set<PlayerRef> players;

    @JsonCreator
    public GamePushModelBase(@JsonProperty("game") final GameRef game,
                             @JsonProperty("players") final Set<PlayerRef> players) {
        this.game = game;
        this.players = players;
    }

    public GameRef getGame() {
        return game;
    }

    public Set<PlayerRef> getPlayers() {
        return players;
    }
}
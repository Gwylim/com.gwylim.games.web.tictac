package com.gwylim.games.web.tictac.api.models.references;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.gwylim.common.models.Reference;

public class GameRef extends Reference {

    public static GameRef generateNew() { return new GameRef(); }

    private GameRef() { super(); }

    @JsonCreator
    public GameRef(final String ref) {
        super(ref);
    }

    @Override
    public String prefix() {
        return "Game";
    }
}

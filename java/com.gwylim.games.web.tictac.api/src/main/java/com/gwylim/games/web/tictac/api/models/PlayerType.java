package com.gwylim.games.web.tictac.api.models;

public enum PlayerType {
    AI,
    HUMAN
}

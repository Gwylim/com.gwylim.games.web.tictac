package com.gwylim.games.web.tictac.api.models.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.annotation.Nullable;
import java.util.Optional;
import java.util.Set;


public class GameApiModel {
    private final GameRef gameRef;
    private final Set<PlayerRef> playerRefs;

    @Nullable
    private final PlayerRef winner;
    @Nullable
    private final PlayerRef toGo;

    private final PlayerRef[][] board;
    private final Boolean[][] winMap;

    @JsonCreator
    public GameApiModel(@JsonProperty("gameRef") final GameRef gameRef,
                        @JsonProperty("playerRefs") final Set<PlayerRef> playerRefs,
                        @JsonProperty("toGo") @Nullable final PlayerRef toGo,
                        @JsonProperty("winner") @Nullable final PlayerRef winner,
                        @JsonProperty("board") final PlayerRef[][] board,
                        @JsonProperty("winMap") final Boolean[][] winMap) {

        this.gameRef = gameRef;
        this.playerRefs = playerRefs;
        this.winner = winner;
        this.toGo = toGo;
        this.board = board.clone();
        this.winMap = winMap.clone();
    }

    public GameRef getGameRef() {
        return gameRef;
    }

    public Set<PlayerRef> getPlayerRefs() {
        return playerRefs;
    }

    public Optional<PlayerRef> getToGo() {
        return Optional.ofNullable(toGo);
    }

    public Optional<PlayerRef> getWinner() {
        return Optional.ofNullable(winner);
    }

    public PlayerRef[][] getBoard() {
        return board.clone();
    }

    public Boolean[][] getWinMap() {
        return winMap.clone();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("gameRef", gameRef)
                .append("playerRefs", playerRefs)
                .append("toGo", toGo)
                .append("board", board)
                .toString();
    }
}

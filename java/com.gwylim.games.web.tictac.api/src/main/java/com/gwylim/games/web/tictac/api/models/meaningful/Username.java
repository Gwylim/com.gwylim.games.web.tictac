package com.gwylim.games.web.tictac.api.models.meaningful;

import com.gwylim.common.models.Meaningful;

public class Username extends Meaningful<String> {
    public Username(final String identifier) {
        super(identifier);
    }
}

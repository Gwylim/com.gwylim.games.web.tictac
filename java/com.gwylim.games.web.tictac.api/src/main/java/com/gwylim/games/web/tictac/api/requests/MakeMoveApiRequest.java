package com.gwylim.games.web.tictac.api.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MakeMoveApiRequest {
    private final int x;
    private final int y;

    @JsonCreator
    public MakeMoveApiRequest(@JsonProperty("x") final int x,
                              @JsonProperty("y") final int y) {

        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}

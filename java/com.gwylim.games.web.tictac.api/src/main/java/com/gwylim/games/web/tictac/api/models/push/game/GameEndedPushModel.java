package com.gwylim.games.web.tictac.api.models.push.game;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.system.push.messaging.common.SubscribeBy;
import com.gwylim.system.push.messaging.common.SubscriptionPath;

import java.util.Collections;
import java.util.Set;


@SubscriptionPath("/topic/game/{id}/end")
public class GameEndedPushModel extends GamePushModelBase implements SubscribeBy<GameRef> {

    @JsonCreator
    public GameEndedPushModel(@JsonProperty("game") final GameRef game,
                              @JsonProperty("players") final Set<PlayerRef> players) {
        super(game, players);
    }

    @Override
    public Set<GameRef> getSubscriptionIdentifiers() {
        return Collections.singleton(getGame());
    }
}
package com.gwylim.games.web.tictac.api.models.references;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.gwylim.common.models.Reference;

public class PlayerRef extends Reference {

    public static PlayerRef generateNew() { return new PlayerRef(); }

    private PlayerRef() { super(); }

    @JsonCreator
    public PlayerRef(final String ref) {
        super(ref);
    }

    @Override
    public String prefix() {
        return "Player";
    }
}

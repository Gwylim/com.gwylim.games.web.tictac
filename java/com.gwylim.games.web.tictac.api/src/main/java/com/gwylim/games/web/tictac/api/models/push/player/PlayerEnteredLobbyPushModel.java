package com.gwylim.games.web.tictac.api.models.push.player;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.system.push.messaging.common.SubscriptionPath;

@SubscriptionPath("/topic/lobby/players/entered")
public class PlayerEnteredLobbyPushModel extends PlayerPushModelBase {
    @JsonCreator
    public PlayerEnteredLobbyPushModel(@JsonProperty("playerRef") final PlayerRef playerRef) {
        super(playerRef);
    }
}

package com.gwylim.games.web.tictac.api.models.references;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.gwylim.common.TextUtil;
import com.gwylim.common.models.Reference;

import java.io.Serializable;

/**
 * Player token used for access verification.
 */
public class PlayerRestToken extends Reference implements Serializable {
    private static final int TOKEN_LENGTH = 100;

    public static PlayerRestToken generateNew() { return new PlayerRestToken(); }

    private PlayerRestToken() { super(); }

    @JsonCreator
    public PlayerRestToken(final String ref) {
        super(ref);
    }

    @Override
    public String prefix() {
        return "PlayerRestToken";
    }

    @Override
    protected String generateUniqueString() {
        return TextUtil.secureRandomString(TOKEN_LENGTH);
    }
}

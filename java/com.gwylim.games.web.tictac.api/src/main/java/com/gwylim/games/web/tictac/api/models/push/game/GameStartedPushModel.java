package com.gwylim.games.web.tictac.api.models.push.game;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.system.push.messaging.common.SubscribeBy;
import com.gwylim.system.push.messaging.common.SubscriptionPath;

import java.util.Set;


@SubscriptionPath("/topic/player/{id}/games/start")
public class GameStartedPushModel extends GamePushModelBase implements SubscribeBy<PlayerRef> {

    @JsonCreator
    public GameStartedPushModel(@JsonProperty("game") final GameRef game,
                                @JsonProperty("players") final Set<PlayerRef> players) {
        super(game, players);
    }

    @Override
    public Set<PlayerRef> getSubscriptionIdentifiers() {
        return getPlayers();
    }
}

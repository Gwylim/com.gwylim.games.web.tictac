package com.gwylim.games.web.tictac.api.models.push.player;

import com.gwylim.games.web.tictac.api.models.references.PlayerRef;

public abstract class PlayerPushModelBase {

    private final PlayerRef playerRef;

    public PlayerPushModelBase(final PlayerRef playerRef) {
        this.playerRef = playerRef;
    }

    public PlayerRef getPlayerRef() {
        return playerRef;
    }
}

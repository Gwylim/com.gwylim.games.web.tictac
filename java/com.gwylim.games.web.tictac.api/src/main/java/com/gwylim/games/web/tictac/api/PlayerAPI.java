package com.gwylim.games.web.tictac.api;


import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.api.models.rest.CurrentPlayerApiModel;
import com.gwylim.games.web.tictac.api.models.rest.PlayerApiModel;
import com.gwylim.games.web.tictac.api.requests.SignInApiRequest;
import com.gwylim.games.web.tictac.api.requests.SignUpApiRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * API for games.
 * See: https://cloud.spring.io/spring-cloud-netflix/multi/multi_spring-cloud-feign.html
 */
@FeignClient(name = "PlayerAPI", url = "${com.gwylim.games.web.tictac.api.url}")
@RequestMapping(PlayerAPI.PATH)
public interface PlayerAPI {
    String PATH = "/api/player";

    @PostMapping(path = "/signIn")
    CurrentPlayerApiModel signIn(@RequestBody SignInApiRequest request);

    @PostMapping(path = "/signUp")
    CurrentPlayerApiModel signUp(@RequestBody SignUpApiRequest request);

    @GetMapping("/{playerRef}")
    PlayerApiModel getPlayer(@PathVariable("playerRef") PlayerRef playerRef);

    @PostMapping("/disconnect")
    void disconnect(@RequestBody PlayerRef playerRef);
}

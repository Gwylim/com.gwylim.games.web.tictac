package com.gwylim.games.web.tictac.api.models.push.game;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.system.push.messaging.common.SubscribeBy;
import com.gwylim.system.push.messaging.common.SubscriptionPath;

import java.util.Collections;
import java.util.Set;

@SubscriptionPath("/topic/game/{id}/moves")
public class GameMoveMadePushModel extends GamePushModelBase implements SubscribeBy<GameRef> {

    private final PlayerRef mover;

    @JsonCreator
    public GameMoveMadePushModel(@JsonProperty("game") final GameRef gameRef,
                                 @JsonProperty("players") final Set<PlayerRef> players,
                                 @JsonProperty("mover") final PlayerRef mover) {
        super(gameRef, players);
        this.mover = mover;
    }

    @Override
    public Set<GameRef> getSubscriptionIdentifiers() {
        return Collections.singleton(getGame());
    }

    public PlayerRef getMover() {
        return mover;
    }
}

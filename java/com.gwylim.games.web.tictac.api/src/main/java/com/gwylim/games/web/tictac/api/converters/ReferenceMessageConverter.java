package com.gwylim.games.web.tictac.api.converters;

import com.google.common.base.Charsets;
import com.gwylim.common.models.Reference;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Auto convert RequestBodies to References and back.
 */
public class ReferenceMessageConverter implements HttpMessageConverter<Reference> {
    @Override
    public boolean canRead(@Nonnull final Class<?> clazz, final MediaType mediaType) {
        return Reference.class.isAssignableFrom(clazz);
    }

    @Override
    public boolean canWrite(@Nonnull final Class<?> clazz, final MediaType mediaType) {
        return Reference.class.isAssignableFrom(clazz) && (mediaType == null || mediaType.includes(MediaType.TEXT_PLAIN) || mediaType.includes(MediaType.APPLICATION_JSON));
    }

    @Nonnull
    @Override
    public List<MediaType> getSupportedMediaTypes() {
        return List.of(MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON);
    }

    @Nonnull
    @Override
    public Reference read(@Nonnull final Class<? extends Reference> clazz, final HttpInputMessage inputMessage) throws IOException {
        final List<String> strings = IOUtils.readLines(inputMessage.getBody());
        try {
            final Constructor<? extends Reference> constructor = clazz.getConstructor(String.class);
            return constructor.newInstance(strings.get(0));
        }
        catch (final NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new HttpMessageNotReadableException("Failed to read " + clazz.getSimpleName(), e, inputMessage);
        }
    }

    @Override
    public void write(final Reference reference, final MediaType contentType, final HttpOutputMessage outputMessage) throws IOException {
        outputMessage.getBody().write(reference.getValue().getBytes(Charsets.UTF_8));
    }
}
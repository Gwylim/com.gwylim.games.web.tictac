package com.gwylim.games.web.tictac.ui.cli.components;

import com.gwylim.common.Console;
import com.gwylim.games.web.tictac.api.LobbyAPI;
import com.gwylim.games.web.tictac.api.PlayerAPI;
import com.gwylim.games.web.tictac.api.models.push.player.PlayerEnteredLobbyPushModel;
import com.gwylim.games.web.tictac.api.models.push.player.PlayerLeftLobbyPushModel;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.api.models.rest.PlayerApiModel;
import com.gwylim.system.push.messaging.client.PushClient;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class LobbyPlayers implements AutoCloseable {

    private final LobbyAPI lobbyAPI;
    private final PlayerAPI playerAPI;
    private final Console console;
    private final PlayerRef playerRef;

    private final Map<PlayerRef, Integer> playersToNumbers = new ConcurrentHashMap<>();
    private final Map<Integer, PlayerApiModel> numbersToPlayers = new ConcurrentHashMap<>();
    private final AtomicInteger counter = new AtomicInteger(0);

    private final PushClient.Subscription playerLeft;
    private final PushClient.Subscription playerEntered;

    public LobbyPlayers(final LobbyAPI lobbyAPI,
                        final PlayerAPI playerAPI,
                        final Console console,
                        final PushClient pushClient,
                        final PlayerRef playerRef) {

        this.lobbyAPI = lobbyAPI;
        this.playerAPI = playerAPI;
        this.console = console;
        this.playerRef = playerRef;

        // Fill lobby:
        lobbyAPI.getPlayersInLobby().forEach(this::addPlayer);

        // Subscribe to new players events...
        playerLeft = pushClient.subscribe(this::handlePlayerLeft);
        playerEntered = pushClient.subscribe(this::handlePlayerEntered);
    }


    public Optional<PlayerRef> getForNumber(final int opponent) {
        return Optional.ofNullable(numbersToPlayers.get(opponent)).map(PlayerApiModel::getPlayerRef);
    }


    private synchronized void handlePlayerLeft(final PlayerLeftLobbyPushModel event) {
        final PlayerRef ref = event.getPlayerRef();

        final Optional<PlayerApiModel> player =
                Optional.ofNullable(playersToNumbers.remove(ref))
                .flatMap(v -> Optional.ofNullable(numbersToPlayers.remove(v)));

        console.printf("Player left: " + player.map(PlayerApiModel::getName).orElse("[unknown]"));
        renderLobby();
    }

    private void handlePlayerEntered(final PlayerEnteredLobbyPushModel event) {
        final PlayerApiModel player = addPlayer(event.getPlayerRef());

        console.printf("Player entered: " + player.getName());
        renderLobby();
    }

    private synchronized PlayerApiModel addPlayer(final PlayerRef ref) {
        final PlayerApiModel player = playerAPI.getPlayer(ref);
        // Don't add ourselves:
        if (ref.equals(playerRef)) {
            return player;
        }

        final int number = counter.getAndIncrement();
        if (playersToNumbers.put(ref, number) == null) {
            numbersToPlayers.put(number, player);
        }
        else {
            counter.decrementAndGet();
        }

        return player;
    }

    public synchronized void renderLobby() {
        final Set<PlayerRef> opponents = lobbyAPI.getOpponents(playerRef);

        final List<Integer> numbers = numbersToPlayers.keySet().stream()
                .sorted()
                .collect(Collectors.toList());

        console.block(() ->
                numbers.forEach(n -> {
                            final PlayerApiModel player = numbersToPlayers.get(n);
                            final String selectedIndicator = opponents.contains(player.getPlayerRef()) ? "*" : " ";
                            console.printf("{}) {} {}", n, selectedIndicator, player.getName());
                        }
                ));
    }

    @Override
    public void close() {
        playerLeft.unsubscribe();
        playerEntered.unsubscribe();
    }
}

package com.gwylim.games.web.tictac.ui.cli.components;

import com.gwylim.common.Console;
import com.gwylim.common.Console.SupersededException;
import com.gwylim.games.web.tictac.api.LobbyAPI;
import com.gwylim.games.web.tictac.api.PlayerAPI;
import com.gwylim.games.web.tictac.api.models.push.game.GameStartedPushModel;
import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.ui.cli.CommandLine;
import com.gwylim.system.push.messaging.client.PushClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

@Component
public class LobbyView {
    private static final Logger LOG = LoggerFactory.getLogger(CommandLine.class);

    private final Console console;

    private final LobbyAPI lobbyAPI;
    private final PlayerAPI playerAPI;
    private final GameView gameView;

    public LobbyView(final Console console,
                     final LobbyAPI lobbyAPI,
                     final PlayerAPI playerAPI,
                     final GameView gameView) {

        this.console = console;

        this.lobbyAPI = lobbyAPI;
        this.playerAPI = playerAPI;
        this.gameView = gameView;
    }

    public void run(final PlayerRef playerRef, final PushClient pushClient) {
        final Instance instance = new Instance(playerRef, pushClient);
        instance.run();
    }

    private final class Instance {

        private final PlayerRef playerRef;
        private final PushClient pushClient;

        private Instance(final PlayerRef playerRef, final PushClient pushClient) {
            this.playerRef = playerRef;
            this.pushClient = pushClient;
        }

        private void run() {
            // The in progress game:
            final AtomicReference<GameRef> game = new AtomicReference<>();

            // TODO: Should add a graceful way to exit...
            while (true) {
                console.printf("Entering lobby...");
                lobbyAPI.enterLobby(playerRef);

                final PushClient.Subscription gameStartedSubscription = pushClient.subscribe(playerRef, (Consumer<GameStartedPushModel>) g -> {
                    console.escape();
                    game.set(g.getGame());
                });

                try (final LobbyPlayers lobbyPlayers = new LobbyPlayers(lobbyAPI,
                                                                        playerAPI,
                                                                        console,
                                                                        pushClient,
                                                                        playerRef)) {
                    console.block(() -> {
                        console.printf("players in lobby:");
                        lobbyPlayers.renderLobby();
                    });

                    while (game.get() == null) {
                        final int opponentNumber = console.read(Integer.class, "Select opponent:");

                        lobbyPlayers.getForNumber(opponentNumber).ifPresent(this::opponentSelected);
                        lobbyPlayers.renderLobby();
                    }
                }
                catch (final SupersededException e) {
                    // We are no-longer in control of the console, assume we have entered a game.
                }
                catch (final Exception e) {
                    throw new RuntimeException("Unexpected exception", e);
                }

                gameStartedSubscription.unsubscribe();
                LOG.info("Exiting Lobby to enter game.");
                lobbyAPI.exitLobby(playerRef);

                // Run the game, we will return to the lobby when it is done:
                gameView.run(game.get(), playerRef, pushClient);
                game.set(null);
            }
        }

        private void opponentSelected(final PlayerRef opponentRef) {
            if (opponentRef.equals(playerRef)) {
                console.printf("Don't select your self.");
            }
            else {
                console.printf("Selected: " + opponentRef);

                lobbyAPI.toggleOpponent(playerRef, opponentRef);
            }
        }
    }
}

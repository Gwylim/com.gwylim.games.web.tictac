package com.gwylim.games.web.tictac.ui.cli.components;

import com.gwylim.common.Console;
import com.gwylim.common.Console.SupersededException;
import com.gwylim.common.ThreadUtil;
import com.gwylim.games.web.tictac.api.GameAPI;
import com.gwylim.games.web.tictac.api.models.push.game.GameEndedPushModel;
import com.gwylim.games.web.tictac.api.models.push.game.GameMoveMadePushModel;
import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.api.models.rest.GameApiModel;
import com.gwylim.games.web.tictac.api.requests.MakeMoveApiRequest;
import com.gwylim.games.web.tictac.ui.cli.CommandLine;
import com.gwylim.system.push.messaging.client.PushClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.gwylim.games.web.tictac.ui.cli.components.GameRenderUtil.createView;

@Component
public class GameView {
    private static final Logger LOG = LoggerFactory.getLogger(CommandLine.class);
    static final int BOARD_SIDE_LENGTH = 3;

    private final Console console;

    private final GameAPI gameAPI;

    @Autowired
    public GameView(final Console console,
                    final GameAPI gameAPI) {

        this.console = console;

        this.gameAPI = gameAPI;
    }

    public void run(final GameRef gameRef, final PlayerRef playerRef, final PushClient pushClient) {
        final Instance instance = new Instance(gameRef, playerRef, pushClient);
        instance.run();
    }

    private final class Instance {

        private final PlayerRef playerRef;

        private final GameRef gameRef;
        private final PushClient pushClient;

        private Instance(final GameRef gameRef, final PlayerRef playerRef, final PushClient pushClient) {
            this.playerRef = playerRef;
            this.gameRef = gameRef;
            this.pushClient = pushClient;
        }

        private void run() {

            final CountDownLatch gameInProgress = new CountDownLatch(1);

            final PushClient.Subscription gameEndSubscription = pushClient.subscribe(gameRef, (GameEndedPushModel e) -> {
                LOG.info("Game {} ended.", e.getGame());
                console.escape();
                gameInProgress.countDown();
            });

            // If it is our move render so:
            gameAPI.getGame(gameRef).getToGo()
                    .filter(playerRef::equals)
                    .ifPresent(p -> ThreadUtil.run(this::ourMove));

            // Subscribe to the moves made in the game...
            // However the other player might have already made the first move:
            final PushClient.Subscription moveMadeSubscription = pushClient.subscribe(gameRef, this::moveMade);

            try {
                gameInProgress.await();
            }
            catch (final InterruptedException e) {
                LOG.warn("Interrupted waiting for game to end.");
            }

            gameEnded();
            gameEndSubscription.unsubscribe();
            moveMadeSubscription.unsubscribe();
        }


        private void moveMade(final GameMoveMadePushModel event) {
            LOG.info("Move made in game {} by {}.", event.getGame(), event.getMover());

            if (!event.getMover().equals(playerRef)) {
                console.block(() -> console.printf("Other player made a move."));
                ourMove();
            }
            // Otherwise we are observing a move we just made and we will have
            // already rendered that.
        }

        // Lock to stop the scenario where we subscribe to moves after the other player
        // has made a move rendering "Your move..." twice.
        private final Lock ourMoveLock = new ReentrantLock();

        private void ourMove() {
            if (ourMoveLock.tryLock()) {
                try {
                    console.block(() -> {
                        console.printf("Your move...");
                        renderGame();
                    });

                    final int move = console.read(Integer.class, "Enter Move:",
                            n -> n <= BOARD_SIDE_LENGTH * BOARD_SIDE_LENGTH,
                            n -> n >= 0
                    );

                    // Moves are always 0 -> 9 row by row:
                    final int y = move % BOARD_SIDE_LENGTH;
                    final int x = (move - y) / BOARD_SIDE_LENGTH;
                    final GameApiModel gameApiModel = gameAPI.makeMove(gameRef, playerRef, new MakeMoveApiRequest(x, y));
                    renderGame(gameApiModel);
                }
                catch (final SupersededException e) {
                    LOG.info("Console Superseded...");
                }
                finally {
                    ourMoveLock.unlock();
                }
            }
            else {
                LOG.info("Already in our move state.");
            }
        }

        private void gameEnded() {
            final GameApiModel finalState = gameAPI.getGame(gameRef);

            console.block(() -> {
                console.printf("Game over:");
                renderGame(finalState);
                final Optional<PlayerRef> winner = finalState.getWinner();
                if (winner.isPresent()) {
                    if (winner.get().equals(playerRef)) {
                        console.printf("      You win.");
                    }
                    else {
                        console.printf("      You lost.");
                    }
                }
                else {
                    console.printf("      No winner.");
                }
            });


            try {
                console.enterToContinue();
            }
            catch (final SupersededException e) {
                // We don't mind being superseded.
            }
        }

        private synchronized void renderGame() {
            renderGame(gameAPI.getGame(gameRef));
        }

        private GameApiModel renderGame(final GameApiModel game) {
            final PlayerRef[][] board = game.getBoard();

            console.printf(createView(playerRef, board));

            return game;
        }
    }
}

package com.gwylim.games.web.tictac.ui.cli.components;

import com.gwylim.common.ArrayUtil;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;

import java.util.Optional;

import static com.gwylim.games.web.tictac.ui.cli.components.GameView.BOARD_SIDE_LENGTH;

public final class GameRenderUtil {
    private GameRenderUtil() {
        // Utility class.
    }

    public static String createView(final PlayerRef us, final PlayerRef[][] board) {
        final StringBuilder view = new StringBuilder();
        ArrayUtil.stream(board)
                .forEach(c -> {
                    if (c.getY() == 0 && c.getX() != 0) {
                        view.append("\n");
                    }
                    view.append(" ");

                    final String square = square(c.getValue(), us)
                            .orElse(Integer.toString(c.getX() * BOARD_SIDE_LENGTH + c.getY()));

                    view.append(square);
                });
        return view.toString();
    }

    private static Optional<String> square(final PlayerRef playerRef, final PlayerRef us) {
        if (playerRef == null) {
            return Optional.empty();
        }
        else if (playerRef.equals(us)) {
            return Optional.of("X");
        }
        else {
            return Optional.of("O");
        }
    }
}

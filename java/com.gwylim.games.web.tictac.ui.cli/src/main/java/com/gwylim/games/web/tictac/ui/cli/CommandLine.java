package com.gwylim.games.web.tictac.ui.cli;

import java.util.Map;

import com.gwylim.common.Console;
import com.gwylim.common.Console.SupersededException;
import com.gwylim.games.web.tictac.api.PlayerAPI;
import com.gwylim.games.web.tictac.api.models.meaningful.Password;
import com.gwylim.games.web.tictac.api.models.meaningful.Username;
import com.gwylim.games.web.tictac.api.models.rest.CurrentPlayerApiModel;
import com.gwylim.games.web.tictac.api.requests.SignInApiRequest;
import com.gwylim.games.web.tictac.api.requests.SignUpApiRequest;
import com.gwylim.games.web.tictac.ui.cli.components.LobbyView;
import feign.FeignException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import static com.gwylim.games.web.tictac.api.client.RESTClientConfig.AUTHORIZATION;

@Component
public class CommandLine implements CommandLineRunner {
    private static final Logger LOG = LoggerFactory.getLogger(CommandLine.class);

    private final Console console;
    private final PlayerAPI playerAPI;

    private final LobbyView lobbyView;
    private final SettableAuthTokenProvider settableAuthTokenProvider;
    private final PushClientFactory pushClientFactory;

    @Autowired
    public CommandLine(final Console console,
                       final PlayerAPI playerAPI,
                       final LobbyView lobbyView,
                       final SettableAuthTokenProvider settableAuthTokenProvider,
                       final PushClientFactory pushClientFactory) {

        this.console = console;

        this.playerAPI = playerAPI;
        this.lobbyView = lobbyView;
        this.settableAuthTokenProvider = settableAuthTokenProvider;
        this.pushClientFactory = pushClientFactory;
    }


    @Override
    public void run(final String... args) throws SupersededException {
        LOG.info("Started command line...");

        final CurrentPlayerApiModel player = signInOrUp();

        settableAuthTokenProvider.set(player.getToken());

        LOG.info("Player created: {}", player.getPlayerRef());

        lobbyView.run(player.getPlayerRef(), pushClientFactory.createClient(() -> Map.of(AUTHORIZATION, player.getToken().getValue())));
    }

    private CurrentPlayerApiModel signInOrUp() throws SupersededException {
        while (true) {

            final Integer mode = console.read(Integer.class, "1. Sign In\n2. Sign Up");

            final Username username = new Username(console.read("Enter name:"));
            final Password password = new Password(console.read("Enter password:")); // TODO: Password blanking.

            try {
                if (mode == 1) {
                    return playerAPI.signIn(new SignInApiRequest(username, password));
                }
                else {
                    final Password passwordConfirm = new Password(console.read("Enter password:")); // TODO: Password blanking.
                    if (passwordConfirm.equals(password)) {
                        return playerAPI.signUp(new SignUpApiRequest(username, password));
                    }
                    else {
                        console.printf("Passwords do not match.");
                    }
                }
            }
            catch (final FeignException e) {
                final int status = e.status();
                if (status == HttpStatus.FORBIDDEN.value()) {
                    console.printf("Username or password incorrect.");
                }
                else if (status == HttpStatus.CONFLICT.value()) {
                    console.printf("Username in use.");
                }
                else {
                    console.printf("Unknown error: " + status);
                }
            }
        }
    }
}


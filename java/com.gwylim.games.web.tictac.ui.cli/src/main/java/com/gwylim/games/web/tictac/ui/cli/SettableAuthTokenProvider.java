package com.gwylim.games.web.tictac.ui.cli;

import com.gwylim.games.web.tictac.api.client.AuthTokenProvider;
import com.gwylim.games.web.tictac.api.models.references.PlayerRestToken;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class SettableAuthTokenProvider implements AuthTokenProvider {

    private PlayerRestToken playerToken;

    public void set(final PlayerRestToken playerToken) {
        this.playerToken = playerToken;
    }

    @Override
    public Optional<PlayerRestToken> get() {
        return Optional.ofNullable(playerToken);
    }
}

package com.gwylim.games.web.tictac.ui.cli;

import com.gwylim.common.Console;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;

@SpringBootApplication
@ComponentScan(basePackages = {"com.gwylim"})
@SuppressWarnings({"checkstyle:HideUtilityClassConstructor"}) // Will be subclassed by @SpringBootApplication
public class CliMain {
    public static void main(final String[] args) {
        SpringApplication.run(CliMain.class);
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public static Console consoleBean() {
        return new Console();
    }
}


import com.google.common.base.Joiner;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.ui.cli.components.GameRenderUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class GameViewTest {
    private static final PlayerRef X = PlayerRef.generateNew(); // Our player
    private static final PlayerRef O = PlayerRef.generateNew(); // Other player


    @Test
    public void testRenderFull() {
        final String view = GameRenderUtil.createView(X,
            new PlayerRef[][] {
                {X, X, O},
                {X, X, O},
                {X, X, X}
        });

        Assertions.assertEquals(
            Joiner.on("\n")
                  .join(
                    " X X O",
                    " X X O",
                    " X X X"
                  ),
            view
        );
    }

    @Test
    public void testRenderEmpty() {
        final String view = GameRenderUtil.createView(X,
            new PlayerRef[][] {
                {null, null, null},
                {null, null, null},
                {null, null, null}
            });

        Assertions.assertEquals(
            Joiner.on("\n")
                  .join(
                      " 0 1 2",
                      " 3 4 5",
                      " 6 7 8"
                  ),
            view
        );
    }

}

package com.gwylim.games.web.tictac.api.client;

import com.gwylim.games.web.tictac.api.converters.ReferenceMessageConverter;
import com.gwylim.games.web.tictac.api.models.references.PlayerRestToken;
import feign.RequestInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Optional;

@EnableFeignClients(basePackages = {"com.gwylim"})
@Configuration
public class RESTClientConfig {
    private static final Logger LOG = LoggerFactory.getLogger(RESTClientConfig.class);

    public static final String AUTHORIZATION = "Authorization";

    /**
     * Intercept feign requests to add the auth token if it is present in the UI session.
     */
    @Bean
    public static RequestInterceptor requestInterceptor(final AuthTokenProvider tokenProvider) {
        return template -> {
            final Optional<PlayerRestToken> authorization = tokenProvider.get();

            authorization.ifPresent(token -> template.header(AUTHORIZATION, token.getValue()));
        };
    }

    /**
     * Add converter to allow us to send playerRef as TEXT/PLAIN.
     */
    @Bean
    public ReferenceMessageConverter getConverter() {
        return new ReferenceMessageConverter();
    }
}

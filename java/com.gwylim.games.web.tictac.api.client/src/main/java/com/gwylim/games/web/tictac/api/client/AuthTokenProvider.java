package com.gwylim.games.web.tictac.api.client;

import com.gwylim.games.web.tictac.api.models.references.PlayerRestToken;

import java.util.Optional;
import java.util.function.Supplier;

public interface AuthTokenProvider extends Supplier<Optional<PlayerRestToken>> {

}

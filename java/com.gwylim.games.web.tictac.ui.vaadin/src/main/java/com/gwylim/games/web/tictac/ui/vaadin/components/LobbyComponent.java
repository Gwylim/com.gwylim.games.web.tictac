package com.gwylim.games.web.tictac.ui.vaadin.components;

import com.google.common.collect.Sets;
import com.gwylim.games.web.tictac.api.LobbyAPI;
import com.gwylim.games.web.tictac.api.PlayerAPI;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.api.models.rest.PlayerApiModel;
import com.gwylim.games.web.tictac.ui.vaadin.util.UIUtil;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ClickNotifier;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Set;

@Tag(Tag.DIV)
@HtmlImport("frontend:///style/lobby.html")
@SuppressFBWarnings(value = "SE_BAD_FIELD", justification = "Class not actually serialized")
public class LobbyComponent extends Component implements HasComponents, HasStyle {
    private static final Logger LOG = LoggerFactory.getLogger(LobbyComponent.class);

    private final LobbyAPI lobbyService;
    private final PlayerAPI playerService;

    private final PlayerRef playerRef;

    public LobbyComponent(final LobbyAPI lobbyService,
                          final PlayerAPI playerService,
                          final PlayerRef playerRef) {
        this.lobbyService = lobbyService;
        this.playerService = playerService;
        this.playerRef = playerRef;
        setClassName("lobby-table");
    }

    @Override
    protected void onAttach(final AttachEvent attachEvent) {
        refresh();
    }

    @Tag(Tag.DIV)
    private class LobbyRow extends Component implements HasStyle, HasComponents, ClickNotifier<LobbyRow> {
        private final PlayerRef rowPlayerRef;

        private boolean selected;

        LobbyRow(final PlayerRef rowPlayerRef, final boolean selected) {
            this.rowPlayerRef = rowPlayerRef;
            this.selected = selected;

            final PlayerApiModel rowPlayer = playerService.getPlayer(rowPlayerRef);
            LobbyRow.this.add(new Icon(VaadinIcon.valueOf(rowPlayer.getIcon())));
            LobbyRow.this.add(new Label(rowPlayer.getName()));

            LobbyRow.this.addClickListener(this::click);
            setClassName(rowPlayer.getType().name());
            setClassName("lobby-row");
        }

        private synchronized void click(final ClickEvent<LobbyRow> event) {
            if (event.isFromClient()) {
                selected = !selected;
                if (selected) {
                    lobbyService.addOpponent(playerRef, rowPlayerRef);
                }
                else {
                    lobbyService.removeOpponent(playerRef, rowPlayerRef);
                }
                setClassName("selected", selected);
            }
        }
    }

    public void refresh() {
        // New players excluding self:
        final Set<PlayerRef> newPlayerRefs = Sets.difference(lobbyService.getPlayersInLobby(), Collections.singleton(playerRef));

        // Select all previous selection that is still present:
        final Set<PlayerRef> selectedOpponents = lobbyService.getOpponents(playerRef);

        UIUtil.access(this, () -> {
            removeAll();

            newPlayerRefs.forEach(p -> add(new LobbyRow(p, selectedOpponents.contains(p))));
        });
    }
}


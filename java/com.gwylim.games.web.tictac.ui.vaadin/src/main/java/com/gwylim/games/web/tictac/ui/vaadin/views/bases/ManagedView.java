package com.gwylim.games.web.tictac.ui.vaadin.views.bases;

import com.vaadin.flow.component.AttachNotifier;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.DetachNotifier;
import com.vaadin.flow.component.HasComponents;

public abstract class ManagedView extends Component implements HasComponents, DetachNotifier, AttachNotifier {

    @Override
    protected final void onDetach(final DetachEvent detachEvent) {
        onDetach();
    }

    protected void onDetach() {
        // No action by default.
    }
}

package com.gwylim.games.web.tictac.ui.vaadin.views.bases;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Function;

/**
 * Factory base class for views that have onDetach automatically called when the factory is destroyed.
 * Subclasses must be @UIScope @Component so they are destroyed when the UI is.
 * @param <A> The Construction argument object for creating an instances.
 * @param <T> The instance type.
 */
public abstract class ManagedViewFactory<A, T extends ManagedView> {
    private static final Logger LOG = LoggerFactory.getLogger(ManagedViewFactory.class);

    /**
     * Collect the alive instances.
     */
    private final Set<T> components = new LinkedHashSet<>();

    public final T build(final A a) {
        return register(builder().apply(a));
    }

    protected abstract Function<A, T> builder();

    synchronized T register(final T instance) {
        LOG.info("Register " + instance.getClass().getSimpleName());
        components.add(instance);
        instance.addDetachListener(l -> this.unregister(instance));
        return instance;
    }

    private synchronized void unregister(final T instance) {
        LOG.info("Unregister " + instance.getClass().getSimpleName());
        components.remove(instance);
    }

    @PreDestroy
    private void preDestroy() {
        LOG.info("Destroying " + getClass().getSimpleName());
        components.forEach(ManagedView::onDetach);
    }
}

package com.gwylim.games.web.tictac.ui.vaadin.util;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.server.Command;

public class UIUtil {
    private UIUtil() { }

    public static void access(final Component c, final Command command) {
        c.getUI().filter(ui -> !ui.isClosing())
                 .ifPresent(ui -> ui.access(command));
    }

    /**
     * Create a {@link TextField} with sensible defaults.
     */
    public static TextField createTextField(final String label) {
        final TextField field = new TextField(label);
        field.setRequired(true);
        field.setPattern(".*[^ ].*"); // Disallow only whitespace.
        field.setPreventInvalidInput(true); // Will just drop whitespace as first characters.
        field.addFocusListener(f -> UIUtil.access(field, () -> field.setInvalid(false)));
        return field;
    }

    /**
     * Create a {@link PasswordField} with sensible defaults.
     */
    public static PasswordField createPasswordField(final String label) {
        final PasswordField field = new PasswordField(label);
        field.setRequired(true);
        field.setPattern(".*[^ ].*"); // Disallow only whitespace.
        field.setPreventInvalidInput(true); // Will just drop whitespace as first characters.
        field.addFocusListener(f -> UIUtil.access(field, () -> field.setInvalid(false)));
        return field;
    }
}

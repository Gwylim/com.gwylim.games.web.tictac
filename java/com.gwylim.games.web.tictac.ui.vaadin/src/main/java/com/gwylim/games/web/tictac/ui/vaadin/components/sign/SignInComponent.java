package com.gwylim.games.web.tictac.ui.vaadin.components.sign;

import java.util.function.Consumer;

import com.gwylim.games.web.tictac.api.PlayerAPI;
import com.gwylim.games.web.tictac.api.models.meaningful.Password;
import com.gwylim.games.web.tictac.api.models.meaningful.Username;
import com.gwylim.games.web.tictac.api.models.rest.CurrentPlayerApiModel;
import com.gwylim.games.web.tictac.api.requests.SignInApiRequest;
import com.gwylim.games.web.tictac.ui.vaadin.util.UIUtil;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import feign.FeignException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.vaadin.marcus.shortcut.Shortcut;

@Tag(Tag.DIV)
@SuppressFBWarnings(value = "SE_BAD_FIELD", justification = "Class not actually serialized")
public class SignInComponent extends Component implements HasComponents {
    private static final Logger LOG = LoggerFactory.getLogger(SignInComponent.class);

    private final PlayerAPI playerService;
    private final Consumer<CurrentPlayerApiModel> playerCallback;

    private final TextField usernameField = UIUtil.createTextField("User name");
    private final PasswordField passwordField = UIUtil.createPasswordField("Password");

    public SignInComponent(final PlayerAPI playerService, final Consumer<CurrentPlayerApiModel> playerCallback, final Runnable switchToSignUp) {
        this.playerService = playerService;
        this.playerCallback = playerCallback;

        usernameField.setAutofocus(true);

        add(new VerticalLayout(
            usernameField,
            passwordField,
            new Button("OK", e -> this.submit()),
            new Button("Sign Up", e -> switchToSignUp.run())
        ));


        // Make return submit.
        Shortcut.add(usernameField, Key.ENTER, this::submit);
    }

    private void submit() {
        final String username = usernameField.getValue().trim();
        final String password = passwordField.getValue();

        try {
            playerCallback.accept(playerService.signIn(new SignInApiRequest(new Username(username), new Password(password))));
            setVisible(false);
        }
        catch (final FeignException e) {
            if (e.status() == HttpStatus.FORBIDDEN.value()) {
                UIUtil.access(this, () -> {
                    usernameField.setErrorMessage("Username or password incorrect.");
                    usernameField.setInvalid(true);
                });
            }
            else {
                LOG.warn("Unknown error response: " + e.status());
            }
        }
    }
}


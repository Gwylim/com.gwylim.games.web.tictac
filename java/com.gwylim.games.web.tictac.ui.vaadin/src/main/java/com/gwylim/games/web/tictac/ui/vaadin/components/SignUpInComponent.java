package com.gwylim.games.web.tictac.ui.vaadin.components;

import java.util.function.Consumer;

import com.gwylim.games.web.tictac.api.PlayerAPI;
import com.gwylim.games.web.tictac.api.models.rest.CurrentPlayerApiModel;
import com.gwylim.games.web.tictac.ui.vaadin.components.sign.SignInComponent;
import com.gwylim.games.web.tictac.ui.vaadin.components.sign.SignUpComponent;
import com.gwylim.games.web.tictac.ui.vaadin.util.UIUtil;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.html.Section;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Tag(Tag.DIV)
@SuppressFBWarnings(value = "SE_BAD_FIELD", justification = "Class not actually serialized")
public class SignUpInComponent extends Component implements HasComponents {
    private static final Logger LOG = LoggerFactory.getLogger(SignUpInComponent.class);

    private final PlayerAPI playerService;
    private final Consumer<CurrentPlayerApiModel> playerCallback;

    private final Section container = new Section();

    public SignUpInComponent(final PlayerAPI playerService, final Consumer<CurrentPlayerApiModel> playerCallback) {
        this.playerService = playerService;
        this.playerCallback = playerCallback;

        this.add(container);
    }

    @Override
    protected void onAttach(final AttachEvent attachEvent) {
        signUp();
    }

    private void signUp() {
        switchOutContent(new SignUpComponent(playerService, playerCallback, this::signIn));
    }

    private void signIn() {
        switchOutContent(new SignInComponent(playerService, playerCallback, this::signUp));
    }

    private void switchOutContent(final Component view) {
        UIUtil.access(this, () -> {
            container.removeAll();
            container.add(view);
        });
    }
}


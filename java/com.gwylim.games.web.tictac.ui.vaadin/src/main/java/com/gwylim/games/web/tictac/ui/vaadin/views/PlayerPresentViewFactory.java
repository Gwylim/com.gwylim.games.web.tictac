package com.gwylim.games.web.tictac.ui.vaadin.views;

import java.util.Map;
import java.util.function.Function;

import com.gwylim.games.web.tictac.api.PlayerAPI;
import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.api.models.rest.CurrentPlayerApiModel;
import com.gwylim.games.web.tictac.ui.vaadin.PushClientFactory;
import com.gwylim.games.web.tictac.ui.vaadin.util.UIUtil;
import com.gwylim.games.web.tictac.ui.vaadin.views.GameViewFactory.GameViewArgs;
import com.gwylim.games.web.tictac.ui.vaadin.views.LobbyViewFactory.LobbyViewArgs;
import com.gwylim.games.web.tictac.ui.vaadin.views.bases.ManagedView;
import com.gwylim.games.web.tictac.ui.vaadin.views.bases.ManagedViewFactory;
import com.gwylim.system.push.messaging.client.PushClient;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.ClientCallable;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Section;
import com.vaadin.flow.spring.annotation.UIScope;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.gwylim.games.web.tictac.api.client.RESTClientConfig.AUTHORIZATION;


/**
 * Factory for the creation of GameViews.
 */
@UIScope
@Component
@SuppressFBWarnings(value = "SE_BAD_FIELD", justification = "Class not actually serialized")
public final class PlayerPresentViewFactory extends ManagedViewFactory<CurrentPlayerApiModel, PlayerPresentViewFactory.PlayerPresentView> {
    private static final Logger LOG = LoggerFactory.getLogger(PlayerPresentViewFactory.class);

    private final LobbyViewFactory lobbyViewFactory;
    private final GameViewFactory gameViewFactory;
    private final PlayerAPI playerService;

    private final PushClientFactory pushClientFactory;

    @Autowired
    private PlayerPresentViewFactory(final LobbyViewFactory lobbyViewFactory,
                                     final GameViewFactory gameViewFactory,
                                     final PushClientFactory pushClientFactory,
                                     final PlayerAPI playerService) {

        this.lobbyViewFactory = lobbyViewFactory;
        this.gameViewFactory = gameViewFactory;
        this.pushClientFactory = pushClientFactory;
        this.playerService = playerService;
    }

    protected Function<CurrentPlayerApiModel, PlayerPresentView> builder() {
        return PlayerPresentView::new;
    }


    @Tag(Tag.DIV)
    public final class PlayerPresentView extends ManagedView implements HasStyle {

        private final PlayerRef playerRef;
        private final PushClient pushClient;

        private final Section container = new Section();

        public PlayerPresentView(final CurrentPlayerApiModel player) {
            this.playerRef = player.getPlayerRef();
            this.pushClient = pushClientFactory.createClient(() -> Map.of(AUTHORIZATION, player.getToken().getValue()));

            addClassName("player-present");

            container.setWidth("100%");
            add(container);
        }

        @Override
        protected void onAttach(final AttachEvent attachEvent) {
            // Create javascript hook that calls disconnected() when the window/tab is closed:
            getUI().ifPresent(ui -> ui.getPage().executeJavaScript("window.onbeforeunload = function (e) { var e = e || window.event; $0.$server.disconnected(); return; };", getElement()));

            toLobby();
        }

        @ClientCallable
        public void disconnected() {
            // Terminate the UI which kills the scope and releases all associated UIScope resources:
            getUI().ifPresent(UI::close);
            playerService.disconnect(playerRef);
        }

        private void toLobby() {
            LOG.info("Switch to LobbyView.");
            switchOutContent(lobbyViewFactory.build(new LobbyViewArgs(
                playerRef,
                pushClient,
                this::gameStarted
            )));
        }

        private void gameStarted(final GameRef gameRef) {
            LOG.info("Switch to GameView.");
            switchOutContent(gameViewFactory.build(new GameViewArgs(
                playerRef,
                gameRef,
                pushClient,
                this::toLobby
            )));
        }

        private void switchOutContent(final ManagedView view) {
            UIUtil.access(this, () -> {
                container.removeAll();
                container.add(view);
            });
        }
    }
}



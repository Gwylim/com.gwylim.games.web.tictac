package com.gwylim.games.web.tictac.ui.vaadin;

import com.gwylim.games.web.tictac.api.client.AuthTokenProvider;
import com.gwylim.games.web.tictac.api.models.references.PlayerRestToken;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.server.VaadinSession;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.gwylim.games.web.tictac.api.client.RESTClientConfig.AUTHORIZATION;

/**
 * Used to set the headers on feign requests.
 */
@Component
public class UIAuthTokenProvider implements AuthTokenProvider {
    @Override
    public Optional<PlayerRestToken> get() {
        final Optional<UI> ui = Optional.ofNullable(UI.getCurrent());

        if (ui.isEmpty()) {
            throw new IllegalStateException("Failed to get UI on this thread.");
        }

        return ui.map(UI::getSession)
                .map(VaadinSession::getSession)
                .map(s -> s.getAttribute(AUTHORIZATION))
                .map(Object::toString)
                .map(PlayerRestToken::new);
    }
}

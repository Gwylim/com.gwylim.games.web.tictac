package com.gwylim.games.web.tictac.ui.vaadin.components;

import com.gwylim.common.ArrayUtil;
import com.gwylim.games.web.tictac.ui.vaadin.util.UIUtil;
import com.vaadin.flow.component.ClickNotifier;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.html.Div;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.BiConsumer;

@Tag(Tag.DIV)
@HtmlImport("frontend:///style/board.html")
@SuppressFBWarnings(value = "SE_BAD_FIELD", justification = "Class not actually serialized")
public class BoardComponent extends Component implements HasComponents, HasStyle {
    private final BiConsumer<Integer, Integer> boardClickedCallback;

    @Tag(Tag.DIV)
    private final class Square extends Component implements HasStyle, HasComponents, ClickNotifier<Square> {
        private Square(final int x, final int y) {
            setClassName("square");

            final Div icon = new Div();
            icon.setClassName("icon");
            add(icon);

            addClickListener(evt -> BoardComponent.this.click(x, y));
        }

        public void win(final boolean win) {
            if (win) {
                addClassName("win");
            }
        }
    }

    public enum SquareState {
        OURS("X"),
        THEIRS("O"),
        EMPTY(null);

        private String className;

        SquareState(final String className) {
            this.className = className;
        }

        public Optional<String> getClassName() {
            return Optional.ofNullable(className);
        }

        private void apply(final Square square) {
            getClassName().ifPresent(square::addClassName);
        }
    }

    private final Square[][] board;

    public BoardComponent(final int x, final int y, final BiConsumer<Integer, Integer> boardClickedCallback) {
        setClassName("board");

        this.boardClickedCallback = boardClickedCallback;

        board = ArrayUtil.initialise(x, y, Square.class, Square::new);

        Arrays.stream(board).forEach(row -> {
            final Div rowDiv = new Div(row);
            rowDiv.setClassName("row");
            add(rowDiv);
        });
    }

    private void click(final int x, final int y) {
        boardClickedCallback.accept(x, y);
    }

    public void update(final SquareState[][] newState, final Boolean[][] winMap) {
        UIUtil.access(this, () -> {
            ArrayUtil.parallelConsume(newState, board, SquareState::apply);

            ArrayUtil.parallelConsume(board, winMap, Square::win);
        });
    }
}


package com.gwylim.games.web.tictac.ui.vaadin.views;

import com.gwylim.common.ArrayUtil;
import com.gwylim.games.web.tictac.api.GameAPI;
import com.gwylim.games.web.tictac.api.models.push.game.GameEndedPushModel;
import com.gwylim.games.web.tictac.api.models.push.game.GameMoveMadePushModel;
import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.api.models.rest.GameApiModel;
import com.gwylim.games.web.tictac.api.requests.MakeMoveApiRequest;
import com.gwylim.games.web.tictac.ui.vaadin.components.BoardComponent;
import com.gwylim.games.web.tictac.ui.vaadin.util.UIUtil;
import com.gwylim.games.web.tictac.ui.vaadin.views.bases.ManagedViewFactory;
import com.gwylim.games.web.tictac.ui.vaadin.views.bases.ManagedView;
import com.gwylim.system.push.messaging.client.PushClient;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.spring.annotation.UIScope;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Function;

/**
 * Factory for the creation of GameViews.
 */
@UIScope
@Component
@SuppressFBWarnings(value = "SE_BAD_FIELD", justification = "Class not actually serialized")
public final class GameViewFactory extends ManagedViewFactory<GameViewFactory.GameViewArgs, GameViewFactory.GameView> {
    private static final Logger LOG = LoggerFactory.getLogger(GameViewFactory.class);
    
    private static final int NOTIFICATION_DURATION = 1000;
    private static final int BOARD_SIZE = 3;

    private final GameAPI gameService;


    @Autowired
    public GameViewFactory(final GameAPI gameService) {
        this.gameService = gameService;
    }

    public Function<GameViewArgs, GameView> builder() {
        return GameView::new;
    }

    public static final class GameViewArgs {
        private final PlayerRef playerRef;
        private final GameRef gameRef;
        private final PushClient pushClient;
        private final Runnable gameOverCallback;

        public GameViewArgs(final PlayerRef playerRef, final GameRef gameRef, final PushClient pushClient, final Runnable gameOverCallback) {
            this.playerRef = playerRef;
            this.gameRef = gameRef;
            this.pushClient = pushClient;
            this.gameOverCallback = gameOverCallback;
        }
    }

    @Tag(Tag.DIV)
    @HtmlImport("frontend:///style/game.html")
    public final class GameView extends ManagedView {

        private final PlayerRef playerRef;
        private final GameRef gameRef;

        private final BoardComponent boardComponent;
        private final Label stateIndicator;
        private final Runnable gameOverCallback;

        private final PushClient.Subscription gameUpdateSubscription;
        private final PushClient.Subscription gameEndedSubscription;

        private GameView(final GameViewArgs args) {
            this.playerRef = args.playerRef;
            this.gameRef = args.gameRef;
            final PushClient pushClient = args.pushClient;
            this.gameOverCallback = args.gameOverCallback;

            boardComponent = new BoardComponent(BOARD_SIZE, BOARD_SIZE, this::moveMade);
            add(boardComponent);

            stateIndicator = new Label();
            stateIndicator.setClassName("state-indicator");
            add(stateIndicator);

            gameUpdateSubscription = pushClient.subscribe(gameRef, this::updateUIAfterMove);
            gameEndedSubscription = pushClient.subscribe(gameRef, this::gameEnded);
        }

        @Override
        protected void onAttach(final AttachEvent attachEvent) {
            final GameApiModel game = gameService.getGame(gameRef);
            stateIndicator.setText(game.getToGo().map(playerRef::equals).orElse(false) ?  "Your Turn" : "Their turn");
        }

        private void moveMade(final int x, final int y) {
            final GameApiModel game = gameService.getGame(gameRef);
            if (game.getToGo().map(playerRef::equals).orElse(false)) {
                gameService.makeMove(gameRef, playerRef, new MakeMoveApiRequest(x, y));
            }
        }


        @Override
        protected void onDetach() {
            gameService.leave(gameRef, playerRef);

            gameUpdateSubscription.unsubscribe();
            gameEndedSubscription.unsubscribe();
        }

        private void updateUIAfterMove(final GameMoveMadePushModel event) {
            this.getUI().ifPresent(UI::setCurrent);
            final GameApiModel game = gameService.getGame(gameRef);
            final BoardComponent.SquareState[][] newState = ArrayUtil.mapValues(game.getBoard(), this::toSquareState, BoardComponent.SquareState.class);

            boardComponent.update(newState, game.getWinMap());
            updateStateLabels(game);
        }

        private void gameEnded(final GameEndedPushModel event) {
            this.getUI().ifPresent(UI::setCurrent);

            UIUtil.access(this, () -> {
                final GameApiModel game = gameService.getGame(event.getGame());

                if (game.getWinner().isEmpty()) {
                    if (game.getToGo().isPresent()) {
                        add(new Label("Other player left."));
                    }
                    else {
                        add(new Label("Game is a draw."));
                    }
                }

                add(new Button("Back to lobby", e ->
                    gameOverCallback.run()
                ));
            });
        }

        private BoardComponent.SquareState toSquareState(final PlayerRef cell) {
            if (cell == null) {
                return BoardComponent.SquareState.EMPTY;
            }
            else if (cell.equals(playerRef)) {
                return BoardComponent.SquareState.OURS;
            }
            else {
                return BoardComponent.SquareState.THEIRS;
            }
        }

        private void updateStateLabels(final GameApiModel game) {
            UIUtil.access(this, () -> {
                game.getToGo().map(playerRef::equals).ifPresent(ourTurn ->
                        stateIndicator.setText(ourTurn ? "Your Turn" : "Their turn")
                );

                game.getWinner().map(playerRef::equals).ifPresent(weWon -> {
                            Notification.show(weWon ? "You Won!" : "You Lost!", NOTIFICATION_DURATION, Notification.Position.MIDDLE);
                            stateIndicator.setText(weWon ? "You Won!" : "You Lost!");
                            stateIndicator.setClassName("win", weWon);
                            stateIndicator.setClassName("lose", !weWon);
                        }
                );
            });
        }

    }
}



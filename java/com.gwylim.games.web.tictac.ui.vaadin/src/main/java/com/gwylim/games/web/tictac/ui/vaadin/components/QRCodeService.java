package com.gwylim.games.web.tictac.ui.vaadin.components;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.util.Optional;

@Component
public class QRCodeService {
    private static final Logger LOG = LoggerFactory.getLogger(QRCodeService.class);
    private static final int CACHE_SIZE = 5;

    // Expect that there are only a small number of URLs that the service will be available on:
    private final Cache<String, byte[]> cache = CacheBuilder.newBuilder().maximumSize(CACHE_SIZE).build();

    public byte[] getOrGenerateQRCodeBytes(final String content) {
        return Optional.ofNullable(cache.getIfPresent(content))
                       .orElseGet(() -> {
                           final byte[] generateQRCodeBytes = getGenerateQRCodeBytes(content);
                           cache.put(content, generateQRCodeBytes);
                           return generateQRCodeBytes;
                       });
    }

    private byte[] getGenerateQRCodeBytes(final String content) {
        LOG.info("Generating QRCode for '" + content + "'.");

        try {
            final QRCodeWriter qrCodeWriter = new QRCodeWriter();
            final BitMatrix bitMatrix = qrCodeWriter.encode(content, BarcodeFormat.QR_CODE, 100, 100);

            final MatrixToImageConfig matrixToImageConfig = new MatrixToImageConfig(MatrixToImageConfig.BLACK, 0x00FFFFFF);

            final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(bitMatrix, "PNG", buffer, matrixToImageConfig);
            return buffer.toByteArray();
        }
        catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }
}

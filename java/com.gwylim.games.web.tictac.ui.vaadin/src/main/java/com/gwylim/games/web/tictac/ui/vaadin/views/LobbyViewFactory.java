package com.gwylim.games.web.tictac.ui.vaadin.views;

import java.util.function.Consumer;
import java.util.function.Function;

import com.gwylim.games.web.tictac.api.LobbyAPI;
import com.gwylim.games.web.tictac.api.PlayerAPI;
import com.gwylim.games.web.tictac.api.models.push.game.GameStartedPushModel;
import com.gwylim.games.web.tictac.api.models.push.player.PlayerEnteredLobbyPushModel;
import com.gwylim.games.web.tictac.api.models.push.player.PlayerLeftLobbyPushModel;
import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.ui.vaadin.components.LobbyComponent;
import com.gwylim.games.web.tictac.ui.vaadin.views.bases.ManagedView;
import com.gwylim.games.web.tictac.ui.vaadin.views.bases.ManagedViewFactory;
import com.gwylim.system.push.messaging.client.PushClient;
import com.gwylim.system.push.messaging.client.PushClient.Subscription;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.spring.annotation.UIScope;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@UIScope
@Component
@SuppressFBWarnings(value = "SE_BAD_FIELD", justification = "Class not actually serialized")
public final class LobbyViewFactory extends ManagedViewFactory<LobbyViewFactory.LobbyViewArgs, LobbyViewFactory.LobbyView> {
    private static final Logger LOG = LoggerFactory.getLogger(LobbyViewFactory.class);
    private static final int NOTIFICATION_DURATION = 500;

    private final LobbyAPI lobbyService;
    private final PlayerAPI playerService;

    @Autowired
    private LobbyViewFactory(final LobbyAPI lobbyService,
                             final PlayerAPI playerService) {

        this.lobbyService = lobbyService;
        this.playerService = playerService;
    }

    protected Function<LobbyViewArgs, LobbyViewFactory.LobbyView> builder() {
        return LobbyView::new;
    }

    public static final class LobbyViewArgs {
        private final PlayerRef playerRef;
        private final PushClient pushClient;
        private final Consumer<GameRef> gameOverCallback;

        public LobbyViewArgs(final PlayerRef playerRef, final PushClient pushClient, final Consumer<GameRef> gameOverCallback) {
            this.playerRef = playerRef;
            this.pushClient = pushClient;
            this.gameOverCallback = gameOverCallback;
        }
    }

    @Tag(Tag.DIV)
    public final class LobbyView extends ManagedView implements HasStyle {

        private final PlayerRef playerRef;

        private final LobbyComponent lobbyComponent;
        private final Consumer<GameRef> gameOverCallback;

        private final Subscription playerEnteredSubscription;
        private final Subscription playerLeftSubscription;

        private final Subscription gameStartedSubscription;

        public LobbyView(final LobbyViewArgs args) {
            this.playerRef = args.playerRef;
            final PushClient pushClient = args.pushClient;
            this.gameOverCallback = args.gameOverCallback;

            lobbyComponent = new LobbyComponent(lobbyService, playerService, playerRef);

            add(lobbyComponent);

            gameStartedSubscription = pushClient.subscribe(playerRef, this::gameStarted);
            playerEnteredSubscription = pushClient.subscribe(this::playerEnteredLobby);
            playerLeftSubscription = pushClient.subscribe(this::playerLeftLobby);
        }

        private void gameStarted(final GameStartedPushModel gameStartedPushModel) {
            gameOverCallback.accept(gameStartedPushModel.getGame());
        }


        @Override
        protected void onAttach(final AttachEvent attachEvent) {
            lobbyService.enterLobby(playerRef);
            Notification.show("Loading lobby", NOTIFICATION_DURATION, Notification.Position.MIDDLE);

            LOG.info("Attached: " + this);
        }

        @Override
        protected void onDetach() {
            playerEnteredSubscription.unsubscribe();
            playerLeftSubscription.unsubscribe();
            gameStartedSubscription.unsubscribe();

            lobbyService.exitLobby(playerRef);
        }

        private void playerEnteredLobby(final PlayerEnteredLobbyPushModel event) {
            this.getUI().ifPresent(UI::setCurrent);
            lobbyComponent.refresh();
        }
        private void playerLeftLobby(final PlayerLeftLobbyPushModel event) {
            this.getUI().ifPresent(UI::setCurrent);
            lobbyComponent.refresh();
        }
    }
}


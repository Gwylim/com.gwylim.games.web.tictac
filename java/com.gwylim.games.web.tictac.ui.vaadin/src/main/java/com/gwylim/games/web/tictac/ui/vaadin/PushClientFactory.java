package com.gwylim.games.web.tictac.ui.vaadin;

import com.gwylim.system.push.messaging.client.PushClientFactoryBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PushClientFactory extends PushClientFactoryBase {
    @Autowired
    public PushClientFactory(@Value("${com.gwylim.games.web.tictac.api.websocket}") final String url) {
        super(url);
    }
}

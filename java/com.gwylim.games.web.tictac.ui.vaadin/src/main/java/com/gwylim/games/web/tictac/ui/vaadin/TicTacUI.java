package com.gwylim.games.web.tictac.ui.vaadin;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;

import com.gwylim.games.web.tictac.api.PlayerAPI;
import com.gwylim.games.web.tictac.api.models.rest.CurrentPlayerApiModel;
import com.gwylim.games.web.tictac.ui.vaadin.components.SignUpInComponent;
import com.gwylim.games.web.tictac.ui.vaadin.components.QRCodeService;
import com.gwylim.games.web.tictac.ui.vaadin.util.UIUtil;
import com.gwylim.games.web.tictac.ui.vaadin.views.PlayerPresentViewFactory;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Section;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinRequest;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinServletRequest;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.gwylim.games.web.tictac.api.client.RESTClientConfig.AUTHORIZATION;

@Push
@UIScope
@Component
@Route("")
@Theme(value = Lumo.class)
@HtmlImport("frontend:///style/styles.html")
@HtmlImport("frontend://bower_components/shadycss/apply-shim.html")
@HtmlImport("frontend://bower_components/shadycss/custom-style-interface.html")
@SuppressFBWarnings(value = "SE_BAD_FIELD", justification = "Class not actually serialized")
public class TicTacUI extends VerticalLayout {
    private static final Logger LOG = LoggerFactory.getLogger(TicTacUI.class);

    private final PlayerPresentViewFactory playerPresentViewFactory;
    private final QRCodeService qrCodeService;

    private final Section container = new Section();

    @Autowired
    public TicTacUI(final PlayerPresentViewFactory playerPresentViewFactory,
                    final QRCodeService qrCodeService,
                    final PlayerAPI playerService) {

        this.playerPresentViewFactory = playerPresentViewFactory;

        this.qrCodeService = qrCodeService;

        container.setWidth("100%");
        container.add(new SignUpInComponent(playerService, this::playerNameSelected));
        add(container);

        final Image qrCode = new Image(createQRCodeFor(getRequestURI()), "QR Code");
        qrCode.setClassName("qr-code");
        add(qrCode);
    }


    private StreamResource createQRCodeFor(final String content) {
        final byte[] buf = qrCodeService.getOrGenerateQRCodeBytes(content);

        return new StreamResource("TecTacQRCode.png", () -> new ByteArrayInputStream(buf));
    }

    private String getRequestURI() {
        final VaadinRequest vaadinRequest = VaadinService.getCurrentRequest();
        final HttpServletRequest httpServletRequest = ((VaadinServletRequest) vaadinRequest).getHttpServletRequest();
        return httpServletRequest.getRequestURL().toString();
    }

    private void playerNameSelected(final CurrentPlayerApiModel currentPlayerApiModel) {
        LOG.info("Player logged in: " + currentPlayerApiModel.getPlayerRef() + " - " + currentPlayerApiModel.getName());

        UI.getCurrent().getSession().getSession().setAttribute(AUTHORIZATION, currentPlayerApiModel.getToken().getValue());

        UIUtil.access(this, () -> {
            container.removeAll();
            container.add(playerPresentViewFactory.build(currentPlayerApiModel));
        });
    }
}

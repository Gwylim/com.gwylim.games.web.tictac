package com.gwylim.games.web.tictac.ui.vaadin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.gwylim"})
public class VaadinMain {

    private static final Logger LOG = LoggerFactory.getLogger(VaadinMain.class);

    public static void main(final String[] args) {
        SpringApplication.run(VaadinMain.class);
    }

    @Bean
    public CommandLineRunner logStartup() {
        return (args) -> LOG.info("Application started.");
    }

}


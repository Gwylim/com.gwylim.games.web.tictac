package com.gwylim.games.web.tictac.api.impl.servers;

import com.gwylim.games.web.tictac.api.models.push.game.GameEndedPushModel;
import com.gwylim.games.web.tictac.api.models.push.game.GameMoveMadePushModel;
import com.gwylim.games.web.tictac.api.models.push.game.GameStartedPushModel;
import com.gwylim.games.web.tictac.events.GameEndedEvent;
import com.gwylim.games.web.tictac.events.GameMoveMadeEvent;
import com.gwylim.games.web.tictac.events.GameStartedEvent;
import com.gwylim.system.events.api.annotations.EventListener;
import com.gwylim.system.push.messaging.server.PushServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class GamePushServer {
    private static final Logger LOG = LoggerFactory.getLogger(GamePushServer.class);

    private final PushServer pushServer;

    @Autowired
    public GamePushServer(final PushServer pushServer) {
        this.pushServer = pushServer;
    }


    @EventListener
    public void gameStarted(final GameStartedEvent event) {
        LOG.info("Pushing GameStartedEvent: " + event.getGameRef());
        pushServer.send(new GameStartedPushModel(event.getGameRef(), event.getPlayers()));
    }

    @EventListener
    public void gameEnded(final GameEndedEvent event) {
        LOG.info("Pushing GameEndedEvent: " + event.getGameRef());
        pushServer.send(new GameEndedPushModel(event.getGameRef(), event.getPlayers()));
    }

    @EventListener
    public void moveMade(final GameMoveMadeEvent event) {
        LOG.info("Pushing GameMoveMadeEvent: " + event.getGameRef());
        pushServer.send(new GameMoveMadePushModel(event.getGameRef(), event.getPlayers(), event.getMover()));
    }
}

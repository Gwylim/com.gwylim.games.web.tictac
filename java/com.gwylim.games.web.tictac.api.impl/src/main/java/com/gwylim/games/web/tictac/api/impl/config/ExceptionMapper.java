package com.gwylim.games.web.tictac.api.impl.config;


import com.gwylim.common.exceptions.NotFoundException;
import feign.Response;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@Component
public class ExceptionMapper {

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    public Response exceptionHandler(final NotFoundException exception) {
        return Response.builder().status(HttpStatus.NOT_FOUND.value())
                .reason(exception.getMessage())
                .build();
    }
}



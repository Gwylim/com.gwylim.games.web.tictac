package com.gwylim.games.web.tictac.api.impl.tokens.model;

import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRestToken;
import com.gwylim.games.web.tictac.api.models.references.PlayerWebsocketToken;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.Optional;

public class PlayerTokensDto {

    private final PlayerRestToken playerToken;
    private final PlayerWebsocketToken playerWebsocketToken;
    private final PlayerRef playerRef;
    private final LocalDateTime updatedAt;

    public PlayerTokensDto(final PlayerRestToken playerToken,
                           @Nullable final PlayerWebsocketToken playerWebsocketToken,
                           final PlayerRef playerRef,
                           final LocalDateTime updatedAt) {

        this.playerToken = playerToken;
        this.playerWebsocketToken = playerWebsocketToken;
        this.playerRef = playerRef;
        this.updatedAt = updatedAt;
    }

    public PlayerRestToken getPlayerToken() {
        return playerToken;
    }

    public Optional<PlayerWebsocketToken> getPlayerWebsocketToken() {
        return Optional.ofNullable(playerWebsocketToken);
    }

    public PlayerRef getPlayerRef() {
        return playerRef;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("playerToken", playerToken)
                .append("playerWebsocketToken", playerWebsocketToken)
                .append("playerRef", playerRef)
                .append("updatedAt", updatedAt)
                .toString();
    }
}

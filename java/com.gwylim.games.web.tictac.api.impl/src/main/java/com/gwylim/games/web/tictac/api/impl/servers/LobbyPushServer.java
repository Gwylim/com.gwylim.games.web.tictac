package com.gwylim.games.web.tictac.api.impl.servers;

import com.gwylim.games.web.tictac.api.models.push.player.PlayerEnteredLobbyPushModel;
import com.gwylim.games.web.tictac.api.models.push.player.PlayerLeftLobbyPushModel;
import com.gwylim.games.web.tictac.events.LobbyEvents.PlayerEnteredLobbyEvent;
import com.gwylim.games.web.tictac.events.LobbyEvents.PlayerLeftLobbyEvent;
import com.gwylim.system.events.api.annotations.EventListener;
import com.gwylim.system.push.messaging.server.PushServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class LobbyPushServer {
    private static final Logger LOG = LoggerFactory.getLogger(LobbyPushServer.class);

    private final PushServer pushServer;

    @Autowired
    public LobbyPushServer(final PushServer pushServer) {
        this.pushServer = pushServer;
    }


    @EventListener
    public void playerEnteredLobby(final PlayerEnteredLobbyEvent event) {
        LOG.info("Pushing player entered: " + event.getPlayerRef());
        pushServer.send(new PlayerEnteredLobbyPushModel(event.getPlayerRef()));
    }

    @EventListener
    public void playerLeftLobbyEvent(final PlayerLeftLobbyEvent event) {
        LOG.info("Pushing player left: " + event.getPlayerRef());
        pushServer.send(new PlayerLeftLobbyPushModel(event.getPlayerRef()));
    }
}
package com.gwylim.games.web.tictac.api.impl.auth;


import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Place on parameter to indicate that it is to be used to check authorisation.
 * e.g Place on the identifier for the entity performing the action to check that
 *     the connection is authorised to act on behalf of that entity.
 */
@Target({PARAMETER})
@Retention(RUNTIME)
public @interface Auth {
}

package com.gwylim.games.web.tictac.api.impl.converters;

import com.gwylim.games.web.tictac.api.models.rest.GameApiModel;
import com.gwylim.games.web.tictac.service.game.model.Game;
import org.springframework.stereotype.Component;

import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class GameToApiModel implements Function<Game, GameApiModel> {
    @Override
    public GameApiModel apply(final Game game) {
        return new GameApiModel(game.getGameRef(),
                                game.getPlayers().stream().collect(Collectors.toSet()),
                                game.toGo().orElse(null),
                                game.winner().orElse(null),
                                game.getBoard().getBoard(),
                                game.getWinMap());
    }
}

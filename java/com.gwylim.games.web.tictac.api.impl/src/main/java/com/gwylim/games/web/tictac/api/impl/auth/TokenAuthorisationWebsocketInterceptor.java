package com.gwylim.games.web.tictac.api.impl.auth;

import com.gwylim.common.ReflectionUtil;
import com.gwylim.common.TextUtil;
import com.gwylim.games.web.tictac.api.impl.tokens.TokenService;
import com.gwylim.games.web.tictac.api.impl.tokens.model.PlayerTokensDto;
import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRestToken;
import com.gwylim.games.web.tictac.api.models.references.PlayerWebsocketToken;
import com.gwylim.games.web.tictac.events.PlayerDisconnectedEvent;
import com.gwylim.games.web.tictac.service.game.GameService;
import com.gwylim.games.web.tictac.service.game.model.Game;
import com.gwylim.system.events.api.EventBroadcaster;
import com.gwylim.system.push.messaging.common.SubscribeBy;
import com.gwylim.system.push.messaging.common.SubscriptionPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.lang.String.format;

/**
 * Intercept incoming SUBSCRIBE messages and check for auth.
 *
 * https://stackoverflow.com/questions/44852776/spring-mvc-websockets-with-stomp-authenticate-against-specific-channels
 *
 * Based on:
 * https://github.com/spring-projects/spring-security/blob/master/messaging/src/main/java/org/springframework/security/messaging/web/csrf/CsrfChannelInterceptor.java
 *
 */
@Component
public class TokenAuthorisationWebsocketInterceptor implements ChannelInterceptor {

    private static final Logger LOG = LoggerFactory.getLogger(TokenAuthorisationWebsocketInterceptor.class);

    private final Map<Pattern, Class<?>> modelsByPathPattern;

    private final TokenService tokenService;
    private final GameService gameService;
    private final EventBroadcaster eventBroadcaster;


    /**
     * Class scanner.
     * Create as bean/inject for testing.
     */
    @Bean
    public static ClassPathScanningCandidateComponentProvider getClasspathScanner() {
        return new ClassPathScanningCandidateComponentProvider(false);
    }

    /**
     * Get all push model classes.
     * https://stackoverflow.com/questions/259140/scanning-java-annotations-at-runtime
     */
    @Autowired
    public TokenAuthorisationWebsocketInterceptor(final TokenService tokenService,
                                                  final GameService gameService,
                                                  final EventBroadcaster eventBroadcaster,
                                                  final ClassPathScanningCandidateComponentProvider scanner) {
        this.tokenService = tokenService;
        this.gameService = gameService;
        this.eventBroadcaster = eventBroadcaster;

        scanner.addIncludeFilter(new AnnotationTypeFilter(SubscriptionPath.class));

        modelsByPathPattern = scanner.findCandidateComponents("com.gwylim").stream()
                .map(BeanDefinition::getBeanClassName)
                .map(ReflectionUtil::getClassByName)
                .collect(Collectors.toMap(
                        TokenAuthorisationWebsocketInterceptor::getPattern,
                        Function.identity()
                ));

        modelsByPathPattern.entrySet().stream().map(Objects::toString).forEach(LOG::info);
    }

    /**
     * Get the path pattern for this class.
     * Verify that the path is valid for the associated class.
     */
    private static Pattern getPattern(final Class<?> clazz) {
        final SubscriptionPath annotation = ReflectionUtil.getAnnotation(clazz, SubscriptionPath.class)
                .orElseThrow(RuntimeException::new);

        final String path = annotation.value();
        final int identifiers = TextUtil.countOccurrences("{id}", path);

        if (identifiers > 1) {
            throw new IllegalStateException(format("Unsupported number of groups on %s path %s", clazz.getName(), path));
        }
        else if (identifiers == 1) {
            if (!SubscribeBy.class.isAssignableFrom(clazz)) {
                throw new IllegalStateException(format("Class %s for path %s must implement %s if it has an identifier.",
                                                       clazz.getName(), path, SubscribeBy.class.getSimpleName()));
            }
        }

        // Convert {id} to group for matching later:
        return Pattern.compile(path.replaceAll("\\{id}", "(.*)"));
    }


    @Override
    public Message<?> preSend(@Nonnull final Message<?> message, @Nonnull final MessageChannel channel) {
        final StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(message);


        final StompCommand command = headerAccessor.getCommand();
        if (command == null) {
            return message;
        }

        switch (command) {
            case CONNECT:
                if (!registerWebSocketToken(headerAccessor)) {
                    LOG.error("CONNECT REJECTED.");
                    return null;
                }
            break;
            case SUBSCRIBE:
                if (!handleSubscribe(headerAccessor))  {
                    LOG.error("SUBSCRIPTION REJECTED.");
                    return null;
                }
                break;
            case DISCONNECT:
                handleDisconnect(headerAccessor);
                break;
            default:
                break;
        }

        return message;
    }

    private void handleDisconnect(final StompHeaderAccessor headerAccessor) {
        final PlayerWebsocketToken playerWebsocketToken = getPlayerWebsocketToken(headerAccessor);

        final Optional<PlayerTokensDto> playerToken = tokenService.getDetailsForToken(playerWebsocketToken);

        if (playerToken.isPresent()) {
            LOG.info("Disconnect: " + playerToken.get());

            eventBroadcaster.globalBroadcast(new PlayerDisconnectedEvent(playerToken.get().getPlayerRef()));
        }
        else {
            LOG.info("Disconnected websocket with no token, probably already disconnected by rest.");
        }
    }

    /**
     * Verify that the requester is authorised to subscribe to the given endpoint.
     * @return true on success, false on no authorisation.
     */
    private boolean handleSubscribe(final StompHeaderAccessor headerAccessor) {
        final String destination = headerAccessor.getDestination();
        if (destination == null) {
            throw new IllegalArgumentException("Destination of subscription cannot be null.");
        }

        LOG.info("SUBSCRIBE TO: " + destination);

        final List<Matcher> matches = modelsByPathPattern.keySet().stream()
                                                          .map(p -> p.matcher(destination))
                                                          .filter(Matcher::matches)
                                                          .collect(Collectors.toList());


        if (matches.size() > 1) {
            // Paths should not collide like this.
            throw new IllegalStateException("More than one path match for destination:" + destination + " : " + matches);
        }
        if (matches.size() < 1) {
            LOG.info("Destination {} does not match a subscription endpoint.", destination);
            return false;
        }

        final Matcher matcher = matches.get(0);
        LOG.info("Request matches {}", matcher.pattern());

        final int groupCount = matcher.groupCount();

        final Optional<PlayerRef> authenticatedPlayer = getPlayerRestToken(headerAccessor)
                                                            .flatMap(tokenService::getDetailsForToken)
                                                            .map(PlayerTokensDto::getPlayerRef);

        // Always require an authenticated player for any subscription:
        if (authenticatedPlayer.isPresent()) {
            if (groupCount == 1) {
                final Object authObject = getAuthObject(matcher.group(1), modelsByPathPattern.get(matcher.pattern()));
                if (!checkAuth(authObject, authenticatedPlayer.get())) {
                    LOG.info("Player {} failed auth check for {}, not allowed to listen to events for {}.",
                            authenticatedPlayer.get(),
                            destination,
                            authObject);

                    return false;
                }
            }
            // Other possible group count is 1 and that only requires player present.
        }
        else {
            LOG.info("No authenticated player present for " + destination);
            return false;
        }
        return true;
    }

    /**
     * Attempt to register a web socket token against a player.
     * Failure indicates no authorisation.
     * @return true if success, false otherwise.
     */
    private boolean registerWebSocketToken(final StompHeaderAccessor headerAccessor) {
        final Optional<PlayerRestToken> playerToken = getPlayerRestToken(headerAccessor);

        if (playerToken.isPresent()) {
            final PlayerWebsocketToken playerWebsocketToken = getPlayerWebsocketToken(headerAccessor);
            tokenService.registerWebSocketSession(playerWebsocketToken, playerToken.get());
            LOG.info("Registered websocket token: " + playerWebsocketToken);
            return true;
        }
        else {
            return false;
        }
    }

    private PlayerWebsocketToken getPlayerWebsocketToken(final StompHeaderAccessor headerAccessor) {
        return PlayerWebsocketToken.fromRawString(headerAccessor.getSessionId());
    }

    private Optional<PlayerRestToken> getPlayerRestToken(final StompHeaderAccessor headerAccessor) {
        final List<String> authHeaders = Optional.ofNullable(headerAccessor.getNativeHeader(TokenAuthorisationRESTInterceptor.AUTHORIZATION_HEADER))
                                                 .orElseGet(Collections::emptyList);
        return authHeaders.stream()
                .findFirst()
                .map(PlayerRestToken::new);
    }

    /**
     * Construct an instance of the auth object using the SubscribeBy interface of the PushModel class.
     */
    @SuppressWarnings("unchecked")
    private Object getAuthObject(final String group, final Class<?> clazz) {
        if (SubscribeBy.class.isAssignableFrom(clazz)) {
            final Class<?> referenceClass = ReflectionUtil.getGenericClass((Class<? extends SubscribeBy<?>>) clazz, SubscribeBy.class);
            try {
                final Constructor<?> constructor = referenceClass.getConstructor(String.class);
                return constructor.newInstance(group);
            }
            catch (final NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                throw new IllegalArgumentException("No appropriate String constructor for class " + clazz, e);
            }
        }
        else {
            throw new IllegalStateException("Cannot map class that does not implement SubscribeBy: " + clazz.getName());
        }
    }

    /**
     * This is the same logic as TokenAuthorisationRESTInterceptor... should combine...
     */
    private boolean checkAuth(final Object authObject, final PlayerRef authenticatedPlayer) {
        if (authObject instanceof PlayerRef) {
            return authenticatedPlayer.equals(authObject);
        }
        else if (authObject instanceof GameRef) {
            final Game game = gameService.getGame((GameRef) authObject);

            return game.getPlayers().contains(authenticatedPlayer);
        }
        // Add Auth for other classes here as needed...
        else {
            return false;
        }
    }
}

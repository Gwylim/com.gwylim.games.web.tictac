package com.gwylim.games.web.tictac.api.impl.config;

import com.clevergang.jdbc.FluentNamedParameterJdbcTemplate;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(basePackages = "com.gwylim")
@EntityScan(basePackages = "com.gwylim")
public class DatabaseConfig {

    /**
     * A nicer wrapper around jdbc.
     */
    @Bean
    public FluentNamedParameterJdbcTemplate getFluentJdbcTemplate(final DataSource dataSource) {
        return new FluentNamedParameterJdbcTemplate(dataSource);
    }

}

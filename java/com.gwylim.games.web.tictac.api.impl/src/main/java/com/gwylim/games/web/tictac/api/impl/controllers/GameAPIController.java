package com.gwylim.games.web.tictac.api.impl.controllers;

import com.gwylim.games.web.tictac.api.GameAPI;
import com.gwylim.games.web.tictac.api.impl.auth.Auth;
import com.gwylim.games.web.tictac.api.impl.converters.GameToApiModel;
import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.api.models.rest.GameApiModel;
import com.gwylim.games.web.tictac.api.requests.MakeMoveApiRequest;
import com.gwylim.games.web.tictac.service.game.GameService;
import com.gwylim.games.web.tictac.service.game.model.Game;
import com.gwylim.games.web.tictac.service.game.model.GameException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(GameAPI.PATH)
public class GameAPIController implements GameAPI {
    private static final Logger LOG = LoggerFactory.getLogger(GameAPIController.class);

    private final GameService gameService;

    private final GameToApiModel gameToApiModel;

    @Autowired
    public GameAPIController(final GameService gameService,
                             final GameToApiModel gameToApiModel) {
        this.gameService = gameService;
        this.gameToApiModel = gameToApiModel;
    }

    @Override
    public GameApiModel getGame(@Auth final GameRef gameRef) {
        LOG.info("Get game: " + gameRef);

        final Game game = gameService.getGame(gameRef);
        return gameToApiModel.apply(game);
    }

    @Override
    public GameApiModel makeMove(@Auth final GameRef gameRef,
                                 @Auth final PlayerRef playerRef,
                                 final MakeMoveApiRequest move) {

        LOG.info("Make move: X:{} Y:{} Player:{} Game:{}", move.getX(), move.getY(), playerRef, gameRef);

        if (move.getX() > 2 || move.getY() > 2
         || move.getX() < 0 || move.getY() < 0)  {

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid move.");
        }

        try {
            gameService.makeMove(move.getX(), move.getY(), playerRef, gameRef);
            return getGame(gameRef);
        }
        catch (final GameException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @Override
    public void leave(final GameRef gameRef, @Auth final PlayerRef playerRef) {
        final Game game = gameService.getGame(gameRef);
        if (game.getPlayers().contains(playerRef)) {
            gameService.gameTerminated(gameRef);
        }
        else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Player not part of game.");
        }
    }
}

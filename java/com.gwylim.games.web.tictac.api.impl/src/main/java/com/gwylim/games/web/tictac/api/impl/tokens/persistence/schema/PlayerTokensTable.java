package com.gwylim.games.web.tictac.api.impl.tokens.persistence.schema;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "player_tokens", indexes = {
    @Index(columnList = "player_ref"),
    @Index(columnList = "player_websocket_token", unique = true),
    @Index(columnList = "player_rest_token", unique = true)
})
@SuppressWarnings("unused")
public class PlayerTokensTable {
    @Id
    @Column(name = "player_rest_token")
    private String playerToken;

    @Nullable
    @Column(name = "player_websocket_token")
    private String playerWebSocketToken;

    @Column(name = "player_ref")
    private String playerRef;


    @Column(name = "updated_at")
    private LocalDateTime lastUpdated;
}

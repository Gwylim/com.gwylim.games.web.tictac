package com.gwylim.games.web.tictac.api.impl.tokens.persistence;

import com.clevergang.jdbc.FluentNamedParameterJdbcTemplate;
import com.google.common.base.Joiner;
import com.gwylim.games.web.tictac.api.impl.tokens.model.PlayerTokensDto;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRestToken;
import com.gwylim.games.web.tictac.api.models.references.PlayerWebsocketToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

/**
 * Persistence for auth tokens for players.
 * See: https://www.baeldung.com/spring-data-jpa-query
 */
@Component
public class PlayerTokenRepository {
    private static final Logger LOG = LoggerFactory.getLogger(PlayerTokenRepository.class);

    private final FluentNamedParameterJdbcTemplate jdbc;

    @Autowired
    public PlayerTokenRepository(final FluentNamedParameterJdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    public Optional<PlayerTokensDto> get(final PlayerRestToken playerToken) {
        return jdbc.query("SELECT * FROM player_tokens WHERE player_rest_token = :player_rest_token")
                   .bind("player_rest_token", playerToken.getValue())
                   .fetch(this::mapper)
                   .stream()
                   .findFirst();
    }

    public Optional<PlayerTokensDto> get(final PlayerWebsocketToken playerToken) {
        return jdbc.query("SELECT * FROM player_tokens WHERE player_websocket_token = :player_websocket_token")
                .bind("player_websocket_token", playerToken.getValue())
                .fetch(this::mapper)
                .stream()
                .findFirst();
    }

    public PlayerTokensDto save(final PlayerRef playerRef, final PlayerRestToken playerToken) {
        jdbc.update(Joiner.on(" ").join(
                "INSERT INTO player_tokens (player_rest_token, player_ref, updated_at)",
                                     "VALUES (:player_rest_token, :player_ref, :updated_at)"))
            .bind("player_rest_token", playerToken.getValue())
            .bind("player_ref", playerRef.getValue())
            .bind("updated_at", new Date())
            .execute();

        return get(playerToken).orElseThrow(() -> new RuntimeException("Failed to retrieve rest token that was just saved."));
    }

    public PlayerTokensDto addWebSocketToken(final PlayerWebsocketToken webSocketToken,
                                             final PlayerRestToken restToken) {

        jdbc.update(Joiner.on(" ").join(
            "UPDATE player_tokens SET player_websocket_token = :player_websocket_token",
            "WHERE player_rest_token = :player_rest_token"))
            .bind("player_rest_token", restToken.getValue())
            .bind("player_websocket_token", webSocketToken.getValue())
            .bind("updated_at", new Date())
            .execute();

        return get(webSocketToken).orElseThrow(() -> new RuntimeException("Failed to retrieve websocket token that was just saved."));
    }

    private PlayerTokensDto mapper(final ResultSet resultSet, final int i) {
        try {
            return new PlayerTokensDto(
                    new PlayerRestToken(resultSet.getString("player_rest_token")),
                    Optional.ofNullable(resultSet.getString("player_websocket_token"))
                            .map(PlayerWebsocketToken::new)
                            .orElse(null),
                    new PlayerRef(resultSet.getString("player_ref")),
                    LocalDateTime.from(resultSet.getTimestamp("updated_at").toInstant().atZone(ZoneId.systemDefault()))

            );
        }
        catch (final SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void delete(final PlayerRef playerRef) {
        final int rows = jdbc.update(Joiner.on(" ").join(
                "DELETE FROM player_tokens",
                "WHERE player_ref = :player_ref"))
                .bind("player_ref", playerRef.getValue())
                .execute();

        if (rows != 1) {
            LOG.warn("Expected one row to be modified but was " + rows);
        }
    }
}

package com.gwylim.games.web.tictac.api.impl.controllers;

import com.gwylim.games.web.tictac.api.LobbyAPI;
import com.gwylim.games.web.tictac.api.impl.auth.Auth;
import com.gwylim.games.web.tictac.api.impl.auth.RequireAnyAuth;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.service.LobbyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Set;

@RestController
@RequestMapping(LobbyAPI.PATH)
public class LobbyAPIController implements LobbyAPI {
    private static final Logger LOG = LoggerFactory.getLogger(LobbyAPIController.class);

    private final LobbyService lobbyService;

    @Autowired
    public LobbyAPIController(final LobbyService lobbyService) {
        this.lobbyService = lobbyService;
    }

    @Override
    public void enterLobby(@Auth final PlayerRef playerRef) {
        LOG.info("Player {} enter lobby request.", playerRef);
        lobbyService.enterPlayer(playerRef);
    }

    @Override
    public void exitLobby(@Auth final PlayerRef playerRef) {
        lobbyService.exitPlayer(playerRef);
    }

    @Override
    @RequireAnyAuth
    public Set<PlayerRef> getPlayersInLobby() {
        return lobbyService.getPlayers();
    }

    @Override
    public Set<PlayerRef> getOpponents(@Auth final PlayerRef playerRef) {
        return lobbyService.getOpponentsForPlayer(playerRef);
    }

    @Override
    public void addOpponent(@Auth final PlayerRef playerRef,
                            final PlayerRef opponentRef) {

        if (playerRef.equals(opponentRef)) {
            LOG.warn("Player selected themselves:" + playerRef);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Player cannot play itself.");
        }

        lobbyService.addOpponentSelection(playerRef, opponentRef);
    }

    @Override
    public void removeOpponent(@Auth final PlayerRef playerRef,
                               final PlayerRef opponentRef) {

        lobbyService.removeOpponentSelection(playerRef, opponentRef);
    }

    @Override
    public void toggleOpponent(@Auth final PlayerRef playerRef,
                               final PlayerRef opponentRef) {

        final Set<PlayerRef> opponents = getOpponents(playerRef);

        if (opponents.contains(opponentRef)) {
            removeOpponent(playerRef, opponentRef);
        }
        else {
            addOpponent(playerRef, opponentRef);
        }
    }
}

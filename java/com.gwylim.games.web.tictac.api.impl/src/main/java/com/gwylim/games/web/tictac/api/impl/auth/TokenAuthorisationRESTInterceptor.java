package com.gwylim.games.web.tictac.api.impl.auth;

import com.gwylim.common.ReflectionUtil;
import com.gwylim.games.web.tictac.api.impl.tokens.TokenService;
import com.gwylim.games.web.tictac.api.impl.tokens.model.PlayerTokensDto;
import com.gwylim.games.web.tictac.api.models.references.GameRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRestToken;
import com.gwylim.games.web.tictac.service.game.GameService;
import com.gwylim.games.web.tictac.service.game.model.Game;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.gwylim.common.ReflectionUtil.hasAnnotation;

/**
 * Use the auth token from the header of requests to authorise access and intention on all REST endpoints.
 *
 * See: https://stackoverflow.com/questions/38360215/how-to-create-a-spring-interceptor-for-spring-restful-web-services
 *
 * Looks like this needs to be an AOP interceptor.
 * https://stackoverflow.com/questions/24604593/access-to-controllers-method-parameter-values-from-spring-mvc-interceptor
 */
@Aspect
@Component
public class TokenAuthorisationRESTInterceptor {
    private static final Logger LOG = LoggerFactory.getLogger(TokenAuthorisationRESTInterceptor.class);

    static final String AUTHORIZATION_HEADER = "Authorization";

    private final TokenService tokenService;
    private final GameService gameService;

    @Autowired
    public TokenAuthorisationRESTInterceptor(final TokenService tokenService,
                                             final GameService gameService) {
        this.tokenService = tokenService;
        this.gameService = gameService;
    }

    /**
     * Interceptor for all calls to public methods on RestController annotated classes.
     * https://stackoverflow.com/questions/2011089/aspectj-pointcut-for-all-methods-of-a-class-with-specific-annotation
     */
    @Around("within(@org.springframework.web.bind.annotation.RestController *) AND execution(public * *(..))")
    public Object authInterceptor(final ProceedingJoinPoint joinPoint) throws Throwable {
        final HttpServletRequest request = getHttpServletRequest();
        final String requestURI = request.getRequestURI();

        final Method method = getMethod(joinPoint);

        LOG.info("Intercepted: " + requestURI + " -> " + joinPoint.getTarget().getClass() + "#" + method.getName());


        final boolean noAuthRequired = hasAnnotation(method, NoAuthRequired.class);
        if (noAuthRequired) {
            LOG.info(requestURI + ": NO AUTH REQUIRED");
            return joinPoint.proceed();
        }
        else {
            // Get authenticated payer from auth token:
            final Optional<PlayerRef> authenticatedPlayer = Optional.ofNullable(request.getHeader(AUTHORIZATION_HEADER))
                    .flatMap(token -> tokenService.getDetailsForToken(new PlayerRestToken(token)))
                    .map(PlayerTokensDto::getPlayerRef);

            if (authenticatedPlayer.isPresent()) {
                handleAuthenticatedPlayer(authenticatedPlayer.get(), method, joinPoint);
            }
            else {
                LOG.info(requestURI + ": AUTH REQUIRED BUT NOT PRESENT");
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Token needed to access resource.");
            }

            return joinPoint.proceed();
        }
    }


    private void handleAuthenticatedPlayer(final PlayerRef authenticatedPLayer, final Method method, final ProceedingJoinPoint joinPoint) {
        final boolean requireAnyAuth = hasAnnotation(method, RequireAnyAuth.class);
        if (requireAnyAuth) {
            LOG.info("ANY AUTH REQUIRED: IS PRESENT");
            return;
        }

        final List<Object> authArgs = getAnnotatedValues(method, joinPoint.getArgs());
        if (authArgs.size() == 0) {
            // Require some manner of auth for every endpoint:
            // TODO: It would be good to verify this at launch or even compile rather than on invocation.
            throw new RuntimeException("No auth specified for " + ReflectionUtil.getMethodIdentifier(method));
        }

        authArgs.forEach(o -> checkAuth(o, authenticatedPLayer));

        LOG.info(authArgs.toString());
    }

    private void checkAuth(final Object authObject, final PlayerRef authenticatedPlayer) {
        if (authObject instanceof PlayerRef) {
            if (!authenticatedPlayer.equals(authObject)) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Authenticated player cannot act on behalf of other player " + authObject);
            }
        }
        else if (authObject instanceof GameRef) {
            final Game game = gameService.getGame((GameRef) authObject);

            if (!game.getPlayers().contains(authenticatedPlayer)) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Authenticated player cannot act on behalf of other player " + authObject);
            }
        }
        // Add Auth for other classes here as needed...
        else {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Type " + authObject.getClass().getName() + " has no auth handling.");
        }
    }

    /**
     * Get the arguments for parameters annotated @Auth.
     */
    private List<Object> getAnnotatedValues(final Method method, final Object[] args) {
        final Annotation[][] parameterAnnotations = method.getParameterAnnotations();

        final List<Object> paramNumbers = new ArrayList<>();
        for (int i = 0; i < parameterAnnotations.length; i++) {
            final boolean hasAnnotation = Arrays.stream(parameterAnnotations[i])
                    .map(Annotation::annotationType)
                    .anyMatch(Auth.class::equals);

            if (hasAnnotation) {
                paramNumbers.add(args[i]);
            }
        }

        return paramNumbers;
    }


    private Method getMethod(final ProceedingJoinPoint joinPoint) {
        final MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        return signature.getMethod();
    }

    /**
     * Get the HttpServletRequest that is in progress in the current thread.
     * https://stackoverflow.com/questions/24604593/access-to-controllers-method-parameter-values-from-spring-mvc-interceptor
     */
    private static HttpServletRequest getHttpServletRequest() {
        final RequestAttributes reqAttr = RequestContextHolder.getRequestAttributes();
        if (reqAttr == null) {
            throw new IllegalStateException("getHttpServletRequest invoked outside request.");
        }
        return ((ServletRequestAttributes) reqAttr).getRequest();
    }
}

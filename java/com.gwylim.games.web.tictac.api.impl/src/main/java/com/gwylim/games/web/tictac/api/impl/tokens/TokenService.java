package com.gwylim.games.web.tictac.api.impl.tokens;

import com.gwylim.games.web.tictac.api.impl.tokens.model.PlayerTokensDto;
import com.gwylim.games.web.tictac.api.impl.tokens.persistence.PlayerTokenRepository;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.api.models.references.PlayerRestToken;
import com.gwylim.games.web.tictac.api.models.references.PlayerWebsocketToken;
import com.gwylim.games.web.tictac.events.PlayerDisconnectedEvent;
import com.gwylim.system.events.api.annotations.EventListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TokenService {

    private final PlayerTokenRepository tokenDAO;

    @Autowired
    public TokenService(final PlayerTokenRepository tokenDAO) {
        this.tokenDAO = tokenDAO;
    }


    public PlayerTokensDto createToken(final PlayerRef playerRef) {
        return tokenDAO.save(playerRef, PlayerRestToken.generateNew());
    }

    public Optional<PlayerTokensDto> getDetailsForToken(final PlayerRestToken playerToken) {
        return tokenDAO.get(playerToken);
    }

    public Optional<PlayerTokensDto> getDetailsForToken(final PlayerWebsocketToken playerToken) {
        return tokenDAO.get(playerToken);
    }

    public PlayerTokensDto registerWebSocketSession(final PlayerWebsocketToken webSocketToken,
                                                    final PlayerRestToken playerRestToken) {

        return tokenDAO.addWebSocketToken(webSocketToken, playerRestToken);
    }

    @EventListener
    public void handlerPlayerDisconnect(final PlayerDisconnectedEvent playerDisconnectedEvent) {
        tokenDAO.delete(playerDisconnectedEvent.getPlayerRef());
    }
}

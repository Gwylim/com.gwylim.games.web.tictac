package com.gwylim.games.web.tictac.api.impl.config;

import com.gwylim.games.web.tictac.api.converters.MeaningfulMessageConverter;
import com.gwylim.games.web.tictac.api.converters.ReferenceMessageConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * Enable cross site requests for the moment.
 */
@Configuration
public class RESTConfig implements WebMvcConfigurer {
    private static final Logger LOG = LoggerFactory.getLogger(RESTConfig.class);

    @Override
    public void addCorsMappings(final CorsRegistry registry) {
        LOG.info("ENABLE CORS");

        registry.addMapping("/**").allowedOrigins("**");
    }

    @Override
    public void extendMessageConverters(final List<HttpMessageConverter<?>> converters) {
        converters.add(new ReferenceMessageConverter());
        converters.add(new MeaningfulMessageConverter());
    }
}

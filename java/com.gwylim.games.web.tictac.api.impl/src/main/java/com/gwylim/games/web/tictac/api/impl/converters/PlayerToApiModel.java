package com.gwylim.games.web.tictac.api.impl.converters;

import com.gwylim.games.web.tictac.api.models.rest.PlayerApiModel;
import com.gwylim.games.web.tictac.service.player.model.PlayerDetails;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class PlayerToApiModel implements Function<PlayerDetails, PlayerApiModel> {
    @Override
    public PlayerApiModel apply(final PlayerDetails playerDetails) {
        return new PlayerApiModel(playerDetails.getPlayerRef(),
                                  playerDetails.getType(),
                                  playerDetails.getName(),
                                  playerDetails.getIcon()
        );
    }
}

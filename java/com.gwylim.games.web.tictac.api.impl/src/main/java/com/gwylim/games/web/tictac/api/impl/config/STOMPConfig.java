package com.gwylim.games.web.tictac.api.impl.config;

import com.gwylim.games.web.tictac.api.impl.auth.TokenAuthorisationWebsocketInterceptor;
import com.gwylim.system.push.messaging.server.PushServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.core.MessageSendingOperations;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * Configuration for web sockets.
 * Based on:
 * https://www.devglan.com/spring-boot/spring-boot-websocket-integration-example
 *
 * TODO: Implement session management
 */
@Configuration
@EnableWebSocketMessageBroker
public class STOMPConfig implements WebSocketMessageBrokerConfigurer {
    private static final Logger LOG = LoggerFactory.getLogger(STOMPConfig.class);

    private final TokenAuthorisationWebsocketInterceptor tokenAuthorisationWebsocketInterceptor;

    @Autowired
    public STOMPConfig(final TokenAuthorisationWebsocketInterceptor tokenAuthorisationWebsocketInterceptor) {

        this.tokenAuthorisationWebsocketInterceptor = tokenAuthorisationWebsocketInterceptor;
    }

    @Override
    public void configureMessageBroker(final MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic");

        // Not needed as we are not receiving messages:
        //config.setApplicationDestinationPrefixes("/app");
    }

    @Override
    public void registerStompEndpoints(final StompEndpointRegistry registry) {
        LOG.info("Registering STOMP endpoints.");

        registry.addEndpoint("/updates")
                .setAllowedOrigins("*");

        registry.addEndpoint("/updates")
                .setAllowedOrigins("*")
                .withSockJS();
    }

    /**
     * Add the authorisation filter to incoming message channel.
     * Based on:
     * https://github.com/spring-projects/spring-security/blob/master/messaging/src/main/java/org/springframework/security/messaging/web/csrf/CsrfChannelInterceptor.java
     */
    public void configureClientInboundChannel(final ChannelRegistration registration) {
        registration.interceptors(tokenAuthorisationWebsocketInterceptor);
    }

    @Bean
    public static PushServer createServer(final MessageSendingOperations<String> messagingTemplate) {
        LOG.info("Creating Push Server.");
        return new PushServer(messagingTemplate);
    }
}
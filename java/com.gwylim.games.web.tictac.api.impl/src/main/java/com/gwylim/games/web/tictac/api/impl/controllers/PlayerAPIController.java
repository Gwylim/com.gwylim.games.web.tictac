package com.gwylim.games.web.tictac.api.impl.controllers;

import com.gwylim.games.web.tictac.api.PlayerAPI;
import com.gwylim.games.web.tictac.api.impl.auth.Auth;
import com.gwylim.games.web.tictac.api.impl.auth.NoAuthRequired;
import com.gwylim.games.web.tictac.api.impl.auth.RequireAnyAuth;
import com.gwylim.games.web.tictac.api.impl.converters.PlayerToApiModel;
import com.gwylim.games.web.tictac.api.impl.tokens.TokenService;
import com.gwylim.games.web.tictac.api.impl.tokens.model.PlayerTokensDto;
import com.gwylim.games.web.tictac.api.models.PlayerType;
import com.gwylim.games.web.tictac.api.models.references.PlayerRef;
import com.gwylim.games.web.tictac.api.models.rest.CurrentPlayerApiModel;
import com.gwylim.games.web.tictac.api.models.rest.PlayerApiModel;
import com.gwylim.games.web.tictac.api.requests.SignInApiRequest;
import com.gwylim.games.web.tictac.api.requests.SignUpApiRequest;
import com.gwylim.games.web.tictac.events.PlayerDisconnectedEvent;
import com.gwylim.games.web.tictac.service.player.auth.PlayerAuthService;
import com.gwylim.games.web.tictac.service.player.auth.exceptions.UsernameExistsException;
import com.gwylim.games.web.tictac.service.player.PlayerDetailsService;
import com.gwylim.games.web.tictac.service.player.model.PlayerDetails;
import com.gwylim.system.events.api.EventBroadcaster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.Nonnull;
import java.util.Optional;

@RestController
@RequestMapping(PlayerAPI.PATH)
public class PlayerAPIController implements PlayerAPI {
    private static final Logger LOG = LoggerFactory.getLogger(PlayerAPIController.class);

    private final PlayerDetailsService playerService;
    private final PlayerAuthService playerAuthService;
    private final TokenService tokenService;

    private final EventBroadcaster eventBroadcaster;

    private final PlayerToApiModel playerToApiModel;

    @Autowired
    public PlayerAPIController(final PlayerDetailsService playerService,
                               final PlayerAuthService playerAuthService,
                               final TokenService tokenService,
                               final EventBroadcaster eventBroadcaster,
                               final PlayerToApiModel playerToApiModel) {

        this.playerService = playerService;
        this.playerAuthService = playerAuthService;
        this.tokenService = tokenService;
        this.eventBroadcaster = eventBroadcaster;
        this.playerToApiModel = playerToApiModel;
    }


    @Override
    @NoAuthRequired
    public CurrentPlayerApiModel signIn(final SignInApiRequest request) {
        LOG.info("Log in: " + request.getUsername());
        final Optional<PlayerRef> playerRef = playerAuthService.authenticatePlayer(request.getUsername(), request.getPassword());
        if (playerRef.isPresent()) {
            final PlayerDetails player = playerService.getPlayer(playerRef.get());

            return createSessionToken(player);
        }
        else {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }

    @Override
    @NoAuthRequired
    public CurrentPlayerApiModel signUp(final SignUpApiRequest request) {
        try {
            LOG.info("Sign up: " + request.getUsername());
            final PlayerRef playerRef = playerAuthService.createPlayerAuth(request.getUsername(), request.getPassword());
            final PlayerDetails player = playerService.createPlayerDetails(request.getUsername().getValue(), PlayerType.HUMAN, playerRef);

            return createSessionToken(player);
        }
        catch (final UsernameExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    @Nonnull
    private CurrentPlayerApiModel createSessionToken(final PlayerDetails player) {
        final PlayerTokensDto token = tokenService.createToken(player.getPlayerRef());
        LOG.info("Created token for player: " + token);

        return new CurrentPlayerApiModel(playerToApiModel.apply(player), token.getPlayerToken());
    }

    @Override
    @RequireAnyAuth
    public PlayerApiModel getPlayer(final PlayerRef playerRef) {
        final PlayerDetails player = playerService.getPlayer(playerRef);
        return playerToApiModel.apply(player);
    }

    @Override
    public void disconnect(@Auth final PlayerRef playerRef) {
        eventBroadcaster.globalBroadcast(new PlayerDisconnectedEvent(playerRef));
    }
}
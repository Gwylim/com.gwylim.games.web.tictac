# Tic-Tac-Toe Web app
Web application with a simple domain for experimentation.

## Typed References
### Background
It is frequent to use a simple Java type are an identifier for entities, this makes it easy to being easy to 
mismatch types of identifier in methods that take several identifiers and does not allow IDEs to auto-complete 
quickly based on type.

It is also best not to expose the database/storage IDs used for entity tables outside the data access layer of the 
application as this ties one to the current implementation and exposes more information to potential attackers than 
necessary.

We can create different reference types to identify each of the entities in our system. Given that we have this new 
type we can add and enforce some structure on the identifiers to make reading logs easier and provide mismatch 
prevention even after transiting through a dynamically typed language or API. 

### Goals
- **Type safety**  
Each entity reference must be a unique Java type, mismatch should be a compile-time error.
- **Structure in the identifiers that make it clear what kind of entity it references.**  
The string or wire representation of the identifier should make it clear what kind of entity references. 
- **Enforce type safety even over REST communication.**  
It should be a runtime error as soon as possible if two reference types have been mismatched in third party code.
- **Transparency to Non-java clients**  
The API should not have to translate these objects and non-java clients must not have to perform any 
extra work to read them. I.E: JSON/PathParams should remain simple. 

### Implementation
Implementation of base class: **com.gwylim.common.models.Reference**  
Each entity is referenced by a different subclass of Reference so can be checked by type.  

The Reference class enforces that there is a prefix on the string representation of the reference, this prefix can be
 any length but a short prefix is sufficient to easily identify what type of reference is being looked at. This is 
 also checked when de-serializing REST communications so mismatches cannot occur.  
 
The serialisation of the Reference objects is just the string representation (@JsonValue is just the identifier string).

There are scenarios where the identifier string should be more securely generated than just a UUID so 
generateUniqueString can be overridden, for example with SecureRandom for PlayerRestToken.

## Typed Event System
### Background
To promote loose coupling of code and a significantly improved dependency graph a good way of modeling the movement 
of data is with the sending and receiving of events.  
When an area of code takes or observes something that other code should take action based on, it broadcasts an event.  
All code that has registered interest in that event will receive it and can take appropriate action.  

Events can carry some associated data that both identifies the event and will aid handling it.  
However it is sensible for the receiver to query for the current state of any entity referred to by the event.    
To this end only reference objects should be sent in events so the receiver is forced to query persistent storage to 
get the latest versions of the entity.  
This also facilitates easier definitions for the subject of events (See later).    

Spring has an event system that allows subscription and sending of events, unfortunately it does not support 
multicasting of events.  
E.G: There may be many subscribers to the event but only one will relieve it.

### Goals
- **Broadcast**  
I wish to be able to send events with information attached to be received by any number (including 0) interested 
listeners.  
The type of event and attached information will be dictated by class.
- **Polymorphic subscriptions**  
I wish to be able to listen to some type of event, what I am listening for will be dictated by Java class.
If an event is broadcast that is more specific than what I am listening for but has has my type as a supertype I 
still want to receive it.
- **Subscription filtering**  
I wish to be able to limit what events I receive based on some of the formation in the event.
e.g: I wish to filter moves made to moves made by a particular player.
- **Annotation based subscription**  
An object instantiated by Spring DI should come already subscribed to appropriate events.
This should be done by annotation on receiving methods.

### Implementation
Found in package: **com.gwylim.system.events**
Events are standard Java objects and can be broadcast by getting an instance of EventBroadcaster and calling 
globalBroadcast.  
This will call receive on all Listener objects that have been registered with it that receive either the event type 
or supertype and have the appropriate subjects.  

An event is any java object and can optionally have methods annotated with @Subject. When an event is broadcast any 
such method will be called and the response will be used as the subjects for this event.  
The methods may return either a single object or a collection of objects.

To receive events one must create a Listener object and register is using a EventSystem instance.  
The Listener interface allows one to specify the Java class type of event one wishes to receive and the code to run 
on receiving an event.  
Also the subjects one is interested in can be specified, if none are specified it is assumed that the listener is 
interested in all events of the given type.  

If ine is using Spring the com.gwylim.system.events.spring project contains an EventSystem injectable component and 
bean PostProcessors that look for annotations on objects as they are created and destroyed.  
Methods with the @EventListener will be wrapped in a Listener and automatically registered when the Bean is created 
and unregistered when destroyed.  
If the object has any methods annotated @Subject these will be used as the subjects for the created listeners.

## APO/Annotation based request authentication framework
### Background
### Goals
### Implementation

## STOMP WebSockets Typed Subscription framework
### Background
STOMP is a pub/sub layer over web sockets, clients can subscribe to destinations and will receive all messages sent 
there.  
This is very useful, for example, updating all members of a game about the events in that game. 
It would be really useful to not have to do serialisation or routing of events to the correct destination for each 
type of event that can happen.

### Goals
- Routing of messages to destinations should be easy to specify and require minimal code.
- We can have multiple different instances of each destination type, for example, different games, we must be able to
 publish and subscribe to a specific one.
- We must be able to restrict the destination to which a client can subscribe, e.g, only the players in a game can 
subscribe to moves in that game occurring in that game.
- Interaction with the serve must be simple for other languages for ether subscription or receipt of messages. 

### Implementation
Unsurprisingly the code is client and server:  
- **com.gwylim.system.push.messaging.server.PushServer**  
Used by the server to send messages to destinations.  
- **com.gwylim.system.push.messaging.client.PushClient**  
Used by clients to subscribe to destinations. This allows types to be shared between server and client for 
destination specification and message de-serialisation.

This implementation is inspired by feign, but instead of resources there are message types which are annotated to 
specify the destination they are sent to and specify the identifiers for different instances. 
@SubscriptionPath indicates the destination that the type will be sent to and the interface SubscribeBy allows the 
returning of a set of identifiers that will be inserted into the path provided by the @SubscriptionPath annotation.  

The client can then use the same types passed to the PushClient to create a subscription. If the type in question 
requires an identifier this can also be passed in. Clients subscribe with a consumer for the event type, the returned 
Subscription object can be used to manage the subscription, in particular "unsubscribe".  

The server is simple to interact with, one can look for the types that define messages and look for the path on the 
@SubscriptionPath annotation and the identifier it needs given the SubscribeBy interface, the received text will be 
JSON of the structure indicated by the type. 

## REST and Websocket API
### Background
### Goals
### Implementation

## React/Redux Web Frontend
### Background
### Goals
### Implementation

## Vaadin Frontend
### Background
### Goals
### Implementation

## CLI Frontend
### Background
### Goals
### Implementation

## QR Code generation
### Background
### Goals
### Implementation

